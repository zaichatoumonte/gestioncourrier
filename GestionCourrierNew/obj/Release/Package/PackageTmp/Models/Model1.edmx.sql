
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 01/17/2020 11:28:53
-- Generated from EDMX file: C:\Users\atoumonte\Desktop\GestionCourrierNew\GestionCourrierNew\GestionCourrierNew\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [GestionCourrierSoutenanceNew];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ACCEDER_DROIT_ACCES]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ACCEDER] DROP CONSTRAINT [FK_ACCEDER_DROIT_ACCES];
GO
IF OBJECT_ID(N'[dbo].[FK_ACCEDER_MODULE]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ACCEDER] DROP CONSTRAINT [FK_ACCEDER_MODULE];
GO
IF OBJECT_ID(N'[dbo].[FK_COURRIER_CATEGORIE_COURRIER]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[COURRIER] DROP CONSTRAINT [FK_COURRIER_CATEGORIE_COURRIER];
GO
IF OBJECT_ID(N'[dbo].[FK_ADRESSER_CORRESPONDANT]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ECHANGER] DROP CONSTRAINT [FK_ADRESSER_CORRESPONDANT];
GO
IF OBJECT_ID(N'[dbo].[FK_CORRESPONDANT_TYPE_CORRESPONDANT]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CORRESPONDANT] DROP CONSTRAINT [FK_CORRESPONDANT_TYPE_CORRESPONDANT];
GO
IF OBJECT_ID(N'[dbo].[FK_ADRESSER_COURRIER]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ECHANGER] DROP CONSTRAINT [FK_ADRESSER_COURRIER];
GO
IF OBJECT_ID(N'[dbo].[FK_COURRIER_AGENT]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[COURRIER] DROP CONSTRAINT [FK_COURRIER_AGENT];
GO
IF OBJECT_ID(N'[dbo].[FK_COURRIER_NATURE]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[COURRIER] DROP CONSTRAINT [FK_COURRIER_NATURE];
GO
IF OBJECT_ID(N'[dbo].[FK_COURRIER_TYPE_COURRIER_DEPART]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[COURRIER] DROP CONSTRAINT [FK_COURRIER_TYPE_COURRIER_DEPART];
GO
IF OBJECT_ID(N'[dbo].[FK_AGENT_DROIT_ACCES]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UTILISATEUR] DROP CONSTRAINT [FK_AGENT_DROIT_ACCES];
GO
IF OBJECT_ID(N'[dbo].[FK_ADRESSER_SERVICE]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ECHANGER] DROP CONSTRAINT [FK_ADRESSER_SERVICE];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ACCEDER]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ACCEDER];
GO
IF OBJECT_ID(N'[dbo].[CATEGORIE_COURRIER]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CATEGORIE_COURRIER];
GO
IF OBJECT_ID(N'[dbo].[CORRESPONDANT]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CORRESPONDANT];
GO
IF OBJECT_ID(N'[dbo].[COURRIER]', 'U') IS NOT NULL
    DROP TABLE [dbo].[COURRIER];
GO
IF OBJECT_ID(N'[dbo].[DROIT_ACCES]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DROIT_ACCES];
GO
IF OBJECT_ID(N'[dbo].[ECHANGER]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ECHANGER];
GO
IF OBJECT_ID(N'[dbo].[MODULE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MODULE];
GO
IF OBJECT_ID(N'[dbo].[NATURE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NATURE];
GO
IF OBJECT_ID(N'[dbo].[PARAMETRAGE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PARAMETRAGE];
GO
IF OBJECT_ID(N'[dbo].[SERVICE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SERVICE];
GO
IF OBJECT_ID(N'[dbo].[TYPE_CORRESPONDANT]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TYPE_CORRESPONDANT];
GO
IF OBJECT_ID(N'[dbo].[TYPE_COURRIER]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TYPE_COURRIER];
GO
IF OBJECT_ID(N'[dbo].[UTILISATEUR]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UTILISATEUR];
GO
IF OBJECT_ID(N'[dbo].[V_PROFIL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[V_PROFIL];
GO
IF OBJECT_ID(N'[dbo].[V_COURRIER]', 'U') IS NOT NULL
    DROP TABLE [dbo].[V_COURRIER];
GO
IF OBJECT_ID(N'[dbo].[ARCHIVAGE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ARCHIVAGE];
GO
IF OBJECT_ID(N'[dbo].[V_PROFIL_UTILISATEUR]', 'U') IS NOT NULL
    DROP TABLE [dbo].[V_PROFIL_UTILISATEUR];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ACCEDER'
CREATE TABLE [dbo].[ACCEDER] (
    [Id_acc] int IDENTITY(1,1) NOT NULL,
    [Id_drt] int  NOT NULL,
    [Id_mod] int  NOT NULL
);
GO

-- Creating table 'CATEGORIE_COURRIER'
CREATE TABLE [dbo].[CATEGORIE_COURRIER] (
    [Id_cat_cour] int IDENTITY(1,1) NOT NULL,
    [Lib_cat_cour] varchar(50)  NOT NULL
);
GO

-- Creating table 'CORRESPONDANT'
CREATE TABLE [dbo].[CORRESPONDANT] (
    [Id_cor] int IDENTITY(1,1) NOT NULL,
    [Civ_cor] varchar(4)  NULL,
    [Nom_cor] varchar(100)  NOT NULL,
    [Tel_cor] varchar(50)  NOT NULL,
    [Ad_cor] varchar(50)  NULL,
    [Mail_cor] varchar(100)  NULL,
    [Resp_cor] varchar(100)  NULL,
    [Ref_cor] varchar(100)  NULL,
    [Id_typ_cor] int  NOT NULL,
    [isDelete] bit  NULL,
    [dateDelete] datetime  NULL
);
GO

-- Creating table 'COURRIER'
CREATE TABLE [dbo].[COURRIER] (
    [Id_cour] int  NOT NULL,
    [Ref_cour] varchar(50)  NOT NULL,
    [Obj_cour] varchar(50)  NOT NULL,
    [Date_cour] datetime  NOT NULL,
    [Cont_cour] varchar(max)  NULL,
    [Lien_cour] varchar(max)  NULL,
    [Rep_cour] int  NULL,
    [Id_util] int  NOT NULL,
    [Id_nat] int  NOT NULL,
    [Id_cat_cour] int  NULL,
    [isDelete] bit  NULL,
    [dateDelete] datetime  NULL,
    [nom_fichier_joint] varchar(max)  NULL,
    [Stat_cour] varchar(50)  NULL,
    [Id_typ_cour] int  NULL
);
GO

-- Creating table 'DROIT_ACCES'
CREATE TABLE [dbo].[DROIT_ACCES] (
    [Id_drt] int IDENTITY(1,1) NOT NULL,
    [Lib_drt] varchar(50)  NOT NULL,
    [isDelete] bit  NULL,
    [dateDelete] datetime  NULL
);
GO

-- Creating table 'ECHANGER'
CREATE TABLE [dbo].[ECHANGER] (
    [Id_ech] int IDENTITY(1,1) NOT NULL,
    [Id_cour] int  NOT NULL,
    [Id_cor] int  NULL,
    [Id_serv] int  NULL,
    [Date_envoi] datetime  NOT NULL,
    [isDest] bit  NOT NULL
);
GO

-- Creating table 'MODULE'
CREATE TABLE [dbo].[MODULE] (
    [Id_mod] int IDENTITY(1,1) NOT NULL,
    [Lib_mod] varchar(50)  NOT NULL,
    [Role_cs] varchar(20)  NULL
);
GO

-- Creating table 'NATURE'
CREATE TABLE [dbo].[NATURE] (
    [Id_nat] int IDENTITY(1,1) NOT NULL,
    [Lib_nat] varchar(50)  NOT NULL,
    [isDelete] bit  NULL,
    [dateDelete] datetime  NULL
);
GO

-- Creating table 'PARAMETRAGE'
CREATE TABLE [dbo].[PARAMETRAGE] (
    [Nom_directeur] varchar(100)  NOT NULL,
    [Num_cour_ad] int  NOT NULL,
    [Num_cour_st] int  NOT NULL,
    [Id] int IDENTITY(1,1) NOT NULL
);
GO

-- Creating table 'SERVICE'
CREATE TABLE [dbo].[SERVICE] (
    [Id_serv] int IDENTITY(1,1) NOT NULL,
    [Lib_serv] varchar(50)  NOT NULL,
    [Ref_serv] varchar(100)  NOT NULL,
    [Resp_serv] varchar(100)  NOT NULL,
    [Civ_serv] varchar(4)  NULL,
    [isDelete] bit  NULL,
    [dateDelete] datetime  NULL
);
GO

-- Creating table 'TYPE_CORRESPONDANT'
CREATE TABLE [dbo].[TYPE_CORRESPONDANT] (
    [Id_typ_cor] int IDENTITY(1,1) NOT NULL,
    [Lib_typ_cor] varchar(50)  NOT NULL,
    [isDelete] bit  NULL,
    [dateDelete] datetime  NULL
);
GO

-- Creating table 'TYPE_COURRIER'
CREATE TABLE [dbo].[TYPE_COURRIER] (
    [Id_typ_cour] int IDENTITY(1,1) NOT NULL,
    [Lib_typ_cour] varchar(22)  NOT NULL
);
GO

-- Creating table 'UTILISATEUR'
CREATE TABLE [dbo].[UTILISATEUR] (
    [Id_util] int IDENTITY(1,1) NOT NULL,
    [Nom_util] varchar(50)  NOT NULL,
    [Login_util] varchar(15)  NOT NULL,
    [PasswdHash_util] varchar(100)  NOT NULL,
    [Id_drt] int  NOT NULL,
    [isActif_util] bit  NULL,
    [Date_creation_util] datetime  NULL,
    [IsUpdate] bit  NULL,
    [Email_util] varchar(100)  NULL,
    [isDelete] bit  NULL,
    [dateDelete] datetime  NULL
);
GO

-- Creating table 'V_PROFIL'
CREATE TABLE [dbo].[V_PROFIL] (
    [Id_drt] int  NOT NULL,
    [Lib_drt] varchar(50)  NOT NULL,
    [isDelete] bit  NULL,
    [Id_acc] int  NOT NULL,
    [Id_mod] int  NOT NULL,
    [Lib_mod] varchar(50)  NOT NULL,
    [Role_cs] varchar(20)  NULL
);
GO

-- Creating table 'V_COURRIER'
CREATE TABLE [dbo].[V_COURRIER] (
    [Id_cour] int  NOT NULL,
    [Ref_cour] varchar(50)  NOT NULL,
    [Obj_cour] varchar(50)  NOT NULL,
    [Date_cour] datetime  NOT NULL,
    [Cont_cour] varchar(max)  NULL,
    [Lien_cour] varchar(max)  NULL,
    [Rep_cour] int  NULL,
    [Id_util] int  NOT NULL,
    [Id_nat] int  NOT NULL,
    [Id_cat_cour] int  NULL,
    [isDelete] bit  NULL,
    [dateDelete] datetime  NULL,
    [nom_fichier_joint] varchar(max)  NULL,
    [Stat_cour] varchar(50)  NULL,
    [Id_typ_cour] int  NULL,
    [Lib_nat] varchar(50)  NOT NULL,
    [isDeleteNat] bit  NULL,
    [Lib_typ_cour] varchar(22)  NOT NULL,
    [Nom_util] varchar(50)  NOT NULL,
    [isActif_util] bit  NULL,
    [Lib_cat_cour] varchar(50)  NULL,
    [isDest] bit  NOT NULL,
    [Id_serv] int  NULL,
    [Id_cor] int  NULL,
    [Date_envoi] datetime  NOT NULL,
    [Lib_serv] varchar(50)  NULL,
    [Ref_serv] varchar(100)  NULL,
    [Resp_serv] varchar(100)  NULL,
    [Civ_serv] varchar(4)  NULL,
    [Civ_cor] varchar(4)  NULL,
    [Nom_cor] varchar(100)  NULL,
    [Resp_cor] varchar(100)  NULL,
    [Ref_cor] varchar(100)  NULL,
    [Lib_typ_cor] varchar(50)  NULL,
    [Id_typ_cor] int  NULL
);
GO

-- Creating table 'ARCHIVAGE'
CREATE TABLE [dbo].[ARCHIVAGE] (
    [Id_arch] int IDENTITY(1,1) NOT NULL,
    [Annee_arch] datetime  NOT NULL,
    [Ref_cour_arch] varchar(50)  NOT NULL,
    [Obj_cour_arch] varchar(50)  NOT NULL,
    [Date_cour_arch] datetime  NOT NULL,
    [Cont_cour_arch] varchar(max)  NULL,
    [Lien_cour_arch] varchar(max)  NULL,
    [Ref_rep_cour_arch] varchar(100)  NULL,
    [Nom_fichier_joint_arch] varchar(max)  NULL,
    [Stat_cour_arch] varchar(50)  NULL,
    [Lib_nat_arch] varchar(50)  NOT NULL,
    [Lib_typ_cour_arch] varchar(50)  NOT NULL,
    [Lib_cat_cour_arch] varchar(50)  NOT NULL,
    [Nom_util_arch] varchar(100)  NOT NULL,
    [Ref_exp_arch] varchar(100)  NOT NULL,
    [Destinataires_arch] varchar(max)  NOT NULL
);
GO

-- Creating table 'V_PROFIL_UTILISATEUR'
CREATE TABLE [dbo].[V_PROFIL_UTILISATEUR] (
    [Id_util] int  NOT NULL,
    [Nom_util] varchar(50)  NOT NULL,
    [PasswdHash_util] varchar(100)  NOT NULL,
    [Login_util] varchar(15)  NOT NULL,
    [isActif_util] bit  NULL,
    [Date_creation_util] datetime  NULL,
    [IsUpdate] bit  NULL,
    [Id_drt] int  NOT NULL,
    [Lib_drt] varchar(50)  NOT NULL,
    [isDelete] bit  NULL,
    [Id_acc] int  NOT NULL,
    [Id_mod] int  NOT NULL,
    [Lib_mod] varchar(50)  NOT NULL,
    [Role_cs] varchar(20)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id_acc] in table 'ACCEDER'
ALTER TABLE [dbo].[ACCEDER]
ADD CONSTRAINT [PK_ACCEDER]
    PRIMARY KEY CLUSTERED ([Id_acc] ASC);
GO

-- Creating primary key on [Id_cat_cour] in table 'CATEGORIE_COURRIER'
ALTER TABLE [dbo].[CATEGORIE_COURRIER]
ADD CONSTRAINT [PK_CATEGORIE_COURRIER]
    PRIMARY KEY CLUSTERED ([Id_cat_cour] ASC);
GO

-- Creating primary key on [Id_cor] in table 'CORRESPONDANT'
ALTER TABLE [dbo].[CORRESPONDANT]
ADD CONSTRAINT [PK_CORRESPONDANT]
    PRIMARY KEY CLUSTERED ([Id_cor] ASC);
GO

-- Creating primary key on [Id_cour] in table 'COURRIER'
ALTER TABLE [dbo].[COURRIER]
ADD CONSTRAINT [PK_COURRIER]
    PRIMARY KEY CLUSTERED ([Id_cour] ASC);
GO

-- Creating primary key on [Id_drt] in table 'DROIT_ACCES'
ALTER TABLE [dbo].[DROIT_ACCES]
ADD CONSTRAINT [PK_DROIT_ACCES]
    PRIMARY KEY CLUSTERED ([Id_drt] ASC);
GO

-- Creating primary key on [Id_ech] in table 'ECHANGER'
ALTER TABLE [dbo].[ECHANGER]
ADD CONSTRAINT [PK_ECHANGER]
    PRIMARY KEY CLUSTERED ([Id_ech] ASC);
GO

-- Creating primary key on [Id_mod] in table 'MODULE'
ALTER TABLE [dbo].[MODULE]
ADD CONSTRAINT [PK_MODULE]
    PRIMARY KEY CLUSTERED ([Id_mod] ASC);
GO

-- Creating primary key on [Id_nat] in table 'NATURE'
ALTER TABLE [dbo].[NATURE]
ADD CONSTRAINT [PK_NATURE]
    PRIMARY KEY CLUSTERED ([Id_nat] ASC);
GO

-- Creating primary key on [Id] in table 'PARAMETRAGE'
ALTER TABLE [dbo].[PARAMETRAGE]
ADD CONSTRAINT [PK_PARAMETRAGE]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id_serv] in table 'SERVICE'
ALTER TABLE [dbo].[SERVICE]
ADD CONSTRAINT [PK_SERVICE]
    PRIMARY KEY CLUSTERED ([Id_serv] ASC);
GO

-- Creating primary key on [Id_typ_cor] in table 'TYPE_CORRESPONDANT'
ALTER TABLE [dbo].[TYPE_CORRESPONDANT]
ADD CONSTRAINT [PK_TYPE_CORRESPONDANT]
    PRIMARY KEY CLUSTERED ([Id_typ_cor] ASC);
GO

-- Creating primary key on [Id_typ_cour] in table 'TYPE_COURRIER'
ALTER TABLE [dbo].[TYPE_COURRIER]
ADD CONSTRAINT [PK_TYPE_COURRIER]
    PRIMARY KEY CLUSTERED ([Id_typ_cour] ASC);
GO

-- Creating primary key on [Id_util] in table 'UTILISATEUR'
ALTER TABLE [dbo].[UTILISATEUR]
ADD CONSTRAINT [PK_UTILISATEUR]
    PRIMARY KEY CLUSTERED ([Id_util] ASC);
GO

-- Creating primary key on [Id_drt], [Lib_drt], [Id_acc], [Id_mod], [Lib_mod] in table 'V_PROFIL'
ALTER TABLE [dbo].[V_PROFIL]
ADD CONSTRAINT [PK_V_PROFIL]
    PRIMARY KEY CLUSTERED ([Id_drt], [Lib_drt], [Id_acc], [Id_mod], [Lib_mod] ASC);
GO

-- Creating primary key on [Id_cour], [Ref_cour], [Obj_cour], [Date_cour], [Id_util], [Id_nat], [Lib_nat], [Lib_typ_cour], [Nom_util], [isDest], [Date_envoi] in table 'V_COURRIER'
ALTER TABLE [dbo].[V_COURRIER]
ADD CONSTRAINT [PK_V_COURRIER]
    PRIMARY KEY CLUSTERED ([Id_cour], [Ref_cour], [Obj_cour], [Date_cour], [Id_util], [Id_nat], [Lib_nat], [Lib_typ_cour], [Nom_util], [isDest], [Date_envoi] ASC);
GO

-- Creating primary key on [Id_arch] in table 'ARCHIVAGE'
ALTER TABLE [dbo].[ARCHIVAGE]
ADD CONSTRAINT [PK_ARCHIVAGE]
    PRIMARY KEY CLUSTERED ([Id_arch] ASC);
GO

-- Creating primary key on [Id_util], [Nom_util], [PasswdHash_util], [Login_util], [Id_drt], [Lib_drt], [Id_acc], [Id_mod], [Lib_mod] in table 'V_PROFIL_UTILISATEUR'
ALTER TABLE [dbo].[V_PROFIL_UTILISATEUR]
ADD CONSTRAINT [PK_V_PROFIL_UTILISATEUR]
    PRIMARY KEY CLUSTERED ([Id_util], [Nom_util], [PasswdHash_util], [Login_util], [Id_drt], [Lib_drt], [Id_acc], [Id_mod], [Lib_mod] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Id_drt] in table 'ACCEDER'
ALTER TABLE [dbo].[ACCEDER]
ADD CONSTRAINT [FK_ACCEDER_DROIT_ACCES]
    FOREIGN KEY ([Id_drt])
    REFERENCES [dbo].[DROIT_ACCES]
        ([Id_drt])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ACCEDER_DROIT_ACCES'
CREATE INDEX [IX_FK_ACCEDER_DROIT_ACCES]
ON [dbo].[ACCEDER]
    ([Id_drt]);
GO

-- Creating foreign key on [Id_mod] in table 'ACCEDER'
ALTER TABLE [dbo].[ACCEDER]
ADD CONSTRAINT [FK_ACCEDER_MODULE]
    FOREIGN KEY ([Id_mod])
    REFERENCES [dbo].[MODULE]
        ([Id_mod])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ACCEDER_MODULE'
CREATE INDEX [IX_FK_ACCEDER_MODULE]
ON [dbo].[ACCEDER]
    ([Id_mod]);
GO

-- Creating foreign key on [Id_cat_cour] in table 'COURRIER'
ALTER TABLE [dbo].[COURRIER]
ADD CONSTRAINT [FK_COURRIER_CATEGORIE_COURRIER]
    FOREIGN KEY ([Id_cat_cour])
    REFERENCES [dbo].[CATEGORIE_COURRIER]
        ([Id_cat_cour])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_COURRIER_CATEGORIE_COURRIER'
CREATE INDEX [IX_FK_COURRIER_CATEGORIE_COURRIER]
ON [dbo].[COURRIER]
    ([Id_cat_cour]);
GO

-- Creating foreign key on [Id_cor] in table 'ECHANGER'
ALTER TABLE [dbo].[ECHANGER]
ADD CONSTRAINT [FK_ADRESSER_CORRESPONDANT]
    FOREIGN KEY ([Id_cor])
    REFERENCES [dbo].[CORRESPONDANT]
        ([Id_cor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ADRESSER_CORRESPONDANT'
CREATE INDEX [IX_FK_ADRESSER_CORRESPONDANT]
ON [dbo].[ECHANGER]
    ([Id_cor]);
GO

-- Creating foreign key on [Id_typ_cor] in table 'CORRESPONDANT'
ALTER TABLE [dbo].[CORRESPONDANT]
ADD CONSTRAINT [FK_CORRESPONDANT_TYPE_CORRESPONDANT]
    FOREIGN KEY ([Id_typ_cor])
    REFERENCES [dbo].[TYPE_CORRESPONDANT]
        ([Id_typ_cor])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CORRESPONDANT_TYPE_CORRESPONDANT'
CREATE INDEX [IX_FK_CORRESPONDANT_TYPE_CORRESPONDANT]
ON [dbo].[CORRESPONDANT]
    ([Id_typ_cor]);
GO

-- Creating foreign key on [Id_cour] in table 'ECHANGER'
ALTER TABLE [dbo].[ECHANGER]
ADD CONSTRAINT [FK_ADRESSER_COURRIER]
    FOREIGN KEY ([Id_cour])
    REFERENCES [dbo].[COURRIER]
        ([Id_cour])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ADRESSER_COURRIER'
CREATE INDEX [IX_FK_ADRESSER_COURRIER]
ON [dbo].[ECHANGER]
    ([Id_cour]);
GO

-- Creating foreign key on [Id_util] in table 'COURRIER'
ALTER TABLE [dbo].[COURRIER]
ADD CONSTRAINT [FK_COURRIER_AGENT]
    FOREIGN KEY ([Id_util])
    REFERENCES [dbo].[UTILISATEUR]
        ([Id_util])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_COURRIER_AGENT'
CREATE INDEX [IX_FK_COURRIER_AGENT]
ON [dbo].[COURRIER]
    ([Id_util]);
GO

-- Creating foreign key on [Id_nat] in table 'COURRIER'
ALTER TABLE [dbo].[COURRIER]
ADD CONSTRAINT [FK_COURRIER_NATURE]
    FOREIGN KEY ([Id_nat])
    REFERENCES [dbo].[NATURE]
        ([Id_nat])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_COURRIER_NATURE'
CREATE INDEX [IX_FK_COURRIER_NATURE]
ON [dbo].[COURRIER]
    ([Id_nat]);
GO

-- Creating foreign key on [Id_typ_cour] in table 'COURRIER'
ALTER TABLE [dbo].[COURRIER]
ADD CONSTRAINT [FK_COURRIER_TYPE_COURRIER_DEPART]
    FOREIGN KEY ([Id_typ_cour])
    REFERENCES [dbo].[TYPE_COURRIER]
        ([Id_typ_cour])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_COURRIER_TYPE_COURRIER_DEPART'
CREATE INDEX [IX_FK_COURRIER_TYPE_COURRIER_DEPART]
ON [dbo].[COURRIER]
    ([Id_typ_cour]);
GO

-- Creating foreign key on [Id_drt] in table 'UTILISATEUR'
ALTER TABLE [dbo].[UTILISATEUR]
ADD CONSTRAINT [FK_AGENT_DROIT_ACCES]
    FOREIGN KEY ([Id_drt])
    REFERENCES [dbo].[DROIT_ACCES]
        ([Id_drt])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AGENT_DROIT_ACCES'
CREATE INDEX [IX_FK_AGENT_DROIT_ACCES]
ON [dbo].[UTILISATEUR]
    ([Id_drt]);
GO

-- Creating foreign key on [Id_serv] in table 'ECHANGER'
ALTER TABLE [dbo].[ECHANGER]
ADD CONSTRAINT [FK_ADRESSER_SERVICE]
    FOREIGN KEY ([Id_serv])
    REFERENCES [dbo].[SERVICE]
        ([Id_serv])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ADRESSER_SERVICE'
CREATE INDEX [IX_FK_ADRESSER_SERVICE]
ON [dbo].[ECHANGER]
    ([Id_serv]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------