﻿var ExportFileName;

function functionShowPresentSsMenu(class_ss_menu) {
    $('.sidebar .menu ul.list > li a').removeClass('toggled');
    $('.sidebar .menu ul.list > li > ul.ml-menu > li').removeClass('active');

    var selecteur = `.sidebar .menu ul.list  li.${class_ss_menu}`;
    var sous_menu = $(selecteur);
    if (sous_menu) {
        var is_ul_parent = $(sous_menu).parent("ul").length == 0 ? false : true;

        $(sous_menu).children('a').addClass('toggled');
        if (is_ul_parent) {
            $(sous_menu).addClass('active');
            //$(sous_menu).parents('li').children('a.menu-toggle').click();
            $(sous_menu).parents('li').children('a.menu-toggle').addClass('toggled');
        }
    }
}
function MakeDate(lingDate){
    if(lingDate!=null){
        var int=parseInt(lingDate.substring(6));
        var date=new Date(int);
        return date.toLocaleDateString();
    }
    else return "";
}

function GetTomorrowDate(){
    var day=new Date();
    var tomorrow=new Date(day);
    tomorrow.setDate(tomorrow.getDate()+1);
    return tomorrow;
}

function FormatNowDate(boolWithTime=false) {

    let today = new Date();
    let y = today.getFullYear();
    let m = today.getMonth() + 1;
    let j = today.getDate();
    let h = today.getHours();
    let min = today.getMinutes();
    if (m < 10) { m = "0" + m };
    if (j < 10) { j = "0" + j };
    if (h < 10) { h = "0" + h };
    if (min < 10) { min = "0" + min };
    if(boolWithTime==false) return (j + '-' + m + '-' + y)
    else return (j + '-' + m + '-' + y + " " + h + ":" + min);
}

function getExportFileName(buttonClass, defaultFileName) {
    if (defaultFileName == undefined) defaultFileName = "Liste du " + FormatNowDate();
    swal({
        title: "Nom du fichier à exporter!",
        text: "Entrez le nom du fichier à exporter svp!!!",
        type: "input",
        showCancelButton: true,
        cancelButtonText: "Annuler",
        closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "Entrez le nom du fichier svp !!!",
        inputValue: defaultFileName
    },function (inputValue) {
        if (inputValue === false) return false;
        if (inputValue.trim() === "") {
            swal.showInputError("Vous devez renseigner un nom de fichier!!!"); 
            return false
        }
        ExportFileName = inputValue;
        $(buttonClass).click();
        swal.close();
    });
}

function CreateDatatableWithExportButton(table_selector, nom_fichier,addButton,exportOption) {
    let selector=`${table_selector} tr:first th, ${table_selector} tr:first td`;
    let length=$(selector).length;
    var tab=[];
    for(i=0;i<length-1;i++){
        tab.push(i);
    }
    exportOption = exportOption != null ? exportOption : { columns: tab };
    nom_fichier=nom_fichier==null?"Liste du "+FormatNowDate():nom_fichier;
    let buttons=[
        {
            text: 'Excel',
            className: "waves-effect",
            action: function () { getExportFileName(".btn-excel", nom_fichier); }
        },
        {
            text: 'Pdf',
            className: "waves-effect",
            action: function () { getExportFileName(".btn-pdf", nom_fichier); }
        },
        {
            text: 'Imprimer',
            className: "waves-effect",
            action: function () { getExportFileName(".btn-print", nom_fichier); }
        },
        {
            extend: 'excel',
            exportOptions:exportOption,
            className: "waves-effect btn-excel",
            filename: function () { return ExportFileName }
        },
        {
            extend: 'pdf',
            className: "waves-effect btn-pdf",
            exportOptions:exportOption,
            filename: function () { return ExportFileName },
            title: function () { return ExportFileName },
        },
        {
            extend: 'print',
            className: "waves-effect btn-print",
            exportOptions:exportOption,
            filename: function () { return ExportFileName },
            text: "IMPRIMER",
            extension: 'filepdf.pdf',
            //autoPrint: false,
            title: function () { return ExportFileName },
            customize: function (win) {
                console.log(win);
                $(win.document.body).css('font-size', '10pt')
                $(win.document.body).find('table').addClass('compact').css('font-size', '10pt');
            }
        }
    ];
    if(addButton!=undefined)buttons.unshift(addButton);

    let dataTable = $(table_selector).DataTable({
        "language": {
            "url": "../../Plugins/jquery-datatable/French.json"
        },
        dom: 'lBfrtip',
        responsive: true,
        "autoWidth": false,
        "pageLength": 10,
        "lengthMenu": [10, 25, 50, 75, 100],
        //"columnDefs": [
        //    {
        //        "targets": [0],
        //        "visible": false,
        //        "searchable": false
        //    },
        //],
        buttons:buttons
    });
    return dataTable;
}

function ShowSuccessMessage(text="Information enregistrée avec succès",title="Information",timer=3000) {
    swal({
        title: title,
        type: "success",
        timer: timer, 
        text: text,
        closeOnClickOutside: true,
        html: true
    });
}

function ShowConfirmSupMessage(action,text="Etes-vous sur de vouloir supprimer cette information?",title="Confirmation de suppression !!!"){
    if(typeof(action)!=="function"){
        console.log("Vous n'avez pas definit l'action a executer en cas de confirmation. Il doit s'agir d'une fonction");
    }
    else{
        swal({
            title: title,
            text: text,
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Annuler",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Supprimer",
            closeOnConfirm: false,
            closeOnClickOutside: false,
            html: true
            //timer: 3000,
        }, function () {
            action();
            //swal.close();
        });
    }
}
function ShowConfirmMessage(action,text="Etes-vous sûr de vouloir poursuivre cette action?",title="Demande de confirmation",confirmText="OUI",cancelText="NON"){
    if(typeof(action)!=="function"){
        console.log("Vous n'avez pas definit l'action a executer en cas de confirmation. Il doit s'agir d'une fonction");
    }
    else{
        swal({
            title: title,
            text: text,
            type: "warning",
            showCancelButton: true,
            cancelButtonText: cancelText,
            confirmButtonColor: "#DD6B55",
            confirmButtonText:confirmText,
            closeOnConfirm: false,
            closeOnClickOutside: false,
            html: true
            //timer: 3000,
        }, function () {
            action();
            //swal.close();
        });
    }
}

function ShowWarningMessage(text){
    swal({
        title:"Attention!!!",
        text:text,
        type:"warning",
        html: true
    });
}

function ShowErrorMessage(text){
    swal({
        title:"Erreur!!!",
        text:text,
        type:"error",
        html: true
    });
}

function ShowMessageWithCallBack(text,title,type,action){
    if(typeof(action)!="function"){
        console.log("callback non definit");
    }
    else{
        swal({
            title:title,
            text:text,
            type:type,
            html: true
        },function(){action();});
    }
}