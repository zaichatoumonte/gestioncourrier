﻿functionShowPresentSsMenu('opt13');

const addButton = {
    text: 'Ajouter',
    className: "btn-ajout btn-primary waves-effect",
    titleAttr: 'Ajouter d\'un service',
    action: function () {
        $("#Modal").modal('show');
        ShowModal(0, "add");
    }
};

dataTable = CreateDatatableWithExportButton('.dataTable', "Liste des services du " + FormatNowDate(), addButton);

//$(window).on("load", function () {
//    dataTable.button().add(0, {
//        text: 'Ajouter',
//        className: "btn-ajout btn-primary waves-effect",
//        titleAttr: 'Ajouter d\'un service',
//        action: function () {
//            $("#Modal").modal('show');
//            ShowModal(0, "add");
//        }
//    });
//});

$("table").on('click', '.btn-mod', function () {
    tr_modif = $(this).parents('tr:first');
});
$("table").on('click', '.btn-sup', function () {
    tr_sup = $(this).parents('tr:first');
});

function ShowModal(etat, id, lib, resp, civ) {
    etat_action = etat;
    if (etat_action === 'modifier') {
        $("#Modal .modal-title").text("Modification d'un service");
        $("#id").val(id);
        $("#lib").val(lib).parents('div:first').addClass('focused');
        $("#resp").val(resp).parents('div:first').addClass('focused');
        $("#civ").val(civ).selectpicker("refresh").parents('div:first').addClass('focused');
    }
    else {
        $("#id").val("");
        $("#Modal .modal-title").text("Nouveau service");
        $("#Modal select").val("").selectpicker('refresh');
        $("#Modal form input[type=text]").val("").parents('div.focused').removeClass('focused');
    }
}

function ConfirmSup(id) {
    function action() {
        var url = url_delete.replace('0', id);
        $.get(url).done(function (data) {
            if (data.state) {
                dataTable.row(tr_sup).remove().draw();
                ShowSuccessMessage("Service supprimé avec succès!!!");
            }
            else {
                ShowErrorMessage(data.message);
            }
        });
    }
    ShowConfirmSupMessage(action, "Etes-vous sur de vouloir supprimer ce service");
}

$("#btn_valid").on("click", function (e) {
    e.preventDefault();
    var lib = $("#lib").val().trim();
    var resp = $("#resp").val().trim();
    var civ = $("#civ").val().trim();
    var id = $("#id").val();
    var antiforget = $("#Modal form input[name=__RequestVerificationToken]").val();
    if (lib === "") {
        ShowWarningMessage("Vous n'avez pas renseigné le libellé du service!!!");
    }
    else if (resp === "") {
        ShowWarningMessage("Vous n'avez pas renseigé le responsable du service!!!");
    }
    else if (civ === "") {
        ShowWarningMessage("Vous n'avez pas renseigé la civilité du responsable du service!!!");
    }
    else {
        $.post(url_post_action, { service: { Id_serv: id, Lib_serv: lib, Resp_serv: resp, Civ_serv: civ }, __RequestVerificationToken: antiforget }).done(function (data) {
            if (data.state == false) ShowErrorMessage(data.message);
            else {
                if (etat_action == "modifier") {
                    dataTable.row(tr_modif).remove();
                }
                var service = data.service;
                dataTable.row.add([
                    service.Lib_serv,
                    service.Civ_serv + " " + service.Resp_serv,
                    `
                            <button type="button" class ="btn bg-cyan btn-mod" data-toggle="modal" data-target="#Modal" onclick="ShowModal('modifier',${service.Id_serv}, '${service.Lib_serv}','${service.Resp_serv}','${service.Civ_serv}') "><i class ="fa fa-edit"></i>Modifier</button>
                            <button type="button" class ="btn btn-danger btn-sup" onclick="ConfirmSup(${service.Id_serv})"><i class ="fa fa-trash"></i>Supprimer</button>
                            `
                ]).draw(false);
                $("#Modal").modal('hide');
                ShowSuccessMessage("Information enregistrée avec succès!!!");
            }
        }).fail(function (e) { console.log(e); });
    }
});