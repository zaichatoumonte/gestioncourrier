﻿functionShowPresentSsMenu('opt14');
const addButton = {
    text: 'Ajouter',
    className: "btn-ajout btn-primary waves-effect",
    titleAttr: 'Ajouter un type de correspondant',
    action: function () {
        $("#Modal").modal('show');
        ShowModal(0, "add");
    }
};
let texte="Liste des types de correspondants du " + FormatNowDate();
dataTable = CreateDatatableWithExportButton('.dataTable', texte, addButton);

//$(window).on("load", function () {
//    dataTable.button().add(0, {
//        text: 'Ajouter',
//        className: "btn-ajout btn-primary waves-effect",
//        titleAttr: 'Ajouter un type de correspondant',
//        action: function () {
//            $("#Modal").modal('show');
//            ShowModal(0, "add");
//        }
//    });
//    alert("ok");
//});

$("table").on('click', '.btn-mod', function () {
    tr_modif = $(this).parents('tr:first');
});

$("table").on('click', '.btn-sup', function () {
    tr_sup = $(this).parents('tr:first');
});

function ShowModal(id, etat, lib) {
    etat_action = etat;
    if (etat_action === 'modifier') {
        $("#Modal .modal-title").text("Modification d'un Type de correspondant");
        $("#id").val(id);
        $("#lib").val(lib).parents('div:first').addClass('focused');
    }
    else {
        $("#id").val("");
        $("#Modal .modal-title").text("Nouveau Type de correspondant");
        $("#Modal form input[type=text]").val("").parents('div.focused').removeClass('focused');
    }
}

function ConfirmSup(id) {
    function action() {
        var url = url_delete.replace('0', id);
        $.get(url).done(function (data) {
            if (data.state) {
                dataTable.row(tr_sup).remove().draw();
                ShowSuccessMessage("Type de Correspondant supprimé avec succès!!!");
            }
            else {
                ShowErrorMessage(data.message);
            }
        });
    }
    ShowConfirmSupMessage(action, "Etes-vous sur de vouloir supprimer ce type de correspondant?");
}

$("#btn_valid").on("click", function (e) {
    e.preventDefault();
    var lib = $("#lib").val().trim();
    var id = $("#id").val();
    var antiforget = $("#Modal form input[name=__RequestVerificationToken]").val();
    if (lib === "") {
        ShowWarningMessage("Vous n'avez pas renseigner le libellé du type de correspondant!!!");
    }
    else {
        $.post(url_post_action, { types: { Id_typ_cor: id, Lib_typ_cor: lib }, __RequestVerificationToken: antiforget }).done(function (data) {
            if (data.state == false) ShowErrorMessage(data.message);
            else {
                if (etat_action == "modifier") {
                    dataTable.row(tr_modif).remove();
                }
                var type = data.types;
                dataTable.row.add([
                    type.Lib_typ_cor,
                    `
                        <button type="button" class ="btn bg-cyan btn-mod" data-toggle="modal" data-target="#Modal" onclick="ShowModal(${type.Id_typ_cor}, 'modifier','${type.Lib_typ_cor}') "><i class ="fa fa-edit"></i>Modifier</button>
                        <button type="button" class ="btn btn-danger btn-sup" onclick="ConfirmSup(${type.Id_typ_cor})"><i class ="fa fa-trash"></i>Supprimer</button>
                    `
                ]).draw(false);
                $("#Modal").modal('hide');
                ShowSuccessMessage();
            }
        }).fail(function (e) { console.log(e); });
    }
});