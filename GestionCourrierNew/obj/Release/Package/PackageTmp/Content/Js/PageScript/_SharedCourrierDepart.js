﻿//alternance des modal d'ajout de contact et courrier
function functionShowModalContact(HtmlId) {
    var modal_contact_id = "#" + HtmlId;
    $("#Modal").modal("hide");
    $(modal_contact_id).modal("show");
    $(modal_contact_id).on('hidden.bs.modal', function () {
        $(modal_contact_id + " form input:not([name=__RequestVerificationToken])").val("");
        $("#Modal").modal('show');
    });
}

function AfficherDetail(id,ref) {
    if (ref != null) {
        $(".title_modal_detail").html("Détail sur la reponse au courrier: <span style='color:red'>" + ref+"</span>");
    }
    else {
        $(".title_modal_detail").html("Détail sur un courrier");
    }
    var url = url_edit.replace("0", id);
    $.get(url)
        .done(function (result) {
            if (result.state) {
                var courrier = result.courrier_depart;
                $("#modal_detail .code").val(courrier.Ref_cour).parents('div:first').addClass('focused');
                $("#modal_detail .objet").val(courrier.Obj_cour).parents('div:first').addClass('focused');
                $("#modal_detail .nature").val(courrier.nature).parents('div:first').addClass('focused');
                $("#modal_detail .date").val(courrier.date).parents('div:first').addClass('focused');
                $("#modal_detail .type").val(courrier.Lib_cat_cour).parents('div:first').addClass('focused');
                $("#modal_detail .agent").val(courrier.agent).parents('div:first').addClass('focused');
                $("#modal_detail .statut").val(courrier.statut).parents('div:first').addClass('focused');
                $("#modal_detail .contenu").val(courrier.Cont_cour).parents('div:first').addClass('focused');
                $("#modal_detail input.fichier").val(courrier.nom_fichier == "" ? "Aucun fichier joint" : courrier.nom_fichier).parents('div:first').addClass('focused');
                if (courrier.chemin == "") {
                    $("#modal_detail a.fichier").addClass("disabled").attr("href", "#");
                }
                else {
                    $("#modal_detail a.fichier").attr("href", courrier.chemin).removeClass("disabled");
                }
                var contact_dest = courrier.list_contact;
                var service_dest = courrier.list_service;

                $("#modal_detail .dest_service,#modal_detail .dest_contact").html("");
                if (contact_dest.length == 0) {
                    $("#modal_detail .dest_contact").html("Aucun");
                }
                if (service_dest.length == 0) {
                    $("#modal_detail .dest_service").html("Aucun");
                }
                for(var item of contact_dest) {
                    $("#modal_detail .dest_contact").append(`-${item.Nom_cor}<br/>`);
                }
                for(var item of service_dest) {
                    $("#modal_detail .dest_service").append(`-${item.Lib_serv}<br/>`);
                }
            }
            else {
                swal("Erreur", e.message, "error");
            }
        })
        .fail(function () { swal("Erreur", "La requête n'a pas pu être traitée!!!", "error"); });
}

function ShowModal(etat, id_dep, btn , id_arr, ref_cour) {
    etat_action = etat;

    $("#ig_checkbox,#ig_checkbox2,#code").attr('disabled', false);
    $("label[for=ig_checkbox]").click();
    //annuler le css en cas d erreur
    $("#form li[role=tab]").removeClass("error");

    //retour a la premiere etape
    while ($("#form").children("div").steps('getStep', 0) != $("#form").children("div").steps('getCurrentStep', 1)) {
        $("#form").children("div").steps("previous");
    }

    if (etat_action === 'modifier' || etat_action==="valider") {
        if (id_arr != null && id_arr != undefined && id_arr != 0) {
            $("#Modal .modal-title").text("Modification de la reponse au courrier " + ref_cour);
        }
        else {
            id_arr = 0;
            if (etat_action === "valider") {
                $("#Modal .modal-title").text("Validation d'un courrier depart");
            }
            else {
                $("#Modal .modal-title").text("Modification d'un courrier depart");
            }
        }
        var url = url_edit.replace("0", id_dep);
        tr_modif = $(btn).parents("tr:first");
        $.get(url).done(function (data) {
            if (data.state == true) {
                courrier = data.courrier_depart;
                var code = courrier.Ref_cour;
                var code_fin = code.substring(code.lastIndexOf("/") + 1);
                var code_debut = code.split(code_fin)[0];

                $("#id_cour_dep").val(courrier.Id_cour);
                $("#id_cour_arr").val(id_arr);
                $("#objet").val(courrier.Obj_cour).parents('div:first').addClass('focused');

                $("#code").val(code_fin).attr('disabled', true).parents('div:first').addClass('focused');
                $("#code_debut").val(code_debut).attr('disabled', true).parents('div:first').addClass('focused');

                $("#nature").selectpicker("val", courrier.Id_nat);
                $("#user").val(courrier.agent).parents('div:first').addClass('focused');
                $("#date").val(courrier.date).selectpicker('refresh');
                $("#type").val(courrier.Lib_cat_cour).parents('div:first').addClass('focused');
                $("#file").val("");
                CKEDITOR.instances.contenu.setData(courrier.Cont_cour);

                if (courrier.Lib_cat_cour.toLowerCase().includes("transmis")) {
                    $("#Modal label[for=ig_checkbox]").click();
                    $("#ig_checkbox,#ig_checkbox2").attr('disabled', true);
                }
                else {
                    $("#Modal label[for=ig_checkbox2]").click();
                    $("#ig_checkbox,#ig_checkbox2").attr('disabled', true);
                }

                var service = $.map(courrier.list_service, function (item) { return item.Id_serv });
                $("#service").val(service).selectpicker("refresh");

                var contact = $.map(courrier.list_contact, function (item) { return item.Id_cor });
                $("#contacts").val(contact).multiSelect("refresh");
            }
            return false;
        });
    }
    else {
        $("#id_cour_dep").val(id_dep);
        $("#id_cour_arr").val(id_arr);
        if (id_arr == null || id_arr == 0) {
            $("#Modal .modal-title").text("Nouveau courrier départ");
        }
        else {
            $("#Modal .modal-title").text("Envoi d'une reponse au courrier : " + ref_cour);
        }
        $("#Modal select:not(#contacts)").val("").selectpicker('refresh');
        $('#contacts').multiSelect('deselect_all');
        $("#Modal form input:not([type=hidden]").val("").parents('div.focused').removeClass('focused');
        $("#user").val(user).parent('div:first').addClass('focused');
        $("#date").val(date_today).parent('div:first').addClass('focused');
        $("#form input[type=radio][name=type_courrier]").attr("disabled", false);
        $("#code").attr("disabled", false);
        CKEDITOR.instances.contenu.setData();
    }
}

//validation du formulaire et sauvegarde des donnees
$('#form').children('div').steps({
    headerTag: 'h2',
    bodyTag: 'section',
    transitionEffect: 'slideLeft',
    labels: {
        cancel: "Annuler",
        current: "current step:",
        pagination: "Pagination",
        finish: "Valider",
        next: "Suivant",
        previous: "Précédent",
        loading: "Chargement..."
    },
    onInit: function (event, currentIndex) {
        setButtonWavesEffect(event);
    },
    onStepChanged: function (event, currentIndex, priorIndex) {
        setButtonWavesEffect(event);
    },
    onStepChanging: function (event, currentIndex, newIndex) {
        //bouton next
        if (currentIndex < newIndex) {
            var step = $("#form").children("div").steps("getCurrentStep").title.toLowerCase();
            if (step.includes("type")) {
                var categorie = $("#form input[type=radio][name=type_courrier]:checked").data("value");
                if ($("#form input[type=radio][name=type_courrier]:checked").attr("disabled") != "disabled") {
                    var url = url_get_code.replace("0", categorie);
                    $.get(url)
                        .done(function (result) {
                            if (result.state) {
                                $("#code_debut").val(result.code == null ? "" : result.code).parents('div:first').addClass('focused');
                                if (categorie == "st") {
                                    $("#type").val("SOIT TRANSMIS").parents('div:first').addClass('focused');
                                }
                                else {
                                    $("#type").val("COURRIER ADMINISTRATIF").parents('div:first').addClass('focused');
                                }
                                return true;
                            }
                            else {
                                swal("Erreur", "Le code du courrier n'a pu etre determiné", "error");
                                return false;
                            }
                        }).fail(function () {
                            swal("Erreur", "Le code du courrier n'a pu etre determiné", "error");
                            return false;
                        });
                }
                return true;
            }
            else if (step.includes("information")) {
                var code = $("#code").val().trim();
                var objet = $("#objet").val().trim();
                var nature = $("#nature").selectpicker("val");
                if (code == "" || objet == "" || nature == null ) {
                    if (code === "") {
                        swal("Attention", "Veuillez Renseigner le code du courrier svp!!!", "warning");
                    }
                    else if (objet === "") {
                        swal("Attention", "Veuillez Renseigner l'objet du courrier svp!!!", "warning");
                    }
                    else if (nature === null) {
                        swal("Attention", "Veuillez Renseigner la nature du courrier svp!!!", "warning");
                    }
                    return false;
                }
                else if (code.lastIndexOf("/") != -1) {
                    swal("Attention", "Le code de completion du code courrier ne peut pas contenir de /", "warning");
                    return false;
                }
                else {
                    return true;
                }
            }
            else if (step.includes("redaction")) {
                var contenu = $(CKEDITOR.instances.contenu.getData()).text().trim();
                if (contenu == "") {
                    swal("Attention", "Veuillez Renseigner le contenu du courrier svp!!!", "warning");
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    },
    onFinishing: function (event, currentIndex) {
        var services = $("#service").val();
        var contacts = $("#contacts").val();

        if (services == null && contacts == null) {
            swal("Attention", "Vous devez sélectionner au moins un destinataire", "warning");
            return false;
        }
        else {
            var formdata = new FormData();
            var id_dep = $("#id_cour_dep").val();
            var id_arr = $("#id_cour_arr").val();
            var code_debut = $("#code_debut").val();
            var code = $("#code").val().trim();
            var objet = $("#objet").val().trim();
            var nature = $("#nature").selectpicker("val");
            var categorie = $("#type").val();
            var contenu = CKEDITOR.instances.contenu.getData();
            var antiforget = $("#Modal form input[name=__RequestVerificationToken]").val();

            formdata.append("Id_cour", id_dep);
            formdata.append("id_courrier", id_arr);
            formdata.append("Id_nat", nature);
            formdata.append("Ref_cour", code_debut+code);
            formdata.append("Obj_cour", objet);
            formdata.append("typ_cour", categorie);
            formdata.append("Cont_cour", window.escape(contenu));
            formdata.append("Stat_cour", statut||null);
            if (services != null) {
                for(var item of services) {
                    formdata.append("services[]", item);
                }
            }
            if (contacts != null) {
                for(var item of contacts) {
                    formdata.append("contacts[]", item);
                }
            }
            formdata.append("__RequestVerificationToken", antiforget);

            var fileInput = $("#file")[0];
            for (i = 0; i < fileInput.files.length; i++) {
                formdata.append(fileInput.files[i].name, fileInput.files[i]);
            }
            $.ajax({
                url: url_post_action,
                data: formdata,
                type: "post",
                contentType: false,
                processData: false,
                success: function (result) {
                    if (typeof (ActualiseDataTable) === "function") {
                        ActualiseDataTable(result);
                    }
                    else {
                        console.log("La fonction ActualiseDatatable n'a pas été défint");
                    }
                },
            });
        }
    },
    onFinished: function (event, currentIndex) {
        $("#Modal").modal("hide");
        return true;
    }
});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}

//initialisation des plugins
$(function () {
    //Multi-select
    $('#contacts').multiSelect({ selectableOptgroup: true });

    //wywiwig
    CKEDITOR.addCss('.cke_editable { font-size: 15px; padding: 2em; }');

    CKEDITOR.replace('contenu', {
        uiColor: '#CCEAEE',
        language: 'fr',
        toolbar: [
            //{
            //    name: 'document',
            //    items: ['Print']
            //},
          {
              name: 'clipboard',
              items: ['Undo', 'Redo']
          },
          {
              name: 'styles',
              items: ['Format', 'Font', 'FontSize']
          },
          //{
          //    name: 'colors',
          //    items: ['TextColor', 'BGColor']
          //},
          {
              name: 'align',
              items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
          },
          '/',
          {
              name: 'basicstyles',
              items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting']
          },
          //{
          //    name: 'links',
          //    items: ['Link', 'Unlink']
          //},
          {
              name: 'paragraph',
              items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
          },
          {
              name: 'insert',
              items: ['Table']/*'Image',*/
          },
          {
              name: 'tools',
              items: ['Maximize']
          },
          //{
          //    name: 'editing',
          //    items: ['Scayt']
          //}
        ],

        extraAllowedContent: 'h3{clear};h2{line-height};h2 h3{margin-left,margin-top}',
        extraPlugins: 'print,format,font,colorbutton,justify',/*,uploadimage*/
  
    });
});

function ActualiseContactList(contact) {
    var tab = [];
    $("#contacts optgroup").each(function (i, item) { tab.push($(item).attr("label")) });
    if (tab.includes(contact.Lib_typ_cor)) {
        var sel = `#contacts optgroup[label=${contact.Lib_typ_cor}]`;
        $(sel).append(`<option value="${contact.Id_cor}" selected>${contact.Nom_cor}</option>`);
        $('#contacts').multiSelect('refresh');
    }
    else {
        $("#contacts").append(`<optgroup label='${contact.Lib_typ_cor}'><option value='${contact.Id_cor}' selected>${contact.Nom_cor}</option></optgroup>`);
        $('#contacts').multiSelect('refresh')
    }
    swal({
        title: "Félicitation",
        text: "Correspondant enregistrée avec succès!!!",
        type: "success"
    }, function () {
        $("#modal_contact").modal('hide');
    });
}

