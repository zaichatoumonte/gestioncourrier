﻿function AddContact(e){
    e.preventDefault();
    var lib = $("#lib_contact").val().trim();
    var resp = $("#resp_contact").val().trim();
    var civ = $("#civ_contact").val().trim();
    var id = $("#id_contact").val();
    var mail = $("#mail_contact").val().trim();
    var ad = $("#ad_contact").val().trim();
    var typ = $("#typ_contact").val();
    var tel = $("#tel_contact").val().trim();
    var antiforget = $("#lib_contact").parents("form:first").children("input[name=__RequestVerificationToken]").val();
    let message = "";
    if (lib == "") {
        message="Vous n'avez pas renseigner le nom du correspondant!!!";
    }
    else if(civ==""){
        message="Vous n'avez pas renseigner civilité du correspondant ou du responsable de l'entreprise!!!";
    }
    else if (tel == "" || $("#tel_contact").is(":invalid")) {
        message="Numéro de télephone non valide!!!";
    }
    else if(typ==""){
        message="Vous n'avez pas renseigner le type de correspondant!!!";
    }
    else if (mail != "" && $("#mail_contact").is(":invalid")) {
        message = "Veuillez entrer une adresse mail correcte svp!!!";
    }

    if(message!=""){
        ShowWarningMessage(message);
    }
    else if (url_post_ajoutContact==undefined) {
        console.log("Url d'ajout d'un correspondant n'a pas été defini");
    }
    else {
        
        $.post(url_post_ajoutContact, {
            contact: {
                Id_cor: id,
                Nom_cor: lib,
                Resp_cor: resp,
                Civ_cor: civ,
                Tel_cor: tel,
                Ad_cor: ad,
                Mail_cor: mail,
                Id_typ_cor: typ
            }, 
            __RequestVerificationToken: antiforget
        })
            .done(function (data) {
                if (data.state == false)ShowErrorMessage(data.message);
                else {
                    var contact = data.contact;
                    ActualiseContactList(contact);
                }
            })
            .fail(function (e) {
                ShowErrorMessage("correspondant non ajouté.Reessayer ultérieurement!!!");
            });
    }
}

