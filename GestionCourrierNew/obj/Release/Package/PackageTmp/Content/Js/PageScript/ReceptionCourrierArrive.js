﻿functionShowPresentSsMenu('opt3');

//creation des datatbles

const addButton = {
    text: 'Ajouter',
    className: "btn-ajout btn-primary waves-effect",
    titleAttr: 'Ajouter un courrier entrant',
    action: function () {
        $("#Modal").modal('show');
        ShowModal("add", 0);
    }
};

dataTable = CreateDatatableWithExportButton('#data_courrier', "Liste des courriers arrivés du " + FormatNowDate(), addButton);

$(window).on("load", function () {

    //dataTable.button().add(0, {
    //    text: 'Ajouter',
    //    className: "btn-ajout btn-primary waves-effect",
    //    titleAttr: 'Ajouter un courrier entrant',
    //    action: function () {
    //        $("#Modal").modal('show');
    //        ShowModal("add",0);
    //    }
    //});

    $('#data_service').DataTable({ dom: 'Bfrtip', responsive: true });

    $('#data_contact').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            {
                text: 'Ajouter',
                className: "btn-ajout btn-primary waves-effect",
                titleAttr: 'Ajouter un correspondant',
                action: function () {
                    $("#Modal").modal('hide');
                    $("#modal_contact select").val("").selectpicker('refresh');
                    $("#modal_contact .modal-title").html("Creation de courrier -> Ajout d'un correspondant");
                    $("#modal_contact input:not([type=hidden])").val("").parent('.focused').removeClass('focused');
                    $("#modal_contact").modal('show');
                }
            }
        ],
    });
});

function ActualiseContactList(contact) {
    $("#data_contact").DataTable().row.add([
        `
            <input name="choix_cont" type="radio" id="cont_${contact.Id_cor}" class ="radio-col-orange" data-id="${contact.Id_cor}" data-lib="${contact.Nom_cor}"><label for="cont_${contact.Id_cor}" style="color:#777 !important;">${contact.Nom_cor}</label>
        `,
            `
            ${contact.Lib_typ_cor}
        `,
            `
            ${contact.Resp_cor == null ? "" : contact.Civ_cor + " " + contact.Resp_cor}
        `
    ]).draw(false);
    ShowMessageWithCallBack("Correspondant enregistré avec succès!!!", "Information", "success", function () {
        $(`label[for=cont_${contact.Id_cor}]`).click();
        $("#modal_contact").modal('hide');
    });
}

function LoadListCourrier(element) {
    var id_stat = $(element).selectpicker("val");
    var url = url_load_list_selon_statut.replace("0", id_stat);
    $.get(url)
        .done(function (result) {
            if (result.state) {
                var list = result.list;
                dataTable.clear();
                $(list).each(function (i, item) {

                    var bouton = "";
                    if (item.statut.toLowerCase().includes("attente")) {
                        bouton = `
                                    <button type="button" title="Classé comme courrier non traité" class="btn btn-warning btn-etat" onclick="StatutNonTraite(this,${item.Id_cour})"><i class="fa fa-folder"></i></button>
                                    `;
                    }
                    else {
                        bouton = `
                                    <button type="button" title="Classé comme courrier en attente de traitement" class ="btn btn-warning btn-etat" onclick="MiseEnAttente(this,${item.Id_cour})"><i class ="fa fa-folder-open"></i></button>
                                    `;
                    }
                    dataTable.row.add([
                        item.Ref_cour,
                        item.Obj_cour,
                        item.Lib_nat,
                        item.expediteur,
                        item.Date_cour,
                        `
                                    <button type="button" class="btn btn-success btn-det" title="Detail" data-toggle="modal" data-target="#modal_detail" onclick="AfficherDetail(${item.Id_cour})"><i class="fa fa-eye"></i></button>
                                    ${bouton}
                                    <button type="button" title="Modifier" class="btn bg-cyan btn-mod" data-toggle="modal" data-target="#Modal" onclick="ShowModal('modifier',${item.Id_cour});"><i class="fa fa-edit"></i></button>
                                        <button type="button" title="Supprimer" class="btn btn-danger btn-sup" onclick="ConfirmSup(${item.Id_cour});"><i class="fa fa-trash"></i></button>
                                `
                    ]);
                });
                dataTable.draw();
                ShowSuccessMessage("Recherche éffectuée avec succès!!!");
            }
            else {
                ShowErrorMessage(result.message);
            }
        })
        .fail(function (error) {
            console.log(error);
            ShowErrorMessage("Le traitement de la requête a échoué!!!");
        });
}

function StatutNonTraite(e, id) {
    console.log(e);
    ShowConfirmMessage(
        function () {
            var url = url_nonTraite.replace('0', id);
            $.get(url).done(function (data) {
                if (data.state) {
                    if ($("#etat").selectpicker("val") != 0) {
                        var ligne = $(e).parents("tr")[0];
                        dataTable.row(ligne).remove();
                        dataTable.draw();
                    }
                    else {
                        $(e).attr("title", "Classé comme courrier en attente de traitement")
                   .html('<i class="fa fa-folder-open"></i>').attr("onclick", `MiseEnAttente(this,${id})`);
                    }
                    ShowSuccessMessage("Le courrier est désormais classé parmi les courriers non traités!!!");
                }
                else {
                    ShowErrorMessage(data.message);
                }
            })
        },
        "Etes-vous sur de vouloir <span class='text-info font-bold font-underline'> classer ce courrier comme un courrier non traité</span>?"
     );
}

function MiseEnAttente(e, id) {
    console.log(e);
    ShowConfirmMessage(
        function () {
            var url = url_miseEnAttente.replace('0', id);
            $.get(url).done(function (data) {
                if (data.state) {
                    if ($("#etat").selectpicker("val") != 0) {
                        var ligne = $(e).parents("tr")[0];
                        dataTable.row(ligne).remove();
                        dataTable.draw();
                    }
                    else {
                        $(e).html('<i class="fa fa-folder"></i>').attr("onclick", `StatutNonTraite(this,${id})`).attr("title", "Classé comme courrier non traité");
                    }
                    ShowSuccessMessage("Le courrier est désormais un courrier en attente de traitement!!!");

                }
                else {
                    ShowErrorMessage(data.message);
                }
            })
        },
        "Etes-vous sur de vouloir <span class='text-info font-bold font-underline'>classer ce courrier comme courrier en attente de traitement</span>?"
     );
}

function ConfirmSup(id) {
    ShowConfirmSupMessage(function () {
        var url = url_delete.replace('0', id);
        $.get(url).done(function (data) {
            if (data.state) {
                dataTable.row(tr_sup).remove().draw();
                ShowSuccessMessage("Courrier supprimé avec succès!!!");
            }
            else {
                ShowErrorMessage(data.message);
            }
        });
    },
    "Etes-vous sur de vouloir supprimer ce courrier ?")
}

function AfficherDetail(id) {
    var url = url_edit.replace("0", id);
    $.get(url)
        .done(function (result) {
            if (result.state) {
                var courrier = result.courrier_arrive;
                $("#modal_detail .code").val(courrier.Ref_cour).parents('div:first').addClass('focused');
                $("#modal_detail .objet").val(courrier.Obj_cour).parents('div:first').addClass('focused');
                $("#modal_detail .nature").val(courrier.nature).parents('div:first').addClass('focused');
                $("#modal_detail .date").val(courrier.date).parents('div:first').addClass('focused');
                $("#modal_detail .statut").val(courrier.statut).parents('div:first').addClass('focused');
                $("#modal_detail .agent").val(courrier.agent).parents('div:first').addClass('focused');
                $("#modal_detail input.fichier").val(courrier.nom_fichier == "" ? "Aucun fichier joint" : courrier.nom_fichier).parents('div:first').addClass('focused');
                if (courrier.chemin == "") {
                    $("#modal_detail a.fichier").addClass("disabled").attr("href", "#");
                }
                else {
                    $("#modal_detail a.fichier").attr("href", courrier.chemin).removeClass("disabled");
                }
            }
            else {
                ShowErrorMessage(e.message);
            }
        })
        .fail(function () { ShowErrorMessage("La requête n'a pas pu être traitée!!!"); });
}

function ShowModal(etat, id) {

    etat_action = etat;

    $("label[for=ig_checkbox]").click();
    $("input[type=radio][name=correspondant]").change();
    //annuler le css en cas d erreur
    $("#form li[role=tab]").removeClass("error")
    //retour a la premiere etape
    while ($("#form").children("div").steps('getStep', 0) != $("#form").children("div").steps('getCurrentStep', 1)) {
        $("#form").children("div").steps("previous");
    }

    if (etat_action === 'modifier') {
        $("#Modal .modal-title").text("Modification d'un courrier entrant");
        $("#file").val("");
        var url = url_edit.replace("0", id);
        $.get(url).done(function (data) {
            if (data.state == true) {
                courrier = data.courrier_arrive;
                $("#id").val(courrier.Id_cour);
                $("#objet").val(courrier.Obj_cour).parents('div:first').addClass('focused');
                $("#code").val(courrier.Ref_cour).parents('div:first').addClass('focused');
                $("#nature").selectpicker("val", courrier.Id_nat);
                $("#user").val(courrier.agent).parents('div:first').addClass('focused');
                $("#date").val(courrier.date).selectpicker('refresh');
                if (courrier.Id_cor != null) {
                    $("label[for=ig_checkbox2]").click();
                    $("input[type=radio][name=correspondant]").change();
                    $('#data_contact').DataTable().columns(0).search(courrier.Nom_cor).draw();
                    $(`label[for=cont_${courrier.Id_cor}]`).click();
                    $('#data_contact').DataTable().columns(0).search("").draw();
                }
                else {
                    $("label[for=ig_checkbox]").click();
                    $("input[type=radio][name=correspondant]").change();
                    $('#data_service').DataTable().columns(0).search(courrier.Lib_serv).draw();
                    $(`label[for=serv_${courrier.Id_serv}]`).click();
                    $('#data_service').DataTable().columns(0).search("").draw();
                }
                //$("#tel").val(contact.Tel_cor).parents('div:first').addClass('focused');
            }
            return false;
        });
    }
    else {
        $("#id").val("0");
        $("#Modal .modal-title").text("Nouveau courrier entrant");
        $("#Modal select").val("").selectpicker('refresh');
        $("#Modal form input:not([type=hidden]").val("").parents('div.focused').removeClass('focused');
        $("#user").val(user).parent('div:first').addClass('focused');
        $("#date").val(date_today).parent('div:first').addClass('focused');
    }
    //$("#type").val("Courrier Arrivé").parent('div:first').addClass('focused');
}

$("#data_courrier").on('click', '.btn-sup', function () {
    tr_sup = $(this).parents('tr:first');
});

//declenche la fonction de mise a jour de la liste de courrier
$("#etat").change();

//cacher le modal d'ajout de contact et afficher le modal de creation de courrier
$("#modal_contact").on('hidden.bs.modal', function () { $("#Modal").modal('show'); });

//recherche du choix de service ou contact effectuer
$("#form").on("change", "input[type=radio][name=correspondant]", function () {
    var type = $("#form input[type=radio][name=correspondant]:checked").data("value");
    $("#final_choix").attr('data-Id_cor', "");
    $("#final_choix").attr('data-Id_serv', "");
    $("#final_choix").html('Aucun');
    $("#data_service input[type=radio][name=choix_serv],#data_contact input[type=radio][name=choix_cont]").attr("checked", false);
    if (type == "contact") {
        $("#contact").show();
        $("#service").hide();
        return true;
    }
    else {
        $("#contact").hide();
        $("#service").show();
        return true;
    }
});

$("#form").on("change", "#data_service input[type=radio][name=choix_serv]", function () {
    var choix = $("input[type=radio][name=choix_serv]:checked");
    $("#final_choix").attr('data-Id_cor', "");
    $("#final_choix").attr('data-Id_serv', $(choix).data('id'));
    $("#final_choix").html($(choix).data('lib'));
});

$("#form").on("change", "#data_contact input[type=radio][name=choix_cont]", function () {
    var choix = $("input[type=radio][name=choix_cont]:checked");
    $("#final_choix").attr('data-Id_cor', $(choix).data("id"));
    $("#final_choix").attr('data-Id_serv', "");
    $("#final_choix").html($(choix).data('lib'));
});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}

$('#form').children('div').steps({
    headerTag: 'h2',
    bodyTag: 'section',
    transitionEffect: 'slideLeft',
    labels: {
        cancel: "Annuler",
        current: "current step:",
        pagination: "Pagination",
        finish: "Valider",
        next: "Suivant",
        previous: "Précédent",
        loading: "Chargement..."
    },
    onInit: function (event, currentIndex) {
        setButtonWavesEffect(event);
    },
    onStepChanged: function (event, currentIndex, priorIndex) {
        setButtonWavesEffect(event);
    },
    onStepChanging: function (event, currentIndex, newIndex) {
        //bouton next
        if (currentIndex < newIndex) {
            var step = $("#form").children("div").steps("getCurrentStep").title.toLowerCase();
            if (step.includes("information")) {
                var code = $("#code").val().trim();
                var objet = $("#objet").val().trim();
                var nature = $("#nature").selectpicker("val");
                if (code == "" || objet == "" || nature == null) {
                    if (code === "") {
                        ShowWarningMessage("Veuillez Renseigner le code du courrier svp!!!");
                    }
                    else if (objet === "") {
                        ShowWarningMessage("Veuillez Renseigner l'objet du courrier svp!!!");
                    }
                    else if (nature === null) {
                        ShowWarningMessage("Veuillez Renseigner la nature du courrier svp!!!");
                    }
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    },
    onFinishing: function (event, currentIndex) {
        var id_cont = $("#final_choix").attr('data-Id_cor');
        var id_serv = $("#final_choix").attr('data-Id_serv');

        if (id_cont == "" && id_serv == "") {
            ShowWarningMessage("Vous n'avez pas sélectionné l'expéditeur");
            return false;
        }
        else {
            var formdata = new FormData();
            var id = $("#id").val();
            var code = $("#code").val().trim();
            var objet = $("#objet").val().trim();
            var nature = $("#nature").selectpicker("val");
            var antiforget = $("#Modal form input[name=__RequestVerificationToken]").val();
            var service = $("#final_choix").attr('data-Id_serv');
            var contact = $("#final_choix").attr('data-Id_cor');

            formdata.append("Id_cour", id);
            formdata.append("Id_nat", nature);
            formdata.append("Ref_cour", code);
            formdata.append("Obj_cour", objet);
            formdata.append("id_serv", service);
            formdata.append("id_cor", contact);
            formdata.append("__RequestVerificationToken", antiforget);

            var fileInput = $("#file")[0];
            for (i = 0; i < fileInput.files.length; i++) {
                formdata.append(fileInput.files[i].name, fileInput.files[i]);
            }

            $.ajax({
                url: url_post_action,
                data: formdata,
                type: "post",
                contentType: false,
                processData: false,
                success: function (result) {
                    if (result.state) {
                        ShowMessageWithCallBack("Courrier enregistré avec succès", "Information", "success", function () {
                            $("#Modal").modal("hide");
                            $("#etat").change();
                        });
                        return true;
                    }
                    else {
                        ShowErrorMessage(result.message);
                        return false;
                    }
                },
                error: function () {
                    ShowErrorMessage("Désolé une erreur est survenue lors de l'envoi de la requête!!!");
                    return false;
                }
            });

            return false;
        }
    },
    onFinished: function (event, currentIndex) {
        $("#Modal").modal("hide");
        return true;
    }
});