﻿functionShowPresentSsMenu('opt12');

const addButton = {
    text: 'Ajouter',
    className: "btn-ajout btn-primary waves-effect",
    titleAttr: 'Ajouter une nature de courrier',
    action: function () {
        $("#Modal").modal('show');
        ShowModal(0, "add");
    }
};

dataTable = CreateDatatableWithExportButton('.dataTable', "Liste des natures de courrier du " + FormatNowDate(), addButton);

//$(window).on("load", function () {
//    dataTable.button().add(0, {
//        text: 'Ajouter',
//        className: "btn-ajout btn-primary waves-effect",
//        titleAttr: 'Ajouter une nature de courrier',
//        action: function () {
//            $("#Modal").modal('show');
//            ShowModal(0, "add");
//        }
//    });
//});

$("table").on('click', '.btn-mod', function () {
    tr_modif = $(this).parents('tr:first');
});

$("table").on('click', '.btn-sup', function () {
    tr_sup = $(this).parents('tr:first');
});

function ConfirmSup(id) {
    function action() {
        var url = url_delete.replace('0', id);
        $.get(url).done(function (data) {
            if (data.state) {
                dataTable.row(tr_sup).remove().draw();
                ShowSuccessMessage("Nature de courrier supprimée avec succès!!!");
            }
            else {
                ShowErrorMessage(data.message);
            }
        });
    }
    ShowConfirmSupMessage(action, "Etes-vous sur de vouloir supprimer cette nature de courrier");
}

function ShowModal(id, etat, lib) {
    etat_action = etat;
    if (etat_action === 'modifier') {
        $("#Modal .modal-title").text("Modification d'une nature de courrier");
        $("#id").val(id);
        $("#lib").val(lib).parents('div:first').addClass('focused');
    }
    else {
        $("#id").val("");
        $("#Modal .modal-title").text("Nouvelle nature de courrier");
        $("#Modal form input[type=text]").val("").parents('div.focused').removeClass('focused');
    }
}

$("#btn_valid").on("click", function (e) {
    e.preventDefault();
    var lib = $("#lib").val().trim();
    var id = $("#id").val();
    var antiforget = $("#Modal form input[name=__RequestVerificationToken]").val();
    if (lib === "") {
        ShowWarningMessage("Vous n'avez pas renseigné le libellé de la nature de courrier!!!");
    }
    else {
        $.post(url_post_action, { nature: { Id_nat: id, Lib_nat: lib }, __RequestVerificationToken: antiforget }).done(function (data) {
            if (data.state == false)ShowErrorMessage(data.message);
            else {
                if (etat_action == "modifier") {
                    dataTable.row(tr_modif).remove();
                }
                var nature = data.nature;
                dataTable.row.add([
                    nature.Lib_nat,
                    `
                            <button type="button" class ="btn bg-cyan btn-mod" data-toggle="modal" data-target="#Modal" onclick="ShowModal(${nature.Id_nat}, 'modifier','${nature.Lib_nat}') "><i class ="fa fa-edit"></i>Modifier</button>
                            <button type="button" class ="btn btn-danger btn-sup" onclick="ConfirmSup(${nature.Id_nat})"><i class ="fa fa-trash"></i>Supprimer</button>
                            `
                ]).draw(false);
                $("#Modal").modal('hide');
                ShowSuccessMessage();
            }
        }).fail(function (e) { console.log(e); });
    }
});