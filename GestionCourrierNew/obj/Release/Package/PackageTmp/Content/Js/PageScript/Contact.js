﻿functionShowPresentSsMenu('opt20');

const addButton = {
    text: 'Ajouter',
    className: "btn-ajout btn-primary waves-effect",
    titleAttr: 'Ajouter d\'un correspondant',
    action: function () {
        $(id_modal_contact).modal('show');
        ShowModal("add", 0);
    }
};

dataTable = CreateDatatableWithExportButton('.dataTable', "Liste des correspondants du " + FormatNowDate(), addButton);

//$(window).on("load", function () {
//    dataTable.button().add(0, {
//        text: 'Ajouter',
//        className: "btn-ajout btn-primary waves-effect",
//        titleAttr: 'Ajouter d\'un correspondant',
//        action: function () {
//            $(id_modal_contact).modal('show');
//            ShowModal("add",0);
//        }
//    });
//});

$("#table_contact").on('click', '.btn-mod', function () {
    tr_modif = $(this).parents('tr:first');
});

$("#table_contact").on('click', '.btn-sup', function () {
    tr_sup = $(this).parents('tr:first');
});

function ShowModal(etat, id) {
    etat_action = etat;
    if (etat_action === 'modifier') {
        $(id_modal_contact+" .modal-title").text("Modification d'un correspondant");
        var url = url_edit.replace("0", id);
        $.get(url).done(function (data) {
            if (data.state == true) {
                contact = data.contact;
                $("#id_contact").val(contact.Id_cor).parents('div:first').addClass('focused');
                $("#lib_contact").val(contact.Nom_cor).parents('div:first').addClass('focused');
                $("#resp_contact").val(contact.Resp_cor).parents('div:first').addClass('focused');
                $("#civ_contact").val(contact.Civ_cor).parents('div:first').addClass('focused');
                $("#mail_contact").val(contact.Mail_cor).parents('div:first').addClass('focused');
                $("#ad_contact").val(contact.Ad_cor).parents('div:first').addClass('focused');
                $("#typ_contact").val(contact.Id_typ_cor).selectpicker('refresh');
                $("#tel_contact").val(contact.Tel_cor).parents('div:first').addClass('focused');
            }
            return false;
        });
    }
    else {
        $("#id_contact").val("");
        $(id_modal_contact+" .modal-title").text("Nouveau correspondant");
        $(id_modal_contact + " select").val("").selectpicker('refresh');
        $(id_modal_contact + " form input:not([type=hidden]").val("").parents('div.focused').removeClass('focused');
    }
}

function ActualiseContactList(contact) {
    if (etat_action == "modifier") {
        dataTable.row(tr_modif).remove();
    }
    dataTable.row.add([
        contact.Nom_cor,
        contact.Tel_cor,
        contact.Ad_cor,
        contact.Mail_cor,
        contact.Resp_cor,
        contact.Lib_typ_cor,
        `
            <button type="button" title="Modifier" class ="btn bg-cyan btn-mod" data-toggle="modal" data-target="${id_modal_contact}" onclick="ShowModal('modifier',${contact.Id_cor}) "><i class ="fa fa-edit"></i></button>
            <button type="button" title="Supprimer" class ="btn btn-danger btn-sup" onclick="ConfirmSup(${contact.Id_cor}) "><i class ="fa fa-trash"></i></button>
        `
    ]).draw(false);
    $(id_modal_contact).modal('hide');
    ShowSuccessMessage();
}

function ConfirmSup(id) {
    function action() {
        var url = url_delete.replace('0', id);
        $.get(url).done(function (data) {
            if (data.state) {
                dataTable.row(tr_sup).remove().draw();
                ShowSuccessMessage("Correspondant supprimé avec succès!!!");
            }
            else {
                ShowErrorMessage(data.message);
            }
        });
    }
    ShowConfirmSupMessage(action, "Etes-vous sur de vouloir supprimer ce correspondant?");
}