﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionCourrierNew.Models
{
    public class ValidationError
    {
        public string EntityType { get; set; }
        public string PropertyName { get; set; }
        public string ErrorText { get; set; }
    }
}