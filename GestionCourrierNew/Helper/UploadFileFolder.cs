﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionCourrierNew.Helper
{
    public static class UploadFileFolder
    {
        public static string FolderArrive { get;} = "/Upload/CourrierArrive/";
        public static string FolderDepart { get;} = "/Upload/CourrierDepart/";
    }
}