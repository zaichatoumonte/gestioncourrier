﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace GestionCourrierNew.Helper
{
    public static class CodeHelper
    {
        public static string GetMd5Hash(string input)
        {
            MD5 md5Hash = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        // Verify a hash against a string.
        public static bool VerifyMd5Hash(string input, string hash)
        {
            MD5 md5Hash = MD5.Create();
            // Hash the input.
            string hashOfInput = GetMd5Hash(input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static Tuple<string,string> UploadFile(HttpPostedFileBase file, string path)
        {
            try
            {
                if (file != null && !string.IsNullOrWhiteSpace(path))
                {
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    var guid = Guid.NewGuid();
                    var extension = file.FileName.Split('.').Last();
                    file.SaveAs(path + Path.GetFileName(guid+"."+extension));

                    //file.SaveAs(path + Path.GetFileName(file.FileName));

                    return new Tuple<string,string>(file.FileName, guid + "." + extension);

                }
                return new Tuple<string, string>(string.Empty,string.Empty);
            }
            catch (Exception)
            {
                return new Tuple<string, string>(string.Empty, string.Empty);
            }
        }
    }
}