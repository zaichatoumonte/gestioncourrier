﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionCourrierNew.Helper
{
    public static class StatutCourrier
    {
        public static Tuple<int, string> Attente { get; } = new Tuple<int, string>(3, "En attente de traitement");
        public static Tuple<int, string> Non_traite { get; } = new Tuple<int, string>(1, "Non Traité");
        public static Tuple<int, string> Traite { get; } = new Tuple<int, string>(2, "Traité");
		//public static Tuple<int, string> Validation { get; } = new Tuple<int, string>(4, "Attente de validation");

	}
}