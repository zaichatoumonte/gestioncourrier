﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestionCourrierNew.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class GestionCourrierEntities : DbContext
    {
        public GestionCourrierEntities()
            : base("name=GestionCourrierEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ACCEDER> ACCEDER { get; set; }
        public virtual DbSet<CATEGORIE_COURRIER> CATEGORIE_COURRIER { get; set; }
        public virtual DbSet<CORRESPONDANT> CORRESPONDANT { get; set; }
        public virtual DbSet<COURRIER> COURRIER { get; set; }
        public virtual DbSet<DROIT_ACCES> DROIT_ACCES { get; set; }
        public virtual DbSet<ECHANGER> ECHANGER { get; set; }
        public virtual DbSet<MODULE> MODULE { get; set; }
        public virtual DbSet<NATURE> NATURE { get; set; }
        public virtual DbSet<PARAMETRAGE> PARAMETRAGE { get; set; }
        public virtual DbSet<SERVICE> SERVICE { get; set; }
        public virtual DbSet<TYPE_CORRESPONDANT> TYPE_CORRESPONDANT { get; set; }
        public virtual DbSet<TYPE_COURRIER> TYPE_COURRIER { get; set; }
        public virtual DbSet<UTILISATEUR> UTILISATEUR { get; set; }
        public virtual DbSet<V_COURRIER> V_COURRIER { get; set; }
        public virtual DbSet<V_PROFIL> V_PROFIL { get; set; }
        public virtual DbSet<V_PROFIL_UTILISATEUR> V_PROFIL_UTILISATEUR { get; set; }
        public virtual DbSet<ARCHIVAGE> ARCHIVAGE { get; set; }
    }
}
