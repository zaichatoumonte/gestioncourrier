//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestionCourrierNew.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DROIT_ACCES
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DROIT_ACCES()
        {
            this.ACCEDER = new HashSet<ACCEDER>();
            this.UTILISATEUR = new HashSet<UTILISATEUR>();
        }
    
        public int Id_drt { get; set; }
        public string Lib_drt { get; set; }
        public Nullable<bool> isDelete { get; set; }
        public Nullable<System.DateTime> dateDelete { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ACCEDER> ACCEDER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UTILISATEUR> UTILISATEUR { get; set; }
    }
}
