﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionCourrierNew.Models
{
    public class Contact
    {
        public string Resp { get; set; }
        public string Civ { get; set; }
        public string Nom { get; set; }
        public string Type { get; set; }
    }
}