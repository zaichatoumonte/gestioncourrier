USE [master]
GO
/****** Object:  Database [GestionCourrierSoutenanceNew]    Script Date: 26/01/2020 15:51:45 ******/
CREATE DATABASE [GestionCourrierSoutenanceNew]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'GestionCourrierSoutenanceNew', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\GestionCourrierSoutenanceNew.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'GestionCourrierSoutenanceNew_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\GestionCourrierSoutenanceNew_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GestionCourrierSoutenanceNew].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET ARITHABORT OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET  DISABLE_BROKER 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET RECOVERY FULL 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET  MULTI_USER 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET DB_CHAINING OFF 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'GestionCourrierSoutenanceNew', N'ON'
GO
USE [GestionCourrierSoutenanceNew]
GO
/****** Object:  Table [dbo].[ACCEDER]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACCEDER](
	[Id_acc] [int] IDENTITY(1,1) NOT NULL,
	[Id_drt] [int] NOT NULL,
	[Id_mod] [int] NOT NULL,
 CONSTRAINT [PK_ACCEDER] PRIMARY KEY CLUSTERED 
(
	[Id_acc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ARCHIVAGE ]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ARCHIVAGE](
	[Id_arch] [int] IDENTITY(1,1) NOT NULL,
	[Annee_arch] [datetime] NOT NULL,
	[Ref_cour_arch] [varchar](50) NOT NULL,
	[Obj_cour_arch] [varchar](50) NOT NULL,
	[Date_cour_arch] [datetime] NOT NULL,
	[Cont_cour_arch] [varchar](max) NULL,
	[Lien_cour_arch] [varchar](max) NULL,
	[Ref_rep_cour_arch] [varchar](100) NULL,
	[Nom_fichier_joint_arch] [varchar](max) NULL,
	[Stat_cour_arch] [varchar](50) NULL,
	[Lib_nat_arch] [varchar](50) NOT NULL,
	[Lib_typ_cour_arch] [varchar](50) NOT NULL,
	[Lib_cat_cour_arch] [varchar](50)  NULL,
	[Nom_util_arch] [varchar](100) NOT NULL,
	[Ref_exp_arch] [varchar](100)  NULL,
	[Destinataires_arch] [varchar](max)  NULL,
 CONSTRAINT [PK_ARCHIVAGE] PRIMARY KEY CLUSTERED 
(
	[Id_arch] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CATEGORIE_COURRIER]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CATEGORIE_COURRIER](
	[Id_cat_cour] [int] IDENTITY(1,1) NOT NULL,
	[Lib_cat_cour] [varchar](50) NOT NULL,
 CONSTRAINT [PK_CATEGORIE_COURRIER] PRIMARY KEY CLUSTERED 
(
	[Id_cat_cour] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CORRESPONDANT]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CORRESPONDANT](
	[Id_cor] [int] IDENTITY(1,1) NOT NULL,
	[Civ_cor] [varchar](4) NULL,
	[Nom_cor] [varchar](100) NOT NULL,
	[Tel_cor] [varchar](50) NOT NULL,
	[Ad_cor] [varchar](50) NULL,
	[Mail_cor] [varchar](100) NULL,
	[Resp_cor] [varchar](100) NULL,
	[Ref_cor] [varchar](100) NULL,
	[Id_typ_cor] [int] NOT NULL,
	[isDelete] [bit] NULL,
	[dateDelete] [datetime] NULL,
 CONSTRAINT [PK_CORRESPONDANT] PRIMARY KEY CLUSTERED 
(
	[Id_cor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[COURRIER]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[COURRIER](
	[Id_cour] [int] NOT NULL,
	[Ref_cour] [varchar](50) NOT NULL,
	[Obj_cour] [varchar](50) NOT NULL,
	[Date_cour] [datetime] NOT NULL,
	[Cont_cour] [varchar](max) NULL,
	[Lien_cour] [varchar](max) NULL,
	[Rep_cour] [int] NULL,
	[Id_util] [int] NOT NULL,
	[Id_nat] [int] NOT NULL,
	[Id_cat_cour] [int] NULL,
	[isDelete] [bit] NULL,
	[dateDelete] [datetime] NULL,
	[nom_fichier_joint] [varchar](max) NULL,
	[Stat_cour] [varchar](50) NULL,
	[Id_typ_cour] [int] NULL,
 CONSTRAINT [PK_Courrier] PRIMARY KEY CLUSTERED 
(
	[Id_cour] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DROIT_ACCES]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DROIT_ACCES](
	[Id_drt] [int] IDENTITY(1,1) NOT NULL,
	[Lib_drt] [varchar](50) NOT NULL,
	[isDelete] [bit] NULL,
	[dateDelete] [datetime] NULL,
 CONSTRAINT [PK_DROIT_ACCES] PRIMARY KEY CLUSTERED 
(
	[Id_drt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ECHANGER]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ECHANGER](
	[Id_ech] [int] IDENTITY(1,1) NOT NULL,
	[Id_cour] [int] NOT NULL,
	[Id_cor] [int] NULL,
	[Id_serv] [int] NULL,
	[Date_envoi] [datetime] NOT NULL,
	[isDest] [bit] NOT NULL,
 CONSTRAINT [PK_ADRESSER] PRIMARY KEY CLUSTERED 
(
	[Id_ech] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MODULE]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MODULE](
	[Id_mod] [int] IDENTITY(1,1) NOT NULL,
	[Lib_mod] [varchar](50) NOT NULL,
	[Role_cs] [varchar](20) NULL,
 CONSTRAINT [PK_MODULE] PRIMARY KEY CLUSTERED 
(
	[Id_mod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NATURE]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NATURE](
	[Id_nat] [int] IDENTITY(1,1) NOT NULL,
	[Lib_nat] [varchar](50) NOT NULL,
	[isDelete] [bit] NULL,
	[dateDelete] [datetime] NULL,
 CONSTRAINT [PK_NATURE] PRIMARY KEY CLUSTERED 
(
	[Id_nat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PARAMETRAGE]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PARAMETRAGE](
	[Nom_directeur] [varchar](100) NOT NULL,
	[Num_cour_ad] [int] NOT NULL,
	[Num_cour_st] [int] NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_PARAMETRAGE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SERVICE]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SERVICE](
	[Id_serv] [int] IDENTITY(1,1) NOT NULL,
	[Lib_serv] [varchar](50) NOT NULL,
	[Ref_serv] [varchar](100) NOT NULL,
	[Resp_serv] [varchar](100) NOT NULL,
	[Civ_serv] [varchar](4) NULL,
	[isDelete] [bit] NULL,
	[dateDelete] [datetime] NULL,
 CONSTRAINT [PK_SERVICE] PRIMARY KEY CLUSTERED 
(
	[Id_serv] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TYPE_CORRESPONDANT]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TYPE_CORRESPONDANT](
	[Id_typ_cor] [int] IDENTITY(1,1) NOT NULL,
	[Lib_typ_cor] [varchar](50) NOT NULL,
	[isDelete] [bit] NULL,
	[dateDelete] [datetime] NULL,
 CONSTRAINT [PK_TYPE_CORRESPONDANT] PRIMARY KEY CLUSTERED 
(
	[Id_typ_cor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TYPE_COURRIER]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TYPE_COURRIER](
	[Id_typ_cour] [int] IDENTITY(1,1) NOT NULL,
	[Lib_typ_cour] [varchar](22) NOT NULL,
 CONSTRAINT [PK_TYPE_COURRIER_DEPART] PRIMARY KEY CLUSTERED 
(
	[Id_typ_cour] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UTILISATEUR]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UTILISATEUR](
	[Id_util] [int] IDENTITY(1,1) NOT NULL,
	[Nom_util] [varchar](50) NOT NULL,
	[Login_util] [varchar](15) NOT NULL,
	[PasswdHash_util] [varchar](100) NOT NULL,
	[Id_drt] [int] NOT NULL,
	[isActif_util] [bit] NULL CONSTRAINT [DF_UTILISATEUR_isActif_util]  DEFAULT ((1)),
	[Date_fin_validite_util] [datetime] NULL,
	[Date_creation_util] [datetime] NULL,
	[IsUpdate] [bit] NULL CONSTRAINT [DF_UTILISATEUR_IsUpdate]  DEFAULT ((0)),
	[Email_util] [varchar](100) NULL,
	[isDelete] [bit] NULL,
	[dateDelete] [datetime] NULL,
 CONSTRAINT [PK_AGENT] PRIMARY KEY CLUSTERED 
(
	[Id_util] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[V_COURRIER]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_COURRIER]
AS
SELECT        dbo.COURRIER.Id_cour, dbo.COURRIER.Ref_cour, dbo.COURRIER.Obj_cour, dbo.COURRIER.Date_cour, dbo.COURRIER.Cont_cour, dbo.COURRIER.Lien_cour, dbo.COURRIER.Rep_cour, dbo.COURRIER.Id_util, 
                         dbo.COURRIER.Id_nat, dbo.COURRIER.Id_cat_cour, dbo.COURRIER.isDelete, dbo.COURRIER.dateDelete, dbo.COURRIER.nom_fichier_joint, dbo.COURRIER.Stat_cour, dbo.COURRIER.Id_typ_cour, dbo.NATURE.Lib_nat, 
                         dbo.NATURE.isDelete AS isDeleteNat, dbo.TYPE_COURRIER.Lib_typ_cour, dbo.UTILISATEUR.Nom_util, dbo.UTILISATEUR.isActif_util, dbo.CATEGORIE_COURRIER.Lib_cat_cour, dbo.ECHANGER.isDest, dbo.ECHANGER.Id_serv, 
                         dbo.ECHANGER.Id_cor, dbo.ECHANGER.Date_envoi, dbo.SERVICE.Lib_serv, dbo.SERVICE.Ref_serv, dbo.SERVICE.Resp_serv, dbo.SERVICE.Civ_serv, dbo.CORRESPONDANT.Civ_cor, dbo.CORRESPONDANT.Nom_cor, 
                         dbo.CORRESPONDANT.Resp_cor, dbo.CORRESPONDANT.Ref_cor, dbo.TYPE_CORRESPONDANT.Lib_typ_cor, dbo.TYPE_CORRESPONDANT.Id_typ_cor
FROM            dbo.TYPE_CORRESPONDANT INNER JOIN
                         dbo.CORRESPONDANT ON dbo.TYPE_CORRESPONDANT.Id_typ_cor = dbo.CORRESPONDANT.Id_typ_cor RIGHT OUTER JOIN
                         dbo.UTILISATEUR INNER JOIN
                         dbo.TYPE_COURRIER INNER JOIN
                         dbo.COURRIER LEFT OUTER JOIN
                         dbo.CATEGORIE_COURRIER ON dbo.COURRIER.Id_cat_cour = dbo.CATEGORIE_COURRIER.Id_cat_cour INNER JOIN
                         dbo.NATURE ON dbo.COURRIER.Id_nat = dbo.NATURE.Id_nat ON dbo.TYPE_COURRIER.Id_typ_cour = dbo.COURRIER.Id_typ_cour ON dbo.UTILISATEUR.Id_util = dbo.COURRIER.Id_util INNER JOIN
                         dbo.ECHANGER ON dbo.COURRIER.Id_cour = dbo.ECHANGER.Id_cour ON dbo.CORRESPONDANT.Id_cor = dbo.ECHANGER.Id_cor LEFT OUTER JOIN
                         dbo.SERVICE ON dbo.ECHANGER.Id_serv = dbo.SERVICE.Id_serv

GO
/****** Object:  View [dbo].[V_PROFIL]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_PROFIL]
AS
SELECT        dbo.DROIT_ACCES.Id_drt, dbo.DROIT_ACCES.Lib_drt, dbo.DROIT_ACCES.isDelete, dbo.ACCEDER.Id_acc, dbo.MODULE.Id_mod, dbo.MODULE.Lib_mod, dbo.MODULE.Role_cs
FROM            dbo.ACCEDER INNER JOIN
                         dbo.DROIT_ACCES ON dbo.ACCEDER.Id_drt = dbo.DROIT_ACCES.Id_drt INNER JOIN
                         dbo.MODULE ON dbo.ACCEDER.Id_mod = dbo.MODULE.Id_mod

GO
/****** Object:  View [dbo].[V_PROFIL_UTILISATEUR]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_PROFIL_UTILISATEUR]
AS
SELECT        dbo.UTILISATEUR.Id_util, dbo.UTILISATEUR.Nom_util, dbo.UTILISATEUR.PasswdHash_util, dbo.UTILISATEUR.Login_util, dbo.UTILISATEUR.isActif_util, dbo.UTILISATEUR.Date_fin_validite_util, 
                         dbo.UTILISATEUR.Date_creation_util, dbo.UTILISATEUR.IsUpdate, dbo.DROIT_ACCES.Id_drt, dbo.DROIT_ACCES.Lib_drt, dbo.DROIT_ACCES.isDelete, dbo.ACCEDER.Id_acc, dbo.MODULE.Id_mod, dbo.MODULE.Lib_mod, 
                         dbo.MODULE.Role_cs
FROM            dbo.ACCEDER INNER JOIN
                         dbo.DROIT_ACCES ON dbo.ACCEDER.Id_drt = dbo.DROIT_ACCES.Id_drt INNER JOIN
                         dbo.UTILISATEUR ON dbo.DROIT_ACCES.Id_drt = dbo.UTILISATEUR.Id_drt INNER JOIN
                         dbo.MODULE ON dbo.ACCEDER.Id_mod = dbo.MODULE.Id_mod

GO
ALTER TABLE [dbo].[ACCEDER]  WITH CHECK ADD  CONSTRAINT [FK_ACCEDER_DROIT_ACCES] FOREIGN KEY([Id_drt])
REFERENCES [dbo].[DROIT_ACCES] ([Id_drt])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ACCEDER] CHECK CONSTRAINT [FK_ACCEDER_DROIT_ACCES]
GO
ALTER TABLE [dbo].[ACCEDER]  WITH CHECK ADD  CONSTRAINT [FK_ACCEDER_MODULE] FOREIGN KEY([Id_mod])
REFERENCES [dbo].[MODULE] ([Id_mod])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ACCEDER] CHECK CONSTRAINT [FK_ACCEDER_MODULE]
GO
ALTER TABLE [dbo].[CORRESPONDANT]  WITH CHECK ADD  CONSTRAINT [FK_CORRESPONDANT_TYPE_CORRESPONDANT] FOREIGN KEY([Id_typ_cor])
REFERENCES [dbo].[TYPE_CORRESPONDANT] ([Id_typ_cor])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[CORRESPONDANT] CHECK CONSTRAINT [FK_CORRESPONDANT_TYPE_CORRESPONDANT]
GO
ALTER TABLE [dbo].[COURRIER]  WITH CHECK ADD  CONSTRAINT [FK_COURRIER_AGENT] FOREIGN KEY([Id_util])
REFERENCES [dbo].[UTILISATEUR] ([Id_util])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[COURRIER] CHECK CONSTRAINT [FK_COURRIER_AGENT]
GO
ALTER TABLE [dbo].[COURRIER]  WITH CHECK ADD  CONSTRAINT [FK_COURRIER_CATEGORIE_COURRIER] FOREIGN KEY([Id_cat_cour])
REFERENCES [dbo].[CATEGORIE_COURRIER] ([Id_cat_cour])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[COURRIER] CHECK CONSTRAINT [FK_COURRIER_CATEGORIE_COURRIER]
GO
ALTER TABLE [dbo].[COURRIER]  WITH CHECK ADD  CONSTRAINT [FK_COURRIER_NATURE] FOREIGN KEY([Id_nat])
REFERENCES [dbo].[NATURE] ([Id_nat])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[COURRIER] CHECK CONSTRAINT [FK_COURRIER_NATURE]
GO
ALTER TABLE [dbo].[COURRIER]  WITH CHECK ADD  CONSTRAINT [FK_COURRIER_TYPE_COURRIER_DEPART] FOREIGN KEY([Id_typ_cour])
REFERENCES [dbo].[TYPE_COURRIER] ([Id_typ_cour])
GO
ALTER TABLE [dbo].[COURRIER] CHECK CONSTRAINT [FK_COURRIER_TYPE_COURRIER_DEPART]
GO
ALTER TABLE [dbo].[ECHANGER]  WITH CHECK ADD  CONSTRAINT [FK_ADRESSER_CORRESPONDANT] FOREIGN KEY([Id_cor])
REFERENCES [dbo].[CORRESPONDANT] ([Id_cor])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ECHANGER] CHECK CONSTRAINT [FK_ADRESSER_CORRESPONDANT]
GO
ALTER TABLE [dbo].[ECHANGER]  WITH CHECK ADD  CONSTRAINT [FK_ADRESSER_COURRIER] FOREIGN KEY([Id_cour])
REFERENCES [dbo].[COURRIER] ([Id_cour])
GO
ALTER TABLE [dbo].[ECHANGER] CHECK CONSTRAINT [FK_ADRESSER_COURRIER]
GO
ALTER TABLE [dbo].[ECHANGER]  WITH CHECK ADD  CONSTRAINT [FK_ADRESSER_SERVICE] FOREIGN KEY([Id_serv])
REFERENCES [dbo].[SERVICE] ([Id_serv])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ECHANGER] CHECK CONSTRAINT [FK_ADRESSER_SERVICE]
GO
ALTER TABLE [dbo].[UTILISATEUR]  WITH CHECK ADD  CONSTRAINT [FK_AGENT_DROIT_ACCES] FOREIGN KEY([Id_drt])
REFERENCES [dbo].[DROIT_ACCES] ([Id_drt])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[UTILISATEUR] CHECK CONSTRAINT [FK_AGENT_DROIT_ACCES]
GO
/****** Object:  Trigger [dbo].[TRG_Insert_Correspondant]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create TRIGGER [dbo].[TRG_Insert_Correspondant]
       ON [dbo].[CORRESPONDANT]
AFTER INSERT
AS
BEGIN
       SET NOCOUNT ON;
 
       DECLARE @Id INT
 
       SELECT @Id = INSERTED.Id_cor      
       FROM INSERTED
 
       update Correspondant set Ref_cor = (CONCAT('COR',@Id)) from Correspondant where Correspondant.Id_cor= @Id
END

GO
/****** Object:  Trigger [dbo].[TRG_Insert_Service]    Script Date: 26/01/2020 15:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create TRIGGER [dbo].[TRG_Insert_Service]
       ON [dbo].[SERVICE]
AFTER INSERT
AS
BEGIN
       SET NOCOUNT ON;
 
       DECLARE @Id INT
 
       SELECT @Id = INSERTED.Id_serv       
       FROM INSERTED
 
       update SERVICE set Ref_serv  = (CONCAT('SERV',@Id)) from SERVICE where SERVICE.Id_serv = @Id
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'decrit les destinataire d''un courrier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ECHANGER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[45] 4[7] 2[6] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -21
         Left = -507
      End
      Begin Tables = 
         Begin Table = "TYPE_COURRIER"
            Begin Extent = 
               Top = 213
               Left = 863
               Bottom = 303
               Right = 1033
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "NATURE"
            Begin Extent = 
               Top = 0
               Left = 899
               Bottom = 129
               Right = 1069
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "COURRIER"
            Begin Extent = 
               Top = 6
               Left = 662
               Bottom = 135
               Right = 843
            End
            DisplayFlags = 280
            TopColumn = 11
         End
         Begin Table = "CATEGORIE_COURRIER"
            Begin Extent = 
               Top = 132
               Left = 1087
               Bottom = 227
               Right = 1257
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "UTILISATEUR"
            Begin Extent = 
               Top = 169
               Left = 522
               Bottom = 298
               Right = 719
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ECHANGER"
            Begin Extent = 
               Top = 0
               Left = 1144
               Bottom = 130
               Right = 1314
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "SERVICE"
            Begin Extent = 
               Top = 10
               Left = 1368
               Bottom = 140
               Right = 1538
            En' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_COURRIER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'd
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "CORRESPONDANT"
            Begin Extent = 
               Top = 97
               Left = 1289
               Bottom = 227
               Right = 1459
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "TYPE_CORRESPONDANT"
            Begin Extent = 
               Top = 137
               Left = 1477
               Bottom = 267
               Right = 1647
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 41
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_COURRIER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_COURRIER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ACCEDER"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 119
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DROIT_ACCES"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MODULE"
            Begin Extent = 
               Top = 6
               Left = 454
               Bottom = 119
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PROFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PROFIL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[10] 2[18] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = -92
      End
      Begin Tables = 
         Begin Table = "ACCEDER"
            Begin Extent = 
               Top = 15
               Left = 386
               Bottom = 127
               Right = 556
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "DROIT_ACCES"
            Begin Extent = 
               Top = 3
               Left = 646
               Bottom = 132
               Right = 816
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MODULE"
            Begin Extent = 
               Top = 23
               Left = 170
               Bottom = 150
               Right = 340
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "UTILISATEUR"
            Begin Extent = 
               Top = 11
               Left = 932
               Bottom = 140
               Right = 1129
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PROFIL_UTILISATEUR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PROFIL_UTILISATEUR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PROFIL_UTILISATEUR'
GO
USE [master]
GO
ALTER DATABASE [GestionCourrierSoutenanceNew] SET  READ_WRITE 
GO
