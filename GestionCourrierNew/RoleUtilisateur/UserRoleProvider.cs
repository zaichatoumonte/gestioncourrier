﻿using System;
using System.Linq;
using System.Web.Security;
using GestionCourrierNew.Models;

namespace GestionCourrierNew.RoleUtilisateur
{
    //cette class permet de definir la source ou le system de definition des roles tires la liste de role.
    //une configuration est ajouter a la Web.config ppour dire que c'est cette class qu il faut utiliser pour definir les roles
    public class UserRoleProvider : RoleProvider
    {
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            using (GestionCourrierEntities db = new GestionCourrierEntities())
            {
                var userRoles = db.V_PROFIL_UTILISATEUR.Where(e => e.Nom_util == username).Select(e => e.Role_cs).Distinct().ToArray();
                return userRoles;
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}