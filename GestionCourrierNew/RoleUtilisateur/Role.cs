﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionCourrierNew.RoleUtilisateur
{
    [Flags]/*permet par exemple d'obtenir une valeur string de Receive en #receive# au lieu de 1*/
    public enum Role
    {
        //les roles doivent commencer a partir de 1
        Receive=1,
        //Dashbord,
        Archive,
        Traitement,
        Configuration,
        Edition,
		Demande
    }
}