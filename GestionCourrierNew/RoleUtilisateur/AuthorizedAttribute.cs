﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionCourrierNew.RoleUtilisateur
{
    public class AuthorizedRoles: AuthorizeAttribute
    {
        public AuthorizedRoles(params Role[] roles) : base()
        {
            //Roles = string.Join(",", Enum.GetNames(typeof(Role)));
            Roles = string.Join(",",roles);
        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult("authentification.login", new System.Web.Routing.RouteValueDictionary { });
                return;
            }
            else if (filterContext.Result is HttpUnauthorizedResult)
            {
                if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
                    filterContext.Result = new RedirectResult("~/authentification");
                else
                    filterContext.Result = new RedirectResult("~/error/index?code=401");
            }
        }
    }
}