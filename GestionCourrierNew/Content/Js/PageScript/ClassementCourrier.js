﻿
functionShowPresentSsMenu('opt5');

$(function () {
    dataTable = $('#data_courrier').DataTable({
        dom: 'lBfrtip',
        responsive: true,
        "autoWidth": false,
        buttons: [
            {
                extend: 'colvisGroup',
                text: 'Afficher tout',
                show: ':hidden',
                className: "waves-effect bg-blue-grey"
            },
            {
                extend: 'colvisGroup',
                text: 'Info Courrier',
                className: "waves-effect bg-blue-grey",
                show: [0, 1, 2, 3, 4, 11],
                hide: [5, 6, 7, 8,9,10]
            },
             {
                 extend: 'colvisGroup',
                 text: 'Détails Courrier',
                 className: "waves-effect bg-blue-grey",
                 show: [0, 5, 6, 7, 8, 11],
                 hide: [1, 2, 3, 4, 9, 10]
             },
            {
                extend: 'colvisGroup',
                text: 'Info Expediteur et Destinataire',
                className: "waves-effect bg-blue-grey",
                show: [0, 9, 10,11],
                hide: [1, 2, 3, 4, 5, 6, 7, 8]
            },

            {
                text: 'Excel',
                className: "waves-effect",
                action: function () { getExportName(".btn-excel"); }
            },
            {
                text: 'Pdf',
                className: "waves-effect",
                action: function () {
                    getExportName(".btn-pdf");
                }
            },
            {
                text: 'Imprimer',
                className: "waves-effect",
                action: function () {
                    getExportName(".btn-print");
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible',
                    //format: {
                    //    body: function (data, row, column, node) {
                    //        //return column === 13 || column === 14 || column === 15 ?
                    //        //data.replace(/[$,]/g, '') :
                    //        //data;

                    //    }
                    //}
                },
                className: "waves-effect btn-excel",
                filename: function () { return filename }
            },
            {
                extend: 'pdf',
                className: "waves-effect btn-pdf",
                exportOptions: {
                    columns: ':visible'
                },
                filename: function () { return filename },
                title: function () { return filename },
                //download: 'open'
            },
            {
                extend: 'print',
                className: "waves-effect btn-print",
                exportOptions: {
                    columns: ':visible'
                },
                filename: function () { return filename },
                text: "IMPRIMER",
                extension: 'filepdf.pdf',
                //autoPrint: false,
                title: function () { return filename },
                customize: function (win) {
                    console.log(win);
                    $(win.document.body)
                        .css('font-size', '10pt')
                    $(win.document.body).find('#data_courrier')
                        .addClass('compact')
                        .css('font-size', '10pt');
                }
            }
        ],
        "pageLength": 5,
        "lengthMenu": [5, 10, 15, 20, 25, 50, 75, 100],
    });

    function FormatNowDate() {
        let today = new Date();
        let y = today.getFullYear();
        let m = today.getMonth() + 1;
        let j = today.getDate();
        let h = today.getHours();
        let min = today.getMinutes();
        if (m < 10) { m = "0" + m };
        if (j < 10) { j = "0" + j };
        if (h < 10) { h = "0" + h };
        if (min < 10) { min = "0" + min };
        return (j + '-' + m + '-' + y + " " + h + ":" + min);
    }

    function getExportName(buttonClass) {
        swal({
            title: "Nom du fichier à exporter!",
            text: "Entrez le nom du fichier à exporter svp!!!",
            type: "input",
            showCancelButton: true,
            cancelButtonText: "Annuler",
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: "Entrez le nom du fichier svp !!!",
            inputValue: "Liste des courriers du " + FormatNowDate()
        }, function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue.trim() === "") {
                swal.showInputError("Vous devez renseigner un nom de fichier!!!"); return false
            }
            filename = inputValue;
            $(buttonClass).click();
            swal.close();
        });
    }
});

$("#type_courrier").on('change', function () {
    if ($(this).val() == 0) {
        $("#div_categorie").show();
    }
    else {
        var group = $("#type_courrier option:selected").text().trim();
        if (group.toLowerCase().includes("départ")) {
            $("#div_categorie").show();
        }
        else {
            $("#categorie").val("0").selectpicker("refresh");
            $("#div_categorie").hide();
        }
    }
}).trigger("change");

function ShowDetailArrive(id) {
    $.get(url_detail.replace('0', id)).done(function (result) {
        if (result.state == true) {

            var courrier = result.courrier;
            var expediteur = result.expediteur;
            var contact_dest = result.destinataire_contact;
            var service_dest = result.destinataire_service;
            $("#modal_arrive .code").val(courrier.Ref_cour).parents('div:first').addClass('focused');
            $("#modal_arrive .objet").val(courrier.Obj_cour).parents('div:first').addClass('focused');
            $("#modal_arrive .nature").val(courrier.Lib_nat).parents('div:first').addClass('focused');
            $("#modal_arrive .date").val(MakeDate(courrier.Date_cour)).parents('div:first').addClass('focused');
            $("#modal_arrive .statut").val(courrier.Stat_cour).parents('div:first').addClass('focused');
            $("#modal_arrive .agent").val(courrier.Nom_util).parents('div:first').addClass('focused');
            $("#modal_arrive .exp, #modal_arrive .type_exp").val("").parents('div:first').addClass('focused');
            if (expediteur != null) {
                var type_exp = expediteur.Type;
                $("#modal_arrive .exp").val(expediteur.Nom).parents('div:first').addClass('focused');
                $("#modal_arrive .type_exp ").val(type_exp).parents('div:first').addClass('focused');
            }
           
            $("#modal_arrive input.fichier").val(courrier.nom_fichier_joint == "" ? "Aucun fichier joint" : courrier.nom_fichier_joint).parents('div:first').addClass('focused');
            if (courrier.Lien_cour == null) {
                $("#modal_arrive a.fichier").addClass("disabled").attr("href", "#");
            }
            else {
                $("#modal_arrive a.fichier").attr("href", courrier.Lien_cour).removeClass("disabled");
            }
      
            $("#modal_arrive .dest_service,#modal_arrive .dest_contact").html("");
            if (contact_dest.length == 0) {
                $("#modal_arrive .dest_contact").html("Aucun");
            }
            if (service_dest.length == 0) {
                $("#modal_arrive .dest_service").html("Aucun");
            }
            for(var item of contact_dest) {
                $("#modal_arrive .dest_contact").append(`-${item.Nom_cor}<br/>`);
            }
            for(var item of service_dest) {
                $("#modal_arrive .dest_service").append(`-${item.Lib_serv}<br/>`);
            }
        }
        else {
            swal("Erreur", result.message, "error");
        }
    });
}

function ShowDetailDepart(id) {
    $.get(url_detail.replace('0', id)).done(function (result) {
        if (result.state) {
            var courrier = result.courrier;
            var contact_dest = result.destinataire_contact;
            var service_dest = result.destinataire_service;
            $("#modal_depart .code").val(courrier.Ref_cour).parents('div:first').addClass('focused');
            $("#modal_depart .objet").val(courrier.Obj_cour).parents('div:first').addClass('focused');
            $("#modal_depart .nature").val(courrier.Lib_nat).parents('div:first').addClass('focused');
            $("#modal_depart .date").val(MakeDate(courrier.Date_cour)).parents('div:first').addClass('focused');
            $("#modal_depart .type").val(courrier.Lib_typ_cour).parents('div:first').addClass('focused');
            $("#modal_depart .agent").val(courrier.Nom_util).parents('div:first').addClass('focused');
            $("#modal_depart input.fichier").val(courrier.nom_fichier_joint == "" ? "Aucun fichier joint" : courrier.nom_fichier_joint).parents('div:first').addClass('focused');
            if (courrier.Lien_cour == null) {
                $("#modal_depart a.fichier").addClass("disabled").attr("href", "#");
            }
            else {
                $("#modal_depart a.fichier").attr("href", courrier.Lien_cour).removeClass("disabled");
            }


            $("#modal_depart .dest_service,#modal_depart .dest_contact").html("");
            if (contact_dest.length == 0) {
                $("#modal_depart .dest_contact").html("Aucun");
            }
            if (service_dest.length == 0) {
                $("#modal_depart .dest_service").html("Aucun");
            }
            for(var item of contact_dest) {
                $("#modal_depart .dest_contact").append(`-${item.Nom_cor}<br/>`);
            }
            for(var item of service_dest) {
                $("#modal_depart .dest_service").append(`-${item.Lib_serv}<br/>`);
            }
        }
        else {
            swal("Erreur", result.message, "error");
        }
    });
}

function Rechercher() {
    var type_courrier = $("#type_courrier").val()
    var agent = $("#agent").val();
    var nat = $("#nat").val();
    var statut = $("#statut").val();
    var categorie = $("#categorie").val();
    var autresCriteres = $("#cont").val();

    type_courrier == null ? 0 : type_courrier;
    agent == null ? 0 : agent;
    nat == null ? 0 : nat;
    statut == null ? 0 : statut;
    autresCriteres == null ? "" : autresCriteres.trim();
    categorie == null ? 0 : categorie;

    $.post(url_search_courrier, {
        id_cat: categorie,
        id_util: agent,
        id_nat: nat,
        id_stat_cour: statut,
        id_typ_cour: type_courrier,
        other: autresCriteres,
    })
        .done(function (data) {
            dataTable.clear().draw();
            if (data.state == true) {
                console.log(data.list);
                window.resultat = data.list;
                $(data.list).each(function (i, item) {
                    let courrier = item.courrier;
                    let destinataire = item.destinataire_service;
                    let info_dest = "";
                    $(destinataire).each(function (i, item2) {
                        info_dest += AfficheInfoCorrespondant(item2) + "<hr>";
                    });
                    destinataire = item.destinataire_contact;
                    $(destinataire).each(function (i, item2) {
                        info_dest += AfficheInfoCorrespondant(item2) + "<hr>";
                    });

                    let action = "";
                    if (courrier.Lib_typ_cour.toLowerCase().includes("arr")) {
                        action = `
                                        <button onclick="ShowDetailArrive(${courrier.Id_cour})" class ="btn bg-cyan" title="afficher les détails sur le courrier" data-target="#modal_arrive" data-toggle="modal"><i class ="fa fa-eye"></i></button>
                                    `;
                        if (item.Rep_cour != null) {
                            action += `
                                        <button onclick="ShowDetailDepart(${courrier.Rep_cour})" class ="btn bg-green" title="afficher les détails sur la reponse au courrier" data-target="#modal_depart" data-toggle="modal"><i class ="fa fa-comments"></i></button>
                                        <a href="${url_print.replace('0', courrier.Rep_cour)}" class ="btn bg-orange" title="Imprimer la reponse"><i class ="fa fa-print"></i></button>
                                    `;
                        }
                    }
                    else {
                        action = `
                                <button onclick="ShowDetailDepart(${courrier.Id_cour})" class ="btn bg-cyan" title="afficher les détails sur le courrier" data-target="#modal_depart" data-toggle="modal"><i class ="fa fa-eye"></i></button>
                                <a href="${url_print.replace('0', courrier.Id_cour)}" class ="btn bg-orange" title="Imprimer le courrier"><i class ="fa fa-print"></i></button>
                            `;
                    }

                    dataTable.row.add([
                        courrier.Ref_cour,
                        courrier.Ref_cour_rep,
                        courrier.Obj_cour,
                        courrier.Lib_nat,
                        MakeDate(courrier.Date_cour),
                        courrier.Nom_util,
                        courrier.Lib_typ_cour,
                        courrier.Stat_cour,
                        courrier.Lib_cat_cour,
                        AfficheInfoCorrespondant(item.expediteur),
                        info_dest,
                        action
                    ]);
                });
                dataTable.draw();
                swal({ title: "Information", text: "Mise à jour de la liste éffectuée", type: "success", timer: 2000 });
            }
            else {
                swal("Erreur", "Une erreur est survenue lors de la recherche des données", "error");
            }
        })
}

$(window).on('load', function () { Rechercher(); });

function AfficheInfoCorrespondant(objet_cor_serv) {
    let info = "";
    if (objet_cor_serv != null) {
        if (objet_cor_serv.Id_cor != null) {
            info = `<u class="text-info">Type:</u>${objet_cor_serv.Lib_typ_cor}\n<br>
                        <u class ="text-info">Nom: </u>${objet_cor_serv.Resp_cor != null ? objet_cor_serv.Nom_cor : objet_cor_serv.Civ_cor + " " + objet_cor_serv.Nom_cor}\n<br>
                        <u class="text-info">Responsable:</u>${objet_cor_serv.Resp_cor == null ? "Non defini" : objet_cor_serv.Civ_cor + " " + objet_cor_serv.Resp_cor}`;
        }
        else {
            info = `<u class="text-info">Type</u>:Service\n<br><u class="text-info">Nom:</u>${objet_cor_serv.Lib_serv}\n<br><u class="text-info">Responsable:</u>${objet_cor_serv.Civ_serv} ${objet_cor_serv.Resp_serv}`;
        }
    }
    return info;
}