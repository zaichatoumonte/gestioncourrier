﻿functionShowPresentSsMenu('opt10');
const addButton = {
    text: 'Ajouter',
    className: "btn-ajout btn-primary waves-effect",
    titleAttr: 'Ajouter un compte utilisateur',
    action: function () {
        $("#Modal").modal('show');
        ShowModal("add");
    }
};

dataTable = CreateDatatableWithExportButton('.dataTable', "Liste des Utilisateurs du " + FormatNowDate(), addButton);

//ajout du bouton d'ajout de compte
//$(window).on("load", function () {
//    dataTable.button().add(0, {
//        text: 'Ajouter',
//        className: "btn-ajout btn-primary waves-effect",
//        titleAttr: 'Ajouter un compte utilisateur',
//        action: function () {
//            $("#Modal").modal('show');
//            ShowModal("add");
//        }
//    });
//});

//on fixe la valeur minimum a demain
//$("#dt-expir,#dt_expir_change").datepicker("setStartDate", GetTomorrowDate());

//pour la gestion de l'apparence flottante du datepicker
$("#dt-expir,#dt_expir_change").on("change", function () {
    if ($("#dt-expir").val() != "") {
        $("#dt-expir").parents("div:first").addClass("focused");
    }
    else {
        $("#dt-expir").parents("div:first").removeClass("focused");
    }
});

/*
$("#dt-expir").datepicker("destroy")
$("#dt-expir").datepicker("update","15/11/2019")
$("#dt-expir").datepicker().val()
$("#dt-expir").datepicker("setStartDate","01/2/2020")
$("#dt-expir").datepicker("clearDates")
*/

$("table").on('click', '.btn-mod', function () {
    tr_modif = $(this).parents('tr:first');
});

$("table").on('click', '.btn-sup', function () {
    tr_sup = $(this).parents('tr:first');
});

function ConfirmSup(id) {
    function action() {
        var url = url_delete.replace('0', id);
        $.get(url).done(function (data) {
            if (data.state) {
                ShowMessageWithCallBack("Compte supprimé avec succès!!!", "Information", "success", function () {
                    window.location.reload();
                });
            }
            else {
                ShowErrorMessage(data.message);
            }
        });
    }
    ShowConfirmSupMessage(action, "Etes-vous sur de vouloir supprimer ce compte utilisateur?");
}

function Desactive(id) {
    function action() {
        let url = url_desactive.replace("0", id);
        $.get(url).done(function (result) {
            if (result.state == true) {
                ShowMessageWithCallBack("Ce compte utilisateur a bien été désactivé!!!", "Information", "success", function () {
                    window.location.reload();
                });
            }
            else {
                ShowErrorMessage(result.message);
            }
        });
    }
    ShowConfirmMessage(action, "Voulez-vous vraiment désactiver ce compte utilisateur?");
}

//confirmation pour activer un compte
function Active(id) {

    function action() {
        let url_info = url_info_profil.replace(0, id);
        $.get(url_info).done(function (result) {
            if (result.state) {
                if (result.valid_profil == true /*&& result.valid_date == true*/) {
                    let url = url_active.replace("0", id);
                    $.get(url).done(function (data) {
                        if (data.state == true) {
                            ShowMessageWithCallBack("Ce compte utilisateur a été réactivé avec succès!!!", "Information", "success", function () {
                                window.location.reload()
                            });
                        }
                        else {
                            ShowErrorMessage(data.message);
                        }
                    });
                }
                else {
                    ShowConfirmMessage(
                        function () {
                            $("#id_change").val(id);
                            //$("#dt_expir_change").datepicker("update", `${result.date}`).parents("div:first").addClass("focused");
                            $("#profil_change").val(result.Id_drt).selectpicker("refresh");
                            swal.close();
                            $("#modal_profil").modal("show");
                        }, "Vous devez modifier <span class='text-info font-bold font-underline'>Le profil de l'utilisateur</span> pour réactiver le compte.", "Attention!!!", "Continuer");
                }
            }
            else {
                ShowErrorMessage(result.message);
            }
        });

    }
    ShowConfirmMessage(action, "Voulez-vous vraiment réactiver ce compte utilisateur?");
}

//lorsque le profil du compte a reactiver est supprimer, action pour changer son profil
//sinon si c est la date d'expiration qui est atteinte, il faut la changer
function ActiverCompte() {
    var profil = $("#profil_change").val();
    //var date = $("#dt_expir_change").val();
    var id = $("#id_change").val();
    var antigorget = $("#form_profil input[name=__RequestVerificationToken]").val();
    if (profil == "") {
        ShowWarningMessage("Vous devez renseigner le profil de l'utilisateur.");
    }
        //else if (date == "") {
        //    ShowWarningMessage("Vous devez renseigner la date de fin de validité du compte de l'utilisateur.");
        //}
    else {
        $.post(url_change_profil, { id: id, id_profil: profil, date_expire: date, __RequestVerificationToken: antigorget }).done(function (result) {
            if (result.state == true) {
                //ShowSuccessMessage("Ce compte utilisateur a été réactivé avec success.");
                //window.location.reload();
                ShowMessageWithCallBack("Ce compte utilisateur a été réactivé avec succès!!!", "Information", "success", function () { window.location.reload() });
            }
            else {
                ShowErrorMessage(result.message);
            }
        });
    }
}

function ShowModal(etat, id) {
    etat_action = etat;
    if (etat_action === 'modifier') {
        $("#Modal .modal-title").text("Modification d'un compte utilisateur");
        $("#message").hide();
        let url_info = url_info_profil.replace(0, id);
        $.get(url_info).done(function (result) {
            if (result.state == true) {

                $("#id").val(id);
                $("#nom").val(result.Nom_util).parents('div:first').addClass('focused');
                $("#mail").val(result.Email_util).parents('div:first').addClass('focused');
                $("#profil").val(result.Id_drt).selectpicker('refresh');
                //$("#dt-expir").datepicker("update",result.date);
                $("#login,#password,#cf-password").val("");
                if (result.IsUpdate == true) {
                    $("#login,#password,#cf-password").attr("disabled", true);
                }
                else {
                    $("#message").show();
                    $("#login,#password,#cf-password").attr("disabled", false);
                    $("#login").val(result.Login_util).parents('div:first').addClass('focused');
                }
            }
            else {
                ShowErrorMessage(result.message);
            }
        });
    }
    else {
        $("#Modal .modal-title").text("Nouveau compte utilisateur");
        $("#login,#password,#cf-password").attr("disabled", false);
        $("#login,#password,#cf-password").parents('div.form-group').show();
        $('#Modal form select').val('').selectpicker('refresh');
        $('#Modal form input:not([name=__RequestVerificationToken])').val("").parents('div.focused').removeClass('focused');
        //$("#dt-expir").datepicker("update","");
    }
}

$("#btn_valid").on("click", function (e) {
    e.preventDefault();
    var message = "";
    var nom = $("#nom").val().trim();
    var id = $("#id").val();
    var login = $("#login").val().trim();
    var password = $("#password").val().trim();
    var cf_password = $("#cf-password").val().trim();
    //var dt_expir = $("#dt-expir").val().trim();
    var profil = $("#profil").val();
    var mail = $("#mail").val().trim();
    var valid_mail = $("#mail")[0].validity.valid;
    var antiforget = $("#Modal form input[name=__RequestVerificationToken]").val();

    if (etat_action === "add") {
        if (nom === "") message = "Vous n'avez pas renseigner le nom de l'utilisateur!!!";
        else if (login === "") message = "Vous n'avez pas renseigner le login de l'utilisateur!!!";
        else if (password === "" || cf_password === "") message = "Vérifiez que les mots de passe ont été renseignés!!!";
        else if (password != cf_password) message = "Les mots de passe ne sont pas identiques!!!";
            //else if (dt_expir === "") message = "Renseignez la date de fin de validité du mot de passe svp!!!";
        else if (profil === "") message = "Renseignez le profil de l'utilisateur svp!!!";
        else if (valid_mail == false) message = "Renseigner un mail valide svp!!!";
    }
    else {
        if (nom === "") message = "Vous n'avez pas renseigner le nom de l'utilisateur!!!";
            //else if (dt_expir === "") message = "Renseignez la date de fin de validité du mot de passe svp!!!";
        else if (profil === "") message = "Renseignez le profil de l'utilisateur svp!!!";
        else if (valid_mail == false) message = "Renseigner un mail valide svp!!!";
    }

    if (message != "") {
        ShowWarningMessage(message);
        //swal("Attention", message, "warning");
    }
    else {
        $.post(url_post_action, {
            utilisateur: {
                Id_util: id,
                Nom_util: nom,
                Id_drt: profil,
                PasswdHash_util: password,
                Login_util: login,
                //Date_fin_validite_util: dt_expir,
                Email_util: mail
            },
            cf_password: cf_password,
            __RequestVerificationToken: antiforget
        }).done(function (data) {
            if (data.state == false) ShowErrorMessage(data.message);
            else {
                //if (etat_action == "modifier") {
                //    dataTable.row(tr_modif).remove();
                //}

                //var utilisateur = data.utilisateur;
                //dataTable.row.add([
                //    utilisateur.Nom_util,
                //    utilisateur.isActif_util,
                //    utilisateur.Date_creation_util,
                //    utilisateur.Date_fin_validite_util,
                //    utilisateur.Lib_drt,
                //    `
                //            <button type="button" class ="btn bg-cyan btn-mod" data-toggle="modal" data-target="#Modal" onclick="ShowModal('modifier',${utilisateur.Id_util},'${utilisateur.Nom_util}',${utilisateur.Id_drt},'${utilisateur.Date_fin_validite_util},'${utilisateur.Date_creation_util}') "><i class ="fa fa-edit"></i>Modifier</button>
                //            <button type="button" class ="btn btn-danger btn-sup" onclick="ConfirmSup(${utilisateur.Id_util})"><i class ="fa fa-trash"></i>Supprimer</button>
                //            `
                //]).draw(false);
                $("#Modal").modal('hide');
                ShowMessageWithCallBack("Ce compte utilisateur a été enregistré avec succès!!!", "Information", "success", function () { window.location.reload() });

            }
        }).fail(function (e) { console.log(e); });
    }
});
