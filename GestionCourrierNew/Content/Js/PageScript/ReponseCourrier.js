﻿functionShowPresentSsMenu('opt7');

function ConfirmSup(id_cour, id_cour_rep) {
    swal({
        title: "Attention !!!",
        text: "Etes-vous sur de vouloir supprimer cette réponse au courrier ?",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Annuler",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Supprimer",
        closeOnConfirm: false,
        closeOnClickOutside: false,
    }, function () {
        var url = url_delete_reponse.replace('0', id_cour);
        var url = url.replace('0', id_cour_rep);
        $.get(url).done(function (data) {
            if (data.state) {
                swal("Félicitation!!!", "Réponse supprimé avec succès!!!", "success");
                window.location.reload();
            }
            else {
                swal("Erreur!!!", data.message, "error");
            }
        });
    });
}

function ActualiseDataTable(result) {
    if (result.state) {
        swal({
            title: "Information",
            text: "Courrier enregistré avec succès",
            type: "success",
        },
        function () {
            $("#Modal").modal("hide");
            window.location.reload();
        });
        return true;
    }
    else {
        swal({title:"Erreur", text:result.message, type:"error",html:true});
        return false;
    }
}
