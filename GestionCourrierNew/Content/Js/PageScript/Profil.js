﻿functionShowPresentSsMenu('opt11');

const addButton = {
    text: 'Ajouter',
    className: "btn-ajout btn-primary waves-effect",
    titleAttr: 'Ajouter une nature de courrier',
    action: function () {
        $("#Modal").modal('show');
        ShowModal(0, "add");
    }
};

dataTable = CreateDatatableWithExportButton('.dataTable', "Liste des profils utilisateurs du " + FormatNowDate(), addButton);

//$(window).on("load", function () {
//    dataTable.button().add(0, {
//        text: 'Ajouter',
//        className: "btn-ajout btn-primary waves-effect",
//        titleAttr: 'Ajouter un nouveau profil',
//        action: function () {
//            $("#Modal").modal('show');
//            ShowModal(0, "add");
//        }
//    });
//});

$("table").on('click', '.btn-mod', function () {
    tr_modif = $(this).parents('tr:first');
});

$("table").on('click', '.btn-sup', function () {
    tr_sup = $(this).parents('tr:first');
});

function ShowModal(id, etat, lib, listModule) {
    etat_action = etat;
    if (etat_action === 'modifier') {
        $("#Modal .modal-title").text("Modification d'un profil utilisateur");
        $("#id").val(id);
        $("#lib").val(lib).parents('div:first').addClass('focused');
        $("#modules").val(listModule.trim().split(" ")).selectpicker('refresh');
    }
    else {
        $("#id").val("");
        $("#modules").val("").selectpicker('refresh');
        $("#Modal .modal-title").text("Nouveau profil utilisateur");
        $("#Modal form input[type=text]").val("").parents('div.focused').removeClass('focused');
    }
}

$("#btn_valid").on("click", function (e) {
    e.preventDefault();
    var lib = $("#lib").val().trim();
    var id = $("#id").val();
    var modules = $("#modules").val();
    var antiforget = $("#Modal form input[name=__RequestVerificationToken]").val();
    if (lib === "") {
        ShowWarningMessage("Vous n'avez pas renseigné le libellé du profil!!!");
    }
    else if (modules == null) {
        ShowWarningMessage("Vous devez renseigner au moins un module accessible!!!");
    }
    else {
        $.post(url_post_action, { profil: { Id_drt: id, Lib_drt: lib }, modules: modules, __RequestVerificationToken: antiforget }).done(function (data) {
            if (data.state == false) ShowWarningMessage(data.message);
            else {
                $("#Modal").modal('hide');
                ShowMessageWithCallBack("Information enregistrée avec succès!!!", "Félicitation", "success", function () { window.location.reload() });
                //if (etat_action == "modifier") {
                //    dataTable.row(tr_modif).remove();
                //}
                //var profil = data.profil;
                //dataTable.row.add([
                //    //profil.Id_drt,
                //    profil.Lib_drt,
                //    profil.LibModules,
                //    `
                //            <button type="button" class ="btn bg-cyan btn-mod" data-toggle="modal" data-target="#Modal" onclick="ShowModal(${profil.Id_drt}, 'modifier','${profil.Lib_drt}','${profil.IdModules}') "><i class ="fa fa-edit"></i>Modifier</button>
                //            <button type="button" class ="btn btn-danger btn-sup" onclick="ConfirmSup(${profil.Id_drt})"><i class ="fa fa-trash"></i>Supprimer</button>
                //            `
                //]).draw(false);
                //$("#Modal").modal('hide');
                ////swal("Félicitation", "Information enregistrée avec succès!!!", "success");
                //ShowSuccessMessage();
            }
        }).fail(function (e) { console.log(e); });
    }
});

function ConfirmSup(id) {
    function action() {
        var url = url_delete.replace('0', id);
        $.get(url).done(function (data) {
            if (data.state) {
                //ici on recharge la page pour s'assurer que l'utilisateur a toujous acces à cette page apres suppression de ce profil
                ShowMessageWithCallBack("Profil supprimé avec succès!!!", "Information", "success", function () { window.location.reload() });
            }
            else {
                ShowErrorMessage(data.message);
            }
        });
    }
    ShowConfirmSupMessage(action, "Etes-vous sur de vouloir supprimer ce profil?");
}