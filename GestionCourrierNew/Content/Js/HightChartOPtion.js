﻿Highcharts.setOptions({
    lang: {
        contextButtonTitle: "Options",
        decimalPoint: ",",
        downloadCSV: "Telecharger CSv",
        downloadJPEG: "Telecharger JPEG",
        downloadPDF: "Telecharger PDF",
        downloadPNG: "Telecharger PNG",
        downloadSVG: "Telecharger SVG",
        downloadXLS: "Telecharger Excel",
        noData: "Aucune donnée disponible",
        printChart: "Imprimer le Graphique",
        viewData: "Afficher le tableau des données",
        viewFullscreen: "Afficher en plein écran",
    }
});
Highcharts.defaultOptions.exporting.buttons.contextButton.menuItems = ["viewFullscreen", "separator", "downloadPNG", "downloadJPEG", "downloadPDF", "downloadSVG", "separator", "downloadCSV", "downloadXLS"];
Highcharts.defaultOptions.tooltip.headerFormat='<span style="font-size:10px">{point.key}</span><table>';
Highcharts.defaultOptions.tooltip.pointFormat= '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y}</b></td></tr>';
Highcharts.defaultOptions.tooltip.footerFormat= '</table>',
Highcharts.defaultOptions.tooltip.shared= true,
Highcharts.defaultOptions.tooltip.useHTML= true
Highcharts.defaultOptions.title.text = "";
Highcharts.defaultOptions.subtitle.text = "";
Highcharts.defaultOptions.chart.defaultSeriesType="column";