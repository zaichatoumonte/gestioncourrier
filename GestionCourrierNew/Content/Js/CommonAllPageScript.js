﻿//variable à definir par la page appelente : DefaultExportMessage,dataTableClass
$.PageUtilities={
    //datatableDomElement:function(hasButton=false,hasLength=false){
    //    let dom="frtip";
    //    if(hasButton==true)dom+="B";
    //    if(hasLength==true)dom+="l";
    //    return dom;
    //},
    functionShowPresentSsMenu:function(class_ss_menu) {
        $('.sidebar .menu ul.list > li a').removeClass('toggled');
        $('.sidebar .menu ul.list > li > ul.ml-menu > li').removeClass('active');

        var selecteur = `.sidebar .menu ul.list  li.${class_ss_menu}`;
        var sous_menu = $(selecteur);
        if (sous_menu) {
            var is_ul_parent = $(sous_menu).parent("ul").length == 0 ? false : true;

            $(sous_menu).children('a').addClass('toggled');
            if (is_ul_parent) {
                $(sous_menu).addClass('active');
                //$(sous_menu).parents('li').children('a.menu-toggle').click();
                $(sous_menu).parents('li').children('a.menu-toggle').addClass('toggled');
            }
        }
    },
    FormatNowDate:function() {

        let today = new Date();
        let y = today.getFullYear();
        let m = today.getMonth() + 1;
        let j = today.getDate();
        let h = today.getHours();
        let min = today.getMinutes();
        if (m < 10) { m = "0" + m };
        if (j < 10) { j = "0" + j };
        if (h < 10) { h = "0" + h };
        if (min < 10) { min = "0" + min };
        return (j + '-' + m + '-' + y + " " + h + ":" + min);
    },
    getExportFileName:function ( buttonClass, message){
        swal({
            title: "Nom du fichier à exporter!",
            text: "Entrez le nom du fichier à exporter svp!!!",
            type: "input",
            showCancelButton: true,
            cancelButtonText: "Annuler",
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: "Entrez le nom du fichier svp !!!",
            inputValue: message
            },function (inputValue) {
                if (inputValue === false) return false;
                if (inputValue.trim() === "") {
                    swal.showInputError("Vous devez renseigner un nom de fichier!!!"); 
                    return false
                }
                ExportFileName = inputValue;
                $(buttonClass).click();
                swal.close();
        });
    },
}

DataTable={
    extendButtonType:
    {
        pdf:"pdf",
        excel:"excel",
        print:"print"
    },
    extendButtonFormat:function(affiche,hide){
	    this.affiche=affiche;
	    this.hide=hide;
    },
    createExtendButton:function(extendButtonType){
        let affiche,hide;
        if(![DataTable.extendButtonType.pdf,DataTable.extendButtonType.excel,DataTable.extendButtonType.print].includes(extendButtonType)){
            return null;
        }
        if(extendButtonType==DataTable.extendButtonType.pdf)
        {
            affiche={
                text: 'Excel',
                className: "waves-effect",
                action: function () { getExportFileName(".btn-excel",DefaultExportMessage); }
            };
            hide={
                extend: 'excel',
                exportOptions: ExportOptions,
                className: "waves-effect btn-excel",
                filename: function () { return ExportFileName }
            };
        }
        else if(extendButtonType==DataTable.extendButtonType.excel){
            affiche={
                text: 'Excel',
                className: "waves-effect",
                action: function () { getExportFileName(".btn-excel",DefaultExportMessage); }
            };
            hide={
                extend: 'excel',
                exportOptions: ExportOptions,
                className: "waves-effect btn-excel",
                filename: function () { return ExportFileName }
            };
        }
        else if(extendButtonType==DataTable.extendButtonType.print){
            affiche={
                text: 'Imprimer',
                className: "waves-effect",
                action: function () { getExportFileName(".btn-print",DefaultExportMessage); }
            },
            hide={
                extend: 'print',
                className: "waves-effect btn-print",
                exportOptions: ExportOptions,
                filename: function () { return ExportFileName },
                text: "IMPRIMER",
                extension: 'filepdf.pdf',
                //autoPrint: false,
                title: function () { return ExportFileName },
                //customize: function (win) {
                //                console.log(win);
                //                $(win.document.body)
                //                    .css('font-size', '10pt')
                //                $(win.document.body).find('#data_courrier').addClass('compact').css('font-size', '10pt');
                //}
            }
        }
        return new DataTable.extendButtonFormat(affiche,hide);
    },
    createDom:function(hasButton=false,hasLength=false){
        let dom="frtip";
        if(hasButton==true)dom+="B";
        if(hasLength==true)dom+="l";
        return dom;
    },
    
    CreateExportOptions:function(dataTableClassName){
        let length=$("."+dataTableClassName+" tr:first>th,table tr:first>td").length;
        let tab=[];
        for(var i=0;i<length-1;i++){tab.push(i);}
        return {columns:tab};
    },
    CreateDataTableOption:function(hasLength,LengthMenu,hasButton,ArrayExtendButtonType=[DataTable.extendButtonType.pdf,DataTable.extendButtonType.excel,DataTable.extendButtonType.print]){
        let option={
                        "language": { "url": "../../Plugins/jquery-datatable/French.json" },
                        responsive: true,
                        "autoWidth": false,
                        "pageLength": 10,
                        dom:new DataTable.createDom(hasButton,hasLength)
                   };
        if(hasLength==true){
            option["lengthMenu"]=LengthMenu==undefined?[10, 25, 50, 75, 100]:LengthMenu;
        }
        $(ArrayExtendButtonType).each(function(i,item){
            var boutons=DataTable.createExtendButton(DataTable.extendButtonType.print);
            if(option.buttons!=undefined){
                option.buttons.push(boutons.affiche,boutons.hide);
            }
            else{
                option["buttons"]=[boutons.affiche,boutons.hide];
            }
        });
        return option;
    },
    CreateDataTable:function(ExportMessage,exportFileName,exportOption,dataTableClassName,hasButton,hasLength,lengthMenu,ArrayExtendButtonType){
        let creationOption=DataTable.CreateDataTableOption(hasLength,lengthMenu,hasButton,ArrayExtendButtonType);
        return{
            datatableOption:{
                ExportMessage:ExportMessage,
                ExportFileName:exportFileName,
                ExportOptions:exportOption,
                ClassName:dataTableClassName
            },
            datatableInstance:$("."+dataTableClassName).DataTable(creationOption),
            AddButton:function(text,className,action,position){
                datatableInstance.buttons().add(position,{text:text,className:className,action:action});
            }
        };
    }
}