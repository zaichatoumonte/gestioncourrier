﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
//using GestionCourrierNew.Filter;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using GestionCourrierNew.RoleUtilisateur;

namespace GestionCourrierNew.Controllers.Parametrage
{
    [AuthorizedRoles(Role.Configuration)]
    [RoutePrefix("Parametrage/Service")]
    public class ServiceController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();
        // GET: Correspondant
        [Route("",Name = "parametrage.service")]
        public ActionResult Index()
        {
            var list = db.SERVICE.Where(e=>e.isDelete!=true).ToList();
            ViewData["list"] = list;
            return View("Parametrage/Service/Index");
        }

        [ValidateAntiForgeryToken, HttpPost]
        [Route("action", Name = "parametrage.service.action")]
        public JsonResult actionCreateAdd(SERVICE service)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(service.Lib_serv)) throw new Exception("Veuillez indiquer le libellé du service svp!!!");
                if (string.IsNullOrWhiteSpace(service.Civ_serv)) throw new Exception("Veuillez indiquer la civilité du responsable du service svp!!!");
                if (string.IsNullOrWhiteSpace(service.Resp_serv)) throw new Exception("Veuillez indiquer les noms et prenoms du responsable du service svp!!!");
                var exit = db.SERVICE.Where(e => e.isDelete != true && e.Lib_serv.ToLower() == service.Lib_serv.ToLower()).FirstOrDefault();
                if(exit!=null && exit.Id_serv != service.Id_serv)
                {
                    throw new Exception("Ce service existe déja");
                }
                if (service.Id_serv == 0)
                {
                    service.Ref_serv = "";
                    service.isDelete = false;
                    db.SERVICE.Add(service);
                }
                else
                {
                    var mod = db.SERVICE.Find(service.Id_serv);
                    mod.Lib_serv = service.Lib_serv;
                    mod.Resp_serv = service.Resp_serv;
                    mod.Civ_serv = service.Civ_serv;
                }
                db.SaveChanges();
                //else throw new Exception("Le libellé du service de courrier ne doit pas exceder 50 caractères et le nom du responsable 100 caractères !!!");/*db.GetValidationErrors().ToList()[0]. */
                return Json(new { state = true, service=new{service.Id_serv,service.Lib_serv,service.Resp_serv, service.Civ_serv } }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException e)
            {
                var exception = SharedController.ErrorPrinter(e.EntityValidationErrors);
                var listException = exception.Item1;
                var message = exception.Item2;
                return Json(new { state = false,message=message, listException = listException }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException)
            {
                return Json(new { state = false, message = "Les informations que vous avez entrées ne sont pas valident." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("delete/{id:int}", Name = "parametrage.service.delete")]
        public JsonResult delete(int id)
        {
            try
            {
                var service = db.SERVICE.Find(id);
                if (service != null)
                {
                    service.isDelete = true;
                    service.dateDelete = DateTime.Now;
                    //if (service.COURRIER.Count() > 0 || service.ADRESSER.Count()>0) throw new Exception("Vous ne pouvez pas supprimer ce service pour le moment car il est en relation avec plusieurs courriers");
                    //db.SERVICE.Remove(service);
                    db.SaveChanges();
                    return Json(new { state = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false, message = "Aucune donnée n'a été trouvée." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}