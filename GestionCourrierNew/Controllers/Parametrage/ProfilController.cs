﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using GestionCourrierNew.RoleUtilisateur;
//using GestionCourrierNew.Filter;

namespace GestionCourrierNew.Controllers.Parametrage
{
    /**
        Dans ce controller on s'assure qu'il y ait toujours un profil donnant acces au module de parametrage pour que l'application ne soit pas bloquée
    */
    [AuthorizedRoles(Role.Configuration)]
    [RoutePrefix("Parametrage/Profil")]
    public class ProfilController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();
        // GET: Profil
        [Route("", Name = "Parametrage.profil")]
        public ActionResult Index()
        {
            try
            {
                var modules = db.MODULE.OrderBy(e => e.Lib_mod).ToList();
                var profils = db.DROIT_ACCES.Where(e => e.isDelete != true).OrderBy(e => e.Lib_drt).ToList();
                ViewData["modules"] = modules;
                ViewData["profils"] = profils;
                return View("Parametrage/Profil/Index");
            }
            catch (Exception e)
            {
                return RedirectToActionPermanent("Index", "Error", new { message = e.Message });
            }
        }

        [ValidateAntiForgeryToken, HttpPost]
        [Route("Action", Name = "parametrage.profil.action")]
        public JsonResult actionCreateAdd(DROIT_ACCES profil, int[] modules)
        {
            try
            {
                int modulesLength = modules.Length;

                if (string.IsNullOrWhiteSpace(profil.Lib_drt)) throw new Exception("Veuillez indiquer le libellé du profil svp!!!");
                if (modules.Length == 0) throw new Exception("Veuillez indiquer au moins un module accéssible au profil svp!!!");

                //verifie si le libelle du profil n'est pas deja attribué
                var exist = db.DROIT_ACCES.Where(e => e.isDelete != true && e.Lib_drt.ToLower() == profil.Lib_drt.ToLower()).FirstOrDefault();
                if (exist != null && exist.Id_drt != profil.Id_drt)
                {
                    throw new Exception("Il existe déja un autre profil avec ce libellé!!!");
                }

                //liste des habilitations ayant le meme nombre  acces que le profil actuel
                var list_habilitation = db.DROIT_ACCES.Where(e=>e.isDelete!= true).Select(e => new { e.Id_drt,e.Lib_drt,acces = e.ACCEDER.Select(s => s.MODULE.Id_mod)}).Where(e=>e.acces.Count()==modulesLength).ToArray();

                //verifie s'il n'existe pas un autre profil avec les memes habilitations
                modules= modules.OrderBy(e => e).ToArray();//ordre permet de faire la recherche plus facilement
                foreach (var item in list_habilitation)
                {
                    var ordreCroissant=item.acces.OrderBy(e => e).ToArray();
                    bool memeDroit = true;
                    for(int i = 0; i < modulesLength; i++)
                    {
                        if (ordreCroissant[i] != modules[i])
                        {
                            memeDroit = false;
                            break;
                        }
                    }
                    if (memeDroit && profil.Id_drt!=item.Id_drt)
                    {
                        throw new Exception("Le profil <span class='font-bold text-info font-underline'>" + item.Lib_drt+ "</span> a les mêmes habilitations que le profil que vous éssayez de créer.");
                    }
                }
                if (profil.Id_drt == 0)
                {
                    db.DROIT_ACCES.Add(profil);
                    db.SaveChanges();
                }
                else
                {
					//on verifie si ce n'est pas le seul profil donnant acces au module de parametrage
                    var mod = db.DROIT_ACCES.Find(profil.Id_drt);
                    if (mod == null) return Json(new { state = false, message = "Le profil à modifier n'a pas été trouvé" });
					//verifie s il s'agit du profil administrateur et si l'utilisateur veut reduire les acces de ce profil
					//if (mod.ACCEDER.Count() >= db.MODULE.Count() - 1 && modulesLength != db.MODULE.Count() - 1) return Json(new { state = false, message = "Vous ne pouvez pas restreindre les droits de ce profil !!!" });

					/*
						on verifie d'abord s'il reste un seul module ayant acces au profil de parametrage
						on verifie ensuite s'il s'agit du profil actuel
						et on verifie enfin si la modification ne vas pas reduire lui retirer ce droit
					*/
					var parametrage = Role.Configuration.ToString();

					if (db.ACCEDER.Count(e=>e.MODULE.Role_cs==parametrage)==1 && 
						mod.ACCEDER.Count(e=>e.MODULE.Role_cs==parametrage)>0 && 
						!modules.Contains(db.MODULE.FirstOrDefault(e=>e.Role_cs==parametrage).Id_mod))
                        return Json(new { state = false, message = "Vous ne pouvez pas retirer le droit de parametrage de ce profil!!!" });
					mod.Lib_drt = profil.Lib_drt;
                    db.SaveChanges();
                }
                db.ACCEDER.RemoveRange(db.ACCEDER.Where(e => e.Id_drt == profil.Id_drt).ToList());
                foreach (var item in modules)
                {
                    db.ACCEDER.Add(new ACCEDER { Id_drt = profil.Id_drt, Id_mod = item });
                }
                db.SaveChanges();

                string LibModules = "", IdModules = "";

                foreach(var item in db.V_PROFIL.Where(e => e.Id_drt == profil.Id_drt).Distinct())
                {
                    LibModules += "<span>- " + item.Lib_mod + "<br/></span>";
                    IdModules += item.Id_mod + " ";
                }
                //foreach (var item in db.ACCEDER.Where(e => e.Id_drt == profil.Id_drt))
                //{
                    
                //    LibModules += "<span>- " + item.MODULE.Lib_mod + "<br/></span>";
                //    IdModules += item.Id_mod + " ";
                //}
                return Json(new { state = true, profil = new { profil.Id_drt, profil.Lib_drt, LibModules, IdModules = IdModules } }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException e)
            {
                var exception = SharedController.ErrorPrinter(e.EntityValidationErrors);
                var listException = exception.Item1;
                var message = exception.Item2;
                return Json(new { state = false, list = listException, message }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException)
            {
                return Json(new { state = false, message = "Les informations que vous avez entrées ne sont pas valident." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("delete/{id:int}", Name = "parametrage.profil.delete")]
        public JsonResult delete(int id)
        {
            try
            {
                //on desactive tous les comptes utilisateurs ayant ce profil
                var profil = db.DROIT_ACCES.Find(id);

				var parametrage = Role.Configuration.ToString();
                if (profil != null)
                {
                    //on ne supprime pas le profil administrateur
                    if (db.ACCEDER.Count(e => e.MODULE.Role_cs == parametrage) == 1 &&
						profil.ACCEDER.Count(e => e.MODULE.Role_cs == parametrage) > 0)
                    {
                        throw new Exception("Vous ne pouvez pas supprimer ce module!!!");
                    }
                    profil.isDelete = true;
                    profil.dateDelete = DateTime.Now;
                    var list_user = db.UTILISATEUR.Where(e => e.Id_drt == profil.Id_drt).ToList();
                    foreach (var item in list_user)
                    {
                        item.isActif_util = false;
                    }
                    db.ACCEDER.RemoveRange(db.ACCEDER.Where(e => e.Id_drt == id).ToList());
                    db.SaveChanges();
                    return Json(new { state = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false, message = "Aucune donnée n'a été trouvée." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}