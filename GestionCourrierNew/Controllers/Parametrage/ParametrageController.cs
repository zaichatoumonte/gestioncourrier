﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using GestionCourrierNew.RoleUtilisateur;
//using GestionCourrierNew.Filter;

namespace GestionCourrierNew.Controllers.Parametrage
{
    [AuthorizedRoles(Role.Configuration)]
    [RoutePrefix("Parametrage/Configuration")]
    public class ParametrageController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();
        // GET: Parametrage
        [Route("", Name = "parametrage.parametrage")]
        public ActionResult Index()
        {
            var l = db.PARAMETRAGE.ToList();
            ViewData["list"] = l;
            return View("Parametrage/Parametrage/Index");
        }

        [Route("Action", Name = "parametrage.parametrage.action")]
        [HttpPost,ValidateAntiForgeryToken]
        public JsonResult modifier(bool choix)
        {
            try
            {
                var nom = Request.Form["nom"].Trim();
                if (nom == "") throw new Exception("Veuillez indiquer le nom du directeur général svp.");
                var parametrage = db.PARAMETRAGE.FirstOrDefault();
                parametrage.Nom_directeur = nom;
                db.SaveChanges();

                return Json(new { state = true });
            }
            catch(Exception e)
            {
                return Json(new { state = false,e.Message });
            }
        }
    }
}