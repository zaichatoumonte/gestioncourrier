﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using System.Security.Cryptography;
using System.Text;
using GestionCourrierNew.Helper;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using GestionCourrierNew.RoleUtilisateur;

namespace GestionCourrierNew.Controllers.Parametrage
{
	//il faut faire attention au fait qu'il y ait toujours un profil ayant acces au module de parametrage
    [AuthorizedRoles(Role.Configuration)]
    [RoutePrefix("Parametrage/Utilisateur")]
    public class UtilisateurController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();

        // GET: Utilisateur
        [Route("", Name = "parametrage.utilisateur")]
        public ActionResult Index()
        {
            try
            {
                List<UTILISATEUR> list = db.UTILISATEUR.Where(e => e.isDelete != true).ToList();
                ViewData["list"] = list;

                List<DROIT_ACCES> listprofils = db.DROIT_ACCES.Where(e => e.isDelete != true).ToList();
                ViewData["listprofils"] = listprofils;

                return View("Parametrage/Utilisateur/Index", list);
            }
            catch (Exception e)
            {
                return RedirectToActionPermanent("Index", "Error", new { message = e.Message });
            }
           
        }

        [ValidateAntiForgeryToken, HttpPost]
        [Route("action", Name = "parametrage.utilisateur.action")]
        public JsonResult actionCreateAdd(UTILISATEUR utilisateur, string cf_password)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(utilisateur.Nom_util)) throw new Exception("Veuillez indiquer le nom de l'utilisateur svp!!!");

                if (utilisateur.Id_drt == 0) throw new Exception("Veuillez indiquer le profil de l'utilisateur svp!!!");

                if (string.IsNullOrWhiteSpace(utilisateur.Email_util)) throw new Exception("Veuillez indiquer le mail de l'utilisateur svp!!!");

                //le login, l'email et le nom de l'utilisateur doivent etre unique
                var exist = db.UTILISATEUR.Where(e => e.isDelete != true && e.Nom_util.ToLower() == utilisateur.Nom_util.ToLower()).FirstOrDefault();

                if (exist != null && utilisateur.Id_util != exist.Id_util)
                {
                    throw new Exception("Ce nom d'utilisateur existe déja!!!");
                }

                exist = db.UTILISATEUR.Where(e => e.isDelete != true && e.Login_util.ToLower() == utilisateur.Login_util.ToLower()).FirstOrDefault();

                if (exist != null && utilisateur.Id_util != exist.Id_util)
                {
                    throw new Exception("Ce Login existe déja!!!");
                }

                exist = db.UTILISATEUR.Where(e => e.isDelete != true && e.Email_util == utilisateur.Email_util).FirstOrDefault();
                if (exist != null && exist.Id_util != utilisateur.Id_util)
                {
                    throw new Exception("Ce mail d'utilisateur est utilisé par un autre compte!!!");
                }

                var user_mod = db.UTILISATEUR.Find(utilisateur.Id_util);
                //l'administrateur peut changer le login de l'utilisateur seulement lorsque le compt de celui-ci n'a pas encore été réinitialier

                if (utilisateur.Id_util == 0 || user_mod.IsUpdate != true)
                {
                    if (string.IsNullOrWhiteSpace(utilisateur.Login_util)) throw new Exception("Veuillez indiquer le login de l'utilisateur svp!!!");
                }

                if (utilisateur.Id_util == 0)
                {
                    if (string.IsNullOrWhiteSpace(utilisateur.PasswdHash_util)) throw new Exception("Veuillez indiquer le mot de passe de l'utilisateur svp!!!");
                    if (!utilisateur.PasswdHash_util.Equals(cf_password)) throw new Exception("Les mots de passe doivent être identique!!!");

                    utilisateur.PasswdHash_util = CodeHelper.GetMd5Hash(utilisateur.PasswdHash_util);
                    utilisateur.Date_creation_util = DateTime.Now;
                    utilisateur.isActif_util = true;
                    utilisateur.IsUpdate = false;
                    utilisateur.isDelete = false;
                    db.UTILISATEUR.Add(utilisateur);
                }
                else
                {
                    if (user_mod == null) throw new Exception("Désolé, Le compte utilisateur n'a pas été retrouvé!!!");

                    //verifier qu'on essai pas de modifier le profil du seul compte admin
                    if (IsUniqueAdministrator(utilisateur.Id_util) && utilisateur.Id_drt != user_mod.Id_drt /*user_mod.DROIT_ACCES.ACCEDER.Count()==db.MODULE.Count() && db.UTILISATEUR.Where(e => e.Id_drt == user_mod.Id_drt).Count() == 1 && utilisateur.Id_drt!=user_mod.Id_drt*/)
                    {
                        throw new Exception("Vous ne pouvez pas modifier le profil du seul compte administrateur actif de la plateforme!!!");
                    }
                    
                    if (user_mod.IsUpdate != true)
                    {
                        if (string.IsNullOrWhiteSpace(utilisateur.PasswdHash_util) != true)
                        {
                            if (!utilisateur.PasswdHash_util.Equals(cf_password)) throw new Exception("Les mots de passe doivent être identique!!!");
                            else
                            {
                                user_mod.PasswdHash_util = CodeHelper.GetMd5Hash(utilisateur.PasswdHash_util);
                            }
                        }
                        user_mod.Login_util = utilisateur.Login_util;
                    }
                    user_mod.Nom_util = utilisateur.Nom_util;
                    user_mod.Id_drt = utilisateur.Id_drt;
                    //user_mod.Date_fin_validite_util = utilisateur.Date_fin_validite_util;
                    user_mod.Email_util = utilisateur.Email_util;
                    utilisateur.isActif_util = user_mod.isActif_util;
                    utilisateur.Date_creation_util = user_mod.Date_creation_util;
                }
                db.SaveChanges();
                var retour = new
                {
                    utilisateur.Id_drt,
                    utilisateur.Id_util,
                    utilisateur.Nom_util,
                    utilisateur.Email_util,
                    Lib_drt = db.DROIT_ACCES.Find(utilisateur.Id_drt)?.Lib_drt,
                    isActif_util = utilisateur.isActif_util == false ? "Non actif" : "Actif",
                    Date_creation_util = utilisateur.Date_creation_util?.ToShortDateString(),
                    //Date_fin_validite_util = utilisateur.Date_fin_validite_util?.ToShortDateString(),
                };
                return Json(new { state = true, utilisateur = retour }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException e)
            {
                var exception = SharedController.ErrorPrinter(e.EntityValidationErrors);
                var listException = exception.Item1;
                var message = exception.Item2;
                return Json(new { state = false, listException = listException, message }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException)
            {
                return Json(new { state = false, message = "Les informations que vous avez entrées ne sont pas valident." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("delete/{id:int}", Name = "parametrage.utilisateur.delete")]
        public JsonResult delete(int id)
        {
            try
            {
                var utilisateur = db.UTILISATEUR.Find(id);
                //var IsAdmin = db.V_PROFIL_UTILISATEUR.Where(e => e.Id_util == utilisateur.Id_util).Count() == db.MODULE.Count();
                if(IsUniqueAdministrator(utilisateur.Id_util)/*IsAdmin && db.UTILISATEUR.Where(e => e.Id_drt == utilisateur.Id_drt).Count() == 1*/)
                {
                    throw new Exception("Vous ne pouvez pas supprimer le seul compte administrateur de la plateforme!!!");
                }
                if (utilisateur != null)
                {
                    utilisateur.isActif_util = false;
                    utilisateur.isDelete = true;
                    utilisateur.dateDelete = DateTime.Now;
                    db.SaveChanges();
                    return Json(new { state = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false, message = "Aucune donnée n'a été trouvée." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("desactive/{id:int}", Name = "parametrage.utilisateur.desactive")]
        public JsonResult desactive(int id)
        {
            try
            {
                var utilisateur = db.UTILISATEUR.Find(id);
                if (utilisateur != null)
                {
                    //var module_user = db.V_PROFIL_UTILISATEUR.Where(e => e.Id_util == id).Select(e => e.Id_mod).Distinct().ToList();
                    ////liste des utilisateur ayant le meme profil que l'utilisateur à supprimer
                    //var profil_admin = db.V_PROFIL_UTILISATEUR.Where(e => e.Id_drt == utilisateur.Id_drt && e.isActif_util == true).Select(e => e.Id_util).Distinct().ToList();
                    //verifie s'il s'agit du compte admin ( le compte admin a acces a tous les modules)
                    if (IsUniqueAdministrator(id)/*module_user.Count() == db.MODULE.Count() && profil_admin.Count() == 1*/)
                    {
                        return Json(new { state = false, message = "Vous ne pouvez pas désactiver ce compte car il s'agit du seul compte administrateur actif!!!" }, JsonRequestBehavior.AllowGet);
                    }
                    utilisateur.isActif_util = false;
                    db.SaveChanges();
                    return Json(new { state = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false, message = "Aucune donnée n'a été trouvée." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("active/{id:int}", Name = "parametrage.utilisateur.active")]
        public JsonResult active(int id)
        {
            try
            {
                var utilisateur = db.UTILISATEUR.Find(id);
                if (utilisateur != null && utilisateur.DROIT_ACCES.isDelete != true)
                {
                    utilisateur.isActif_util = true;
                    db.SaveChanges();
                    return Json(new { state = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false, message = "Aucune donnée n'a été trouvée." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("change-user-profil", Name = "parametrage.utilisateur.changeProfil")]
        public JsonResult ChangeProfil/*AndDateExpire*/(int id, int id_profil/*,DateTime date_expire*/)
        {
            try
            {
                var utilisateur = db.UTILISATEUR.Find(id);
                if (utilisateur != null && utilisateur.isActif_util==false)
                {
                    utilisateur.Id_drt = id_profil;
                    utilisateur.isActif_util = true;
                    db.SaveChanges();
                    return Json(new { state = true });
                }
                return Json(new { state = false, message = "Aucune donnée n'a été trouvée." });

            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message });
            }
        }

        [HttpGet]
        [Route("get-user-activity-info/{id:int}", Name = "parametrage.utilisateur.info")]
        public JsonResult GetInfoUser(int id)
        {
            try
            {
                var user = db.UTILISATEUR.Find(id);
                if (user != null)
                {
                    var valid_profil = user.DROIT_ACCES.isDelete ?? false;
                    //var compareDate = DateTime.Compare(user.Date_fin_validite_util.Value.Date, DateTime.Today.Date);
                    //var  valid_date = compareDate > 0 ? true : false;

                    return Json(new
                    {
                        state = true,
                        user.Id_drt,
                        valid_profil = !valid_profil,
                        //valid_date,
                        //date =user.Date_fin_validite_util.Value.ToShortDateString(),
                        user.Email_util,
                        user.IsUpdate,
                        user.Nom_util,
                        user.Login_util
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false, message = "Ce utilisateur n'existe pas" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        bool IsUniqueAdministrator(int id_user)
        {
            try
            {
                var utilisateur = db.UTILISATEUR.Find(id_user);
                if (utilisateur == null) return false;
                var isAdminAccount = utilisateur.DROIT_ACCES.ACCEDER.Count(e=>e.MODULE.Role_cs==Role.Configuration.ToString())>0;
                if (!isAdminAccount) return false;
                var nombre_compte_admin=db.V_PROFIL_UTILISATEUR.Where(e => e.Id_drt == utilisateur.Id_drt && e.isActif_util == true && e.isDelete!=true && e.Id_util!=id_user).Select(e => e.Id_util).Distinct().Count();
                if (nombre_compte_admin > 0) return false;
                else return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
   
}