﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using GestionCourrierNew.RoleUtilisateur;

namespace GestionCourrierNew.Controllers.Parametrage
{
    [Authorize]
    [AuthorizedRoles(Role.Configuration)]
    [RoutePrefix("Parametrage/Correspondant")]
    public class ContactController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();
        // GET: Contact
        [Route("", Name = "parametrage.contact")]
        public ActionResult Index()
        {
            var list = db.CORRESPONDANT.Where(e => e.isDelete != true).ToList();
            ViewData["list"] = list;
            var listType = db.TYPE_CORRESPONDANT.Where(e => e.isDelete != true).ToList();
            ViewData["listType"] = listType;
            return View("Parametrage/Contact/Index");
        }

        [HttpGet]
        [Route("delete/{id:int}", Name = "parametrage.contact.delete")]
        public JsonResult delete(int id)
        {
            try
            {
                var contact = db.CORRESPONDANT.Find(id);
                if (contact != null)
                {
                    contact.isDelete = true;
                    contact.dateDelete = DateTime.Now;
                    db.SaveChanges();
                    return Json(new { state = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false, message = "Aucune donnée n'a été trouvée." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("edit/{id:int}", Name = "parametrage.contact.edit")]
        public JsonResult edit(int id)
        {
            try
            {
                var contact = db.CORRESPONDANT.Find(id);
                if (contact != null)
                {
                    return Json(new { state = true,
                        contact = new {
                            contact.Id_cor,
                            contact.Id_typ_cor,
                            contact.Mail_cor,
                            contact.Nom_cor,
                            contact.Resp_cor,
                            contact.Tel_cor,
                            contact.Ad_cor,
                            contact.Civ_cor
                        }
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false, message = "Aucune donnée n'a été trouvée." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}