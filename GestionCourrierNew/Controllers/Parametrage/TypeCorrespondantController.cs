﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
//using GestionCourrierNew.Filter;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using GestionCourrierNew.RoleUtilisateur;

namespace GestionCourrierNew.Controllers.Parametrage
{
    [AuthorizedRoles(Role.Configuration)]
    [RoutePrefix("Parametrage/TypeCorrespondant")]
    public class TypeCorrespondantController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();
        // GET: TypeCorrespondant
        [Route("", Name = "parametrage.typeCorrespondant")]
        public ActionResult Index()
        {
            var list = db.TYPE_CORRESPONDANT.Where(e => e.isDelete != true).ToList();
            ViewData["list"] = list;
            return View("Parametrage/TypeCorrespondant/Index");
        }

        [ValidateAntiForgeryToken, HttpPost]
        [Route("action", Name = "parametrage.typeCorrespondant.action")]
        public JsonResult actionCreateAdd(TYPE_CORRESPONDANT types)
        {
            try
            {
                if (types.Lib_typ_cor == "") throw new Exception("Veuillez indiquer le libellé du type de correspondant svp!!!");
                var exist = db.TYPE_CORRESPONDANT.Where(e => e.isDelete != true && e.Lib_typ_cor.ToLower() == types.Lib_typ_cor.ToLower()).FirstOrDefault();
                if(exist!=null && exist.Id_typ_cor != types.Id_typ_cor)
                {
                    throw new Exception("Ce type de correspondant existe déja.");
                }
                if (types.Id_typ_cor == 0)
                {
                    types.isDelete = false;
                    db.TYPE_CORRESPONDANT.Add(types);
                }
                else
                {
                    var mod = db.TYPE_CORRESPONDANT.Find(types.Id_typ_cor);
                    mod.Lib_typ_cor = types.Lib_typ_cor;
                }
                db.SaveChanges();
                //else throw new Exception("Le libellé du type de correspondant ne doit pas exceder 50 caractères");
                return Json(new { state = true, types }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException e)
            {
                var exception = SharedController.ErrorPrinter(e.EntityValidationErrors);
                var listException = exception.Item1;
                var message = exception.Item2;
                return Json(new { state = false, listException = listException,message }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException)
            {
                return Json(new { state = false, message = "Les informations que vous avez entrées ne sont pas valident." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("delete/{id:int}", Name = "parametrage.typeCorrespondant.delete")]
        public JsonResult delete(int id)
        {
            try
            {
                var type = db.TYPE_CORRESPONDANT.Find(id);
                if (type != null)
                {
                    //db.TYPE_CORRESPONDANT.Remove(type);
                    type.isDelete = true;
                    type.dateDelete = DateTime.Now;
                    db.SaveChanges();
                    return Json(new { state = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false,message="Aucune donnée n'a été trouvée." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}