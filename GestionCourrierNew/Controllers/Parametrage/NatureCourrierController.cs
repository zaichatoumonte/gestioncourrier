﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using GestionCourrierNew.RoleUtilisateur;
//using GestionCourrierNew.Filter;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;

namespace GestionCourrierNew.Controllers.Parametrage
{
    [AuthorizedRoles(Role.Configuration)]
    [RoutePrefix("Parametrage/Nature")]
    public class NatureCourrierController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();
        // GET: NatureCourrier
        [Route("", Name = "parametrage.nature")]
        public ActionResult Index()
        {
            var list = db.NATURE.Where(e=>e.isDelete!=true).ToList();
            ViewData["list"] = list;
            return View("Parametrage/NatureCourrier/Index");
        }

        [ValidateAntiForgeryToken, HttpPost]
        [Route("Action", Name = "parametrage.nature.action")]
        public JsonResult actionCreateAdd(NATURE nature)
        {
            try
            {
                if (nature.Lib_nat.Trim() == "") throw new Exception("Veuillez indiquer le libellé de la nature de courrier svp!!!");
                var exist = db.NATURE.Where(e => e.isDelete != true && e.Lib_nat.ToLower() == nature.Lib_nat.ToLower()).FirstOrDefault();
                if(exist!=null && exist.Id_nat != nature.Id_nat)
                {
                    throw new Exception("Cette nature de courrier existe déja!!!");
                }
                if (nature.Id_nat == 0)
                {
                    nature.isDelete = false;
                    db.NATURE.Add(nature);
                }
                else
                {
                    var mod = db.NATURE.Find(nature.Id_nat);
                    mod.Lib_nat = nature.Lib_nat;
                }
                db.SaveChanges();
                return Json(new { state = true, nature }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException e)
            {
                var exception = SharedController.ErrorPrinter(e.EntityValidationErrors);
                var listException = exception.Item1;
                var message = exception.Item2;
                return Json(new { state = false,list=listException, message = e.Message}, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException)
            {
                return Json(new { state = false, message = "Les informations que vous avez entrées ne sont pas valident." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("Delete/{id:int}", Name = "parametrage.nature.delete")]
        public JsonResult delete(int id)
        {
            try
            {
                var nature = db.NATURE.Find(id);

                if (nature != null)
                {
                    nature.isDelete = true;
                    nature.dateDelete = DateTime.Now;
                    db.SaveChanges();
                    return Json(new { state = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false, message = "Aucune donnée n'a été trouvée." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}