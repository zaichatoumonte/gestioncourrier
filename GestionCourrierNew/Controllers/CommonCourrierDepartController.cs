﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using GestionCourrierNew.Helper;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using GestionCourrierNew.RoleUtilisateur;

namespace GestionCourrierNew.Controllers
{
	/*
		Les roles authorisé:
		traitement pour la creation de reponses au courrier
		Demande et receive pour la creation de courrier depart
		*/
    [AuthorizedRoles(Role.Receive,Role.Traitement,Role.Demande)]
    [RoutePrefix("Reception/CourrierDepart")]
    public class CommonCourrierDepartController : Controller
    {
        // GET: CommonCourrierDepart
        GestionCourrierEntities db = new GestionCourrierEntities();

        //static string ref_cour = "";

        int id_courrier_depart = new GestionCourrierEntities().TYPE_COURRIER.FirstOrDefault(e=>e.Lib_typ_cour.ToLower().Contains("part")).Id_typ_cour;
        int id_cat_st = new GestionCourrierEntities().CATEGORIE_COURRIER.FirstOrDefault(e => e.Lib_cat_cour.ToLower().Contains("soi")).Id_cat_cour;
        int id_cat_ca = new GestionCourrierEntities().CATEGORIE_COURRIER.FirstOrDefault(e => e.Lib_cat_cour.ToLower().Contains("admin")).Id_cat_cour;

		[HttpGet]
        [Route("edit/{id:int}", Name = "reception.courrierDepart.edit")]
        public JsonResult edit(int id)
        {
            try
            {
                var courrier_depart = db.COURRIER.Find(id);
                if (courrier_depart != null)
                {
                    //original_file_ref = courrier_depart.Ref_cour;
                    var service = courrier_depart.ECHANGER.Where(e => e.isDest == true && e.Id_serv != null).Select(e => new { e.Id_serv,e.SERVICE.Lib_serv}).ToList();

                    var contact = courrier_depart.ECHANGER.Where(e => e.isDest == true && e.Id_cor != null).Select(e => new { e.Id_cor,e.CORRESPONDANT.Nom_cor}).ToList();
                    
                    return Json(new
                    {
                        state = true,
                        courrier_depart = new
                        {
                            courrier_depart.Id_cour,
                            courrier_depart.Obj_cour,
                            courrier_depart.Ref_cour,
                            courrier_depart.Lien_cour,
                            courrier_depart.Id_nat,
							statut=courrier_depart.Stat_cour,
                            date = courrier_depart.Date_cour.ToShortDateString(),
                            agent = courrier_depart?.UTILISATEUR.Nom_util,
                            nom_fichier = courrier_depart.nom_fichier_joint ?? "",
                            chemin = courrier_depart.Lien_cour ?? "",
                            nature = courrier_depart.NATURE.Lib_nat,
                            courrier_depart.CATEGORIE_COURRIER.Lib_cat_cour,
                            list_contact = contact,
                            list_service = service,
                            courrier_depart.Cont_cour
                        }
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false, message = "Aucune donnée n'a été trouvée." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("get-code-courrier/{id}", Name = "reception.courrierDepart.getCodeCourrier")]
        public JsonResult getCodeCourrier(string id)
        {
            try
            {
                string code = "";
                code = SharedController.getCodeCourrier(id);
                if (code == string.Empty) throw new Exception("reference du  courrier non obtenu");
                //ref_cour = code;
                return Json(new { state = true, code }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateAntiForgeryToken, HttpPost]
        [Route("action", Name = "reception.courrierDepart.action")]
        public JsonResult actionCreateAdd(COURRIER courrier_depart, List<int> services, List<int> contacts, string typ_cour,int? id_courrier)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(courrier_depart.Ref_cour)) throw new Exception("Veuillez indiquer le code du courrier svp!!!");
                if (string.IsNullOrWhiteSpace(courrier_depart.Obj_cour)) throw new Exception("Veuillez indiquer l'objet du courrier svp!!!");
                //if (ref_cour == "" && courrier_depart.Id_cour == 0) throw new Exception("Le code du courrier n'est pas correct!!!");
                if ((services == null && contacts == null)) { throw new Exception("Veuillez indiquer au moins un destinataire svp!!!"); }
                var cont_cour = HttpUtility.UrlDecode(courrier_depart.Cont_cour);
                courrier_depart.Cont_cour = cont_cour;
                if(id_courrier!=null && id_courrier != 0)
                {
                    var courrierArr = db.COURRIER.Find(id_courrier);
                    if (courrierArr == null)
                    {
                        throw new Exception("Le courrier arrivé pour lequel vous voulez envoyer une reponse est introuvable!!!");
                    }
                }

                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    if (file.ContentLength == 0) throw new Exception("Aucun contenu dans le fichier");
                    if (file.ContentLength > 1024 * 1024 * 2) throw new Exception("Le ficher doit avoir une taille d'au plus 2Mo");
                    var guid = Guid.NewGuid();
                    string path = Server.MapPath("~"+UploadFileFolder.FolderDepart);
                    var nom_fichier = CodeHelper.UploadFile(file, path);
                    courrier_depart.Lien_cour = UploadFileFolder.FolderDepart + nom_fichier.Item2;
                    courrier_depart.nom_fichier_joint = nom_fichier.Item1;
                }

                if (courrier_depart.Id_cour == 0)
                {
                    var index = db.COURRIER.OrderByDescending(e => e.Id_cour).FirstOrDefault();
                    courrier_depart.Id_cour = index != null ? index.Id_cour + 1 : 1;
                    //courrier_depart.Ref_cour = ref_cour + courrier_depart.Ref_cour;
                    courrier_depart.Id_typ_cour = id_courrier_depart;
					if (string.IsNullOrWhiteSpace(courrier_depart.Stat_cour))
					{
						courrier_depart.Stat_cour = StatutCourrier.Traite.Item2;
					}
					courrier_depart.isDelete = false;
                    courrier_depart.Date_cour = DateTime.Now;
                    var utilisateur = AuthentificationController.GetUserAuthentificateData(Request);
                    courrier_depart.Id_util = utilisateur==null?0:utilisateur.Id_util;
                    if (typ_cour.ToLower().Contains("soi"))
                    {
                        courrier_depart.Id_cat_cour = id_cat_st;
                    }
                    else
                    {
                        courrier_depart.Id_cat_cour = id_cat_ca;
                    }
                    db.COURRIER.Add(courrier_depart);
                   
                    var parm = db.PARAMETRAGE.First();
                    if (typ_cour.ToLower().Contains("soi"))
                    {
                        parm.Num_cour_st += 1;
                    }
                    else
                    {
                        parm.Num_cour_ad += 1;
                    }
                   
                    db.SaveChanges();
                    if(id_courrier!=null && id_courrier != 0)
                    {
                        var arriverCourrier = db.COURRIER.Find(id_courrier);
                        if(arriverCourrier!= null)
                        {
                            arriverCourrier.Rep_cour = courrier_depart.Id_cour;
                            db.SaveChanges();
                        }
                    }
                }
                else
                {
                    var mod = db.COURRIER.Find(courrier_depart.Id_cour);
                    mod.Obj_cour = courrier_depart.Obj_cour;
                    mod.Id_nat = courrier_depart.Id_nat;
                    mod.Cont_cour = courrier_depart.Cont_cour;
					mod.Stat_cour = courrier_depart.Stat_cour;
                    if (!string.IsNullOrWhiteSpace(courrier_depart.Lien_cour))
                    {
                        mod.Lien_cour = courrier_depart.Lien_cour;
                        mod.nom_fichier_joint = courrier_depart.nom_fichier_joint;
                    }
                    db.ECHANGER.RemoveRange(db.ECHANGER.Where(e => e.Id_cour == courrier_depart.Id_cour && e.isDest==true));
                    courrier_depart.Id_cat_cour = mod.Id_cat_cour;
                    courrier_depart.Date_cour = mod.Date_cour;
					courrier_depart.Stat_cour = mod.Stat_cour;
                    db.SaveChanges();
                }
                var list_destinataire = new List<ECHANGER>();
                var date = DateTime.Now;
                if (services != null)
                {
                    foreach (var item in services)
                    {
                        list_destinataire.Add(new ECHANGER() { Id_serv = item, Id_cour = courrier_depart.Id_cour, Date_envoi = date,isDest=true });
                    }
                }
                if (contacts != null)
                {
                    foreach (var item in contacts)
                    {
                        list_destinataire.Add(new ECHANGER() { Id_cor = item, Id_cour = courrier_depart.Id_cour, Date_envoi = date,isDest=true });
                    }
                }
                db.ECHANGER.AddRange(list_destinataire);
                db.SaveChanges();
                return Json(new
                {
                    courrier = new
                    {
                        courrier_depart.Ref_cour,
                        date = courrier_depart.Date_cour.ToShortDateString(),
                        db.NATURE.Find(courrier_depart.Id_nat)?.Lib_nat,
                        courrier_depart.Obj_cour,
                        courrier_depart.Id_cour,
						courrier_depart.Stat_cour,
                        db.CATEGORIE_COURRIER.Find(courrier_depart.Id_cat_cour)?.Lib_cat_cour,
                    },
                    state = true,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException e)
            {
                var exception = SharedController.ErrorPrinter(e.EntityValidationErrors);
                var listException = exception.Item1;
                var message = exception.Item2;
                return Json(new { state = false, message, listException = listException }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException)
            {
                return Json(new { state = false, message = "Les informations que vous avez entrées ne sont pas valident." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}