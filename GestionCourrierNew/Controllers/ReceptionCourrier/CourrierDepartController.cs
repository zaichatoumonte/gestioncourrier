﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using GestionCourrierNew.RoleUtilisateur;

namespace GestionCourrierNew.Controllers.ReceptionCourrier
{
    [AuthorizedRoles(Role.Receive)]

    [RoutePrefix("Reception/CourrierDepart")]
    public class CourrierDepartController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();
        int id_courrier_depart = 0;
        [Route("", Name = "reception.courrierDepart")]
        public ActionResult Index()
        {
			ViewBag.Title = "Réception de Courrier Départs";
			id_courrier_depart = db.TYPE_COURRIER.FirstOrDefault(e => e.Lib_typ_cour.ToLower().Contains("part"))?.Id_typ_cour??0;
            var list_id_rep = db.COURRIER.Where(e => e.Rep_cour != null).Select(e => e.Rep_cour).ToList();
            var list = db.COURRIER.Where(e=>e.isDelete!=true && e.Id_typ_cour==id_courrier_depart && list_id_rep.Contains(e.Id_cour)==false).ToList();
            var nature = db.NATURE.Where(e => e.isDelete != true).ToList();
            var service = db.SERVICE.Where(e => e.isDelete != true).ToList();
            var contact = db.CORRESPONDANT.Where(e => e.isDelete != true).GroupBy(e => e.TYPE_CORRESPONDANT).ToList();

            ViewBag.courriers = list;
            ViewBag.nature = nature;
            ViewBag.services = service;
            ViewBag.contacts = contact;

            return View("ReceptionCourrier/CourrierDepart/Index");
        }

        [HttpGet]
        [Route("delete/{id:int}", Name = "reception.courrierDepart.delete")]
        public JsonResult delete(int id)
        {
            try
            {
                var courrier = db.COURRIER.Find(id);
                if (courrier != null)
                {
                    courrier.isDelete = true;
                    courrier.dateDelete = DateTime.Now;
                    db.SaveChanges();
                    return Json(new { state = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false,message="Ce courrier n'existe pas" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}