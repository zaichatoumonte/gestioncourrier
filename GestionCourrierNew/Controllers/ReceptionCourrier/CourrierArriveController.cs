﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using GestionCourrierNew.Helper;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using GestionCourrierNew.RoleUtilisateur;

namespace GestionCourrierNew.Controllers.ReceptionCourrier
{
    [AuthorizedRoles(Role.Receive)]
    [RoutePrefix("Reception/CourrierEntrant")]
    public class CourrierArriveController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();
        // GET: CourrierArrive
        [Route("", Name = "reception.courrierArrive")]
        public ActionResult Index()
        {
            var etat = new List<Tuple<int, string>>() {StatutCourrier.Non_traite,StatutCourrier.Attente };
            var nature = db.NATURE.Where(e => e.isDelete != true).ToList();
            var service = db.SERVICE.Where(e => e.isDelete != true).ToList();
            var contact = db.CORRESPONDANT.Where(e => e.isDelete != true).ToList();
            ViewBag.etats = etat;
            ViewBag.nature = nature;
            ViewBag.services = service;
            ViewBag.contacts = contact;
            return View("ReceptionCourrier/CourrierArrive/Index");
        }

        [ValidateAntiForgeryToken, HttpPost]
        [Route("action", Name = "reception.courrierArrive.action")]
        public JsonResult actionCreateAdd(COURRIER courrier_arrive,int?id_cor,int?id_serv)
        {
            try
            {
                if((id_cor==null || id_cor== 0) &&(id_serv == null || id_serv==0))
                {
                    throw new Exception("Veuillez indiquer l'expéditeur du courrier svp!!!");
                }

                if (string.IsNullOrWhiteSpace(courrier_arrive.Ref_cour)) throw new Exception("Veuillez indiquer le code du courrier svp!!!");

                if (string.IsNullOrWhiteSpace(courrier_arrive.Obj_cour)) throw new Exception("Veuillez indiquer l'objet du courrier svp!!!");

                var code_exist = db.COURRIER.SingleOrDefault(e => e.Ref_cour.ToLower() == courrier_arrive.Ref_cour.ToLower() && e.isDelete != true);
                if (code_exist != null && courrier_arrive.Id_cour != code_exist.Id_cour)
                {
                    throw new Exception("Ce code courrier est déja attribué!!!");
                }

                if (Request.Files.Count > 0)
                {
                    HttpPostedFileBase file = Request.Files[0];
                    if (file.ContentLength == 0) throw new Exception("Aucun contenu dans le fichier");
                    if (file.ContentLength > 1024 * 1024 * 40) throw new Exception("Le ficher doit avoir une taille d'au plus 40Mo");
                    var guid = Guid.NewGuid();
                    string path = Server.MapPath("~"+UploadFileFolder.FolderArrive);
                    var nom_fichier = CodeHelper.UploadFile(file, path);
                    courrier_arrive.Lien_cour = UploadFileFolder.FolderArrive + nom_fichier.Item2;
                    courrier_arrive.nom_fichier_joint = nom_fichier.Item1;
                }

                if (courrier_arrive.Id_cour == 0)
                {
                    var index = db.COURRIER.OrderByDescending(e => e.Id_cour).FirstOrDefault();
                    courrier_arrive.Id_cour = index != null ? index.Id_cour + 1 : 1;
                    courrier_arrive.Date_cour = DateTime.Now;
                    courrier_arrive.isDelete = false;
                    courrier_arrive.Stat_cour = StatutCourrier.Attente.Item2;
                    courrier_arrive.Id_typ_cour = db.TYPE_COURRIER.First(e=>e.Lib_typ_cour.ToLower().Contains("arriv")).Id_typ_cour;
                    var user_connected = AuthentificationController.GetUserAuthentificateData(Request);
                    courrier_arrive.Id_util = user_connected != null ? user_connected.Id_util : 0;
                    db.COURRIER.Add(courrier_arrive);
                    db.SaveChanges();
                    try
                    {
                        db.ECHANGER.Add(new ECHANGER() { Id_cor = id_cor, Id_serv = id_serv, isDest = false, Date_envoi = DateTime.Now, Id_cour = courrier_arrive.Id_cour });
                        db.SaveChanges();
                        return Json(new { state = true });
                    }
                    catch (Exception)
                    {
                        db.COURRIER.Remove(courrier_arrive);
                        db.SaveChanges();
                        return Json(new { state = false, message = "Une erreur est survenue lors de la tentative d'enregistrement du courrier." });
                    }
                }
                else
                {
                    var mod = db.COURRIER.Find(courrier_arrive.Id_cour);
                    if (mod != null)
                    {
                        mod.Obj_cour = courrier_arrive.Obj_cour;
                        mod.Ref_cour = courrier_arrive.Ref_cour;
                        var expediteur = db.ECHANGER.First(e => mod.Id_cour == e.Id_cour && e.isDest != true);
                        expediteur.Id_serv = id_serv;
                        expediteur.Id_cor = id_cor;
                        mod.Id_nat = courrier_arrive.Id_nat;
                        if (!string.IsNullOrWhiteSpace(courrier_arrive.Lien_cour))
                        {
                            mod.Lien_cour = courrier_arrive.Lien_cour;
                            mod.nom_fichier_joint = courrier_arrive.nom_fichier_joint;
                        }
                        db.SaveChanges();
                        var info = db.COURRIER.FirstOrDefault(e => e.Id_cour == courrier_arrive.Id_cour);
                        return Json(new
                        {
                            state = true,
                            courrier = new
                            {
                                info.Id_cour,
                                info.Obj_cour,
                                info.Ref_cour,
                                info.NATURE?.Lib_nat,
                                info.TYPE_COURRIER.Lib_typ_cour,
                                date = info.Date_cour.ToShortDateString()
                            }
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else return Json(new { state = false, message = "Vous essayez de modifier un courrier qui n'existe pas!!!" });
                }
                
            }
            catch (DbEntityValidationException e)
            {
                var exception = SharedController.ErrorPrinter(e.EntityValidationErrors);
                var listException = exception.Item1;
                var message = exception.Item2;
                return Json(new { state = false,message, listException = listException }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException)
            {
                return Json(new { state = false, message = "Les informations que vous avez entrées ne sont pas valident." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("delete/{id:int}", Name = "reception.courrierArrive.delete")]
        public JsonResult delete(int id)
        {
            try
            {
                var courrier = db.COURRIER.Find(id);
                if (courrier != null)
                {
                    courrier.isDelete = true;
                    courrier.dateDelete = DateTime.Now;
                    if (courrier.Rep_cour != null)
                    {
                        //on supprime la reponse pour que les statistique du tableau de bord ne soit pas faussé
                        var reponse = db.COURRIER.Find(courrier.Rep_cour);
                        reponse.isDelete = true;
                    }
                    db.SaveChanges();
                    return Json(new { state = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false,message="Ce courrier n'existe pas" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("edit/{id:int}", Name = "reception.courrierArrive.edit")]
        public JsonResult edit(int id)
        {
            try
            {
                var courrier_arrive = db.COURRIER.Find(id);
                if (courrier_arrive != null)
                {
                    return Json(new
                    {
                        state = true,
                        courrier_arrive = new
                        {
                            courrier_arrive.Id_cour,
                            courrier_arrive.Obj_cour,
                            courrier_arrive.Ref_cour,
                            courrier_arrive.ECHANGER.First(e=>e.isDest!=true).Id_cor,
							courrier_arrive.ECHANGER.First(e => e.isDest != true)?.CORRESPONDANT?.Nom_cor,
							courrier_arrive.ECHANGER.First(e => e.isDest != true).Id_serv,
							courrier_arrive.ECHANGER.First(e => e.isDest != true)?.SERVICE?.Lib_serv,
							courrier_arrive.Lien_cour,
                            courrier_arrive.Id_nat,
                            date = courrier_arrive.Date_cour.ToShortDateString(),
                            agent = courrier_arrive.UTILISATEUR.Nom_util,
                            nom_fichier = courrier_arrive.nom_fichier_joint ?? "",
                            chemin = courrier_arrive.Lien_cour ?? "",
                            statut = courrier_arrive.Stat_cour,
                            nature = courrier_arrive.NATURE?.Lib_nat
                        }
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false, message = "Aucune donnée n'a été trouvée." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("Mise-en-attente-de-traitement/{id:int}", Name = "reception.courrierArrive.attente")]
        public JsonResult MiseAttenteDeTraitement(int id)
        {
            try
            {
                var courrier = db.COURRIER.Find(id);
                if (courrier != null)
                {
                    courrier.Stat_cour = StatutCourrier.Attente.Item2;
                    db.SaveChanges();
                    return Json(new { state = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false,message= "Ce courrier n'existe pas" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("Classe-en-non-traité/{id:int}", Name = "reception.courrierArrive.classeNonTraite")]
        public JsonResult ClasserNonTraite(int id)
        {
            try
            {
                var courrier = db.COURRIER.Find(id);
                if (courrier != null)
                {
                    courrier.Stat_cour = StatutCourrier.Non_traite.Item2;
                    db.SaveChanges();
                    return Json(new { state = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false,message= "Ce courrier n'existe pas" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("Recherche-list-courrier-par-statut/{id:int}", Name = "reception.courrierArrive.search")]
        public JsonResult RechercheCourrier(int id)
        {
            try
            {
                var id_courrier_arriv = db.TYPE_COURRIER.Where(e => e.Lib_typ_cour.ToLower().Contains("arriv")).FirstOrDefault().Id_typ_cour;
                var courriers = db.COURRIER.Where(e=>e.isDelete!=true && e.Id_typ_cour==id_courrier_arriv && (e.Stat_cour == StatutCourrier.Attente.Item2 || e.Stat_cour == StatutCourrier.Non_traite.Item2)).ToList();
                if (id != 0)
                {
                    var statut = id == StatutCourrier.Attente.Item1 ? StatutCourrier.Attente.Item2 : StatutCourrier.Non_traite.Item2;
                    courriers = courriers.Where(e => e.Stat_cour == statut).ToList();
                }
                return Json(new
                {
                    state = true,
                    list = courriers.Select(e => new {
                        e.Id_cour,
                        e.Ref_cour,
                        e.Obj_cour,
                        Date_cour = e.Date_cour.ToShortDateString(),
                        e.NATURE.Lib_nat,
                        expediteur = e.ECHANGER.FirstOrDefault(x => x.isDest != true).Id_serv != null ? "Service : " + e.ECHANGER.First(x => x.isDest != true).SERVICE.Lib_serv : e.ECHANGER.First(x => x.isDest != true).CORRESPONDANT?.Nom_cor,
                        //expediteur = e.Id_cor_cont != null ? e.Nom_cont : "Service : " + e.Lib_serv,
                        statut = e.Stat_cour
                    }).ToList()
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}