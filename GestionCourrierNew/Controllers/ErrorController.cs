﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionCourrierNew.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index( string code=null,string message=null)
        {
            message = message ?? "Désolé, nous avons rencontré une erreur lors du traitement de votre requête!!!!";
            code = code ?? "Oops";
            switch (code)
            {
                case "404": message = "Cette page n'existe pas ou a été supprimée!!!";
                    break;
                case "401": message = "Vous n'avez pas les autorisations nécessaires pour accéder à cette page!!!";
                    break;
            }
            ViewData["message"] = message;
            ViewData["code"] = code;

            return View("error");
        }
    }
}