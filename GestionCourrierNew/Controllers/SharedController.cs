﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using System.Data.Entity.Validation;

namespace GestionCourrierNew.Controllers
{
    [RoutePrefix("Shared")]
    [Authorize]
    public class SharedController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();
        // GET: Shared
        [HttpGet]
        [Route("get-code-courrier/{id}", Name = "shared.courrierDepart.getCodeCourrier")]
        public static string getCodeCourrier(string type)
        {
            try
            {
                int compte = 0;
                string code = "";
                GestionCourrierEntities db = new GestionCourrierEntities();
                var dg = db.PARAMETRAGE.First().Nom_directeur.Split(' ').Select(e => e.Substring(0, 1)).ToList();
                var initiaux = "";
                foreach (var item in dg)
                {
                    initiaux += item;
                }
                initiaux = initiaux.ToUpper();
                if (type == "st")
                {
                    compte = db.PARAMETRAGE.First().Num_cour_st;
                    var d = DateTime.Now.ToString("yy");
                    code = "N/ref/" + compte + "/ST/" + d + "/CNTIG/DG/SG/" + initiaux + "/";
                }
                else
                {
                    compte = db.PARAMETRAGE.First().Num_cour_ad;
                    var d = DateTime.Now.ToString("yy");
                    code = "N/ref/" + compte + "/CA/" + d + "/CNTIG/DG/SG/" + initiaux + "/";
                }
                return code;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        [HttpGet]
        [Route("get-destinataires-courrier/{id}", Name = "shared.courrier.destinataires")]
        public JsonResult getDestinatairesCourriers(int id)
        {
            try
            {
                var destCont = db.ECHANGER.Where(e => e.Id_cour == id && e.Id_cor!=null && e.isDest==true).Select(e => new { e.Id_cor, e.CORRESPONDANT.Nom_cor,e.CORRESPONDANT.Civ_cor, Resp_cor=e.CORRESPONDANT.Resp_cor??"" }).ToList();

                var destServ = db.ECHANGER.Where(e => e.Id_cour == id && e.Id_serv!= null && e.isDest == true).Select(e => new { e.Id_serv, e.SERVICE.Lib_serv,e.SERVICE.Civ_serv,e.SERVICE.Resp_serv }).ToList();

                return Json(new { state = true,data=new { destCont,destServ} }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { state = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("get-validation-error", Name = "shared.validation.error")]
        public static Tuple<List<ValidationError>,string> ErrorPrinter(IEnumerable<DbEntityValidationResult> errors)
        {
            try
            {
                var list = new List<ValidationError>();
                var message = "Liste des Erreurs Survenues:<br>";
                foreach (var error in errors)
                {
                    foreach (var item in error.ValidationErrors)
                    {
                        //Console.WriteLine("Entité : {0}", error.Entry.Entity.GetType().Name);/*obtenir le nom de l'entite concernée*/
                        //Console.WriteLine("{0} : {1}", item.PropertyName, item.ErrorMessage);
                        list.Add(new ValidationError()
                        {
                            EntityType = error.Entry.Entity.GetType().Name,
                            PropertyName = item.PropertyName,
                            ErrorText = item.ErrorMessage
                        });
                        message += item.ErrorMessage + "<br>";
                    }
                }
                message = message.Trim();

                return new Tuple<List<ValidationError>,string>(list,message);
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        [HttpGet]
        [Route("get-list-courrier", Name = "shared.courrier.list")]
        public JsonResult getListCourrier()
        {
            try
            {
                var list=db.V_COURRIER.ToList();
                return Json(new { list, state = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message },JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("get-info-courrier/{id}", Name = "shared.courrier.detail")]
        public JsonResult getDetailsCourriers(int id)
        {
            try
            {
                var courrier = db.COURRIER.Find(id);
                var expediteur = courrier.ECHANGER.Where(e => e.isDest != true).FirstOrDefault();
                if (courrier != null)
                {
                    return Json(new
                    {
                        state=true,
                        courrier = new
                        {
                            courrier.Id_cour,
                            Ref_cour_rep = courrier.Rep_cour == null ? "" : db.COURRIER.Find(courrier.Rep_cour).Ref_cour,
                            courrier.Ref_cour,
                            courrier.Obj_cour,
                            courrier.Date_cour,
                            courrier.NATURE.Lib_nat,
                            courrier.UTILISATEUR.Nom_util,
                            courrier.Stat_cour,
                            courrier.TYPE_COURRIER.Lib_typ_cour,
                            nom_fichier_joint=courrier.nom_fichier_joint??"",
                            courrier.Lien_cour,
                            Lib_cat_cour = courrier.Id_cat_cour == null ? "" : courrier.CATEGORIE_COURRIER.Lib_cat_cour
                        },
                        expediteur = expediteur == null ? null :
                                   expediteur.Id_cor != null ? new Contact
                                   {
                                       Nom = expediteur.CORRESPONDANT.Nom_cor,
                                       Resp = expediteur.CORRESPONDANT.Resp_cor,
                                       Civ = expediteur.CORRESPONDANT.Civ_cor,
                                       Type = expediteur.CORRESPONDANT.TYPE_CORRESPONDANT.Lib_typ_cor,
                                   } :
                                   new Contact
                                   {
                                       Nom = expediteur.SERVICE.Lib_serv,
                                       Resp = expediteur.SERVICE.Resp_serv,
                                       Civ = expediteur.SERVICE.Civ_serv,
                                       Type = "Service"
                                   },
                        destinataire_service = courrier.ECHANGER.Where(dest => dest.isDest == true && dest.Id_serv != null).Select(dest => new { dest.Id_cor, dest.Id_serv, dest.SERVICE.Lib_serv, dest.SERVICE.Civ_serv, dest.SERVICE.Resp_serv }),

                        destinataire_contact = courrier.ECHANGER.Where(dest => dest.isDest == true && dest.Id_cor != null).Select(dest => new { dest.Id_cor, dest.Id_serv, dest.CORRESPONDANT.Nom_cor, dest.CORRESPONDANT.Civ_cor, dest.CORRESPONDANT.Ref_cor, dest.CORRESPONDANT.Resp_cor, dest.CORRESPONDANT.TYPE_CORRESPONDANT.Lib_typ_cor }),
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false, message = "Aucune donnee trouvé" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("get-list-courrier-depart", Name = "shared.courrier.listCourrierDepart")]
        public JsonResult getListCourrierDepart()
        {
            try
            {
                var list_id_rep = db.COURRIER.Where(e=>e.Rep_cour!=null).Select(e => e.Rep_cour).ToList();
                var id_depart = db.TYPE_COURRIER.Where(e => e.Lib_typ_cour.ToLower().Contains("part")).First().Id_typ_cour;
                var list = db.V_COURRIER.Where(e =>e.isDelete!=true && list_id_rep.Contains(e.Id_cour)==false && e.Id_typ_cour==id_depart).ToList();
                var final_list = list.GroupBy(e => new
                {
                    e.Id_cour,
                    e.Ref_cour,
                    e.Obj_cour,
                    e.Date_cour,
                    e.Lib_nat,
                    e.Nom_util,
                    e.Stat_cour,
                    e.Lib_typ_cour,
                    e.nom_fichier_joint,
                    e.Lien_cour,
                    Lib_cat_cour = e.Lib_cat_cour,
                })
                .Select(e => new {
                    courrier = e.Key,
                    services = e.Where(x => x.Id_serv != null).Select(x => new { x.Lib_serv, x.Resp_serv, x.Civ_serv }),
                    contacts = e.Where(x => x.Id_cor != null).Select(x => new { x.Nom_cor, x.Lib_typ_cor, x.Civ_cor, Resp_cor = x.Resp_cor ?? "Non defini" })
                }).ToList();

                return Json(new
                {
                    state = true,
                    list=final_list
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("get-list-courrier-reponse", Name = "shared.courrier.listReponseCourrier")]
        public JsonResult getListCourrierReponse()
        {
            try
            {
                //on affiche les reponse pour les courriers qui n'ont pas été supprimé
                var list_id_rep = db.COURRIER.Where(e =>e.isDelete!=true && e.Rep_cour != null).Select(e => e.Rep_cour).ToList();
                var id_depart = db.TYPE_COURRIER.Where(e => e.Lib_typ_cour.ToLower().Contains("part")).First().Id_typ_cour;
                var list = db.V_COURRIER.Where(e => e.isDelete != true && list_id_rep.Contains(e.Id_cour)==true && e.Id_typ_cour == id_depart).ToList();
                var final_list = list.GroupBy(e => new
                {
                    e.Id_cour,
                    e.Ref_cour,
                    Ref_cour_arr=db.COURRIER.Where(x=>x.Rep_cour==e.Id_cour).First()?.Ref_cour,
                    e.Obj_cour,
                    e.Date_cour,
                    e.Lib_nat,
                    e.Nom_util,
                    e.Stat_cour,
                    e.Lib_typ_cour,
                    e.nom_fichier_joint,
                    e.Lien_cour,
                    Lib_cat_cour = e.Lib_cat_cour,
                })
                .Select(e => new {
                    courrier = e.Key,
                    services = e.Where(x => x.Id_serv != null).Select(x => new { x.Lib_serv, x.Resp_serv, x.Civ_serv }),
                    contacts = e.Where(x => x.Id_cor != null).Select(x => new { x.Nom_cor, x.Lib_typ_cor, x.Civ_cor, Resp_cor = x.Resp_cor ?? "Non defini" })
                }).ToList();

                return Json(new
                {
                    state = true,
                    list = final_list
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("get-list-courrier-arrive", Name = "shared.courrier.listCourrierArrive")]
        public JsonResult getListCourrierArrive()
        {
            try
            {
                var id_arrive = db.TYPE_COURRIER.Where(e => e.Lib_typ_cour.ToLower().Contains("arr")).First().Id_typ_cour;
                var list = db.V_COURRIER.Where(e => e.isDelete != true && e.Id_typ_cour == id_arrive).ToList();
                var final_list = list.GroupBy(e => new
                {
                    e.Id_cour,
                    e.Ref_cour,
                    Ref_cour_rep=db.COURRIER.Find(e.Rep_cour)?.Ref_cour,
                    e.Obj_cour,
                    e.Date_cour,
                    e.Lib_nat,
                    e.Nom_util,
                    e.Stat_cour,
                    e.Lib_typ_cour,
                    e.nom_fichier_joint,
                    e.Lien_cour,
                })
                .Select(e => new {
                    courrier = e.Key,
                    services = e.Where(x => x.Id_serv != null).Select(x => new { x.Lib_serv, x.Resp_serv, x.Civ_serv }),
                    contacts = e.Where(x => x.Id_cor != null).Select(x => new { x.Nom_cor, x.Lib_typ_cor, x.Civ_cor, Resp_cor = x.Resp_cor ?? "Non defini" })
                }).ToList();

                return Json(new
                {
                    state = true,
                    list = final_list
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}