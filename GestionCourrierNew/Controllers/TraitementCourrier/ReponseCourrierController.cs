﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using GestionCourrierNew.Helper;
using GestionCourrierNew.RoleUtilisateur;

namespace GestionCourrierNew.Controllers.TraitementCourrier
{
    [AuthorizedRoles(Role.Traitement)]
    [RoutePrefix("traitement/reponse")]
    public class ReponseCourrierController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();
       
        //static string ref_cour ="";

        // GET: ReponseCourrier
        [Route("", Name = "traitement.reponse")]
        public ActionResult Index()
        {
            var id_courrier_arriv = db.TYPE_COURRIER.Where(e => e.Lib_typ_cour.ToLower().Contains("arriv")).FirstOrDefault().Id_typ_cour;
            var list_courrier = db.COURRIER.Where(e => e.isDelete!=true && e.Stat_cour==StatutCourrier.Traite.Item2 && e.Id_typ_cour==id_courrier_arriv).ToList();
            var service = db.SERVICE.Where(e => e.isDelete != true).ToList();
            var contact = db.CORRESPONDANT.Where(e => e.isDelete != true).GroupBy(e => e.TYPE_CORRESPONDANT).ToList();
            var nature = db.NATURE.Where(e => e.isDelete != true).ToList();
            ViewBag.courriers = list_courrier;
            ViewBag.services = service;
            ViewBag.contacts = contact;
            ViewBag.nature = nature;
            return View("TraitementCourrier/ReponseCourrier/Index");
        }

        [HttpGet]
        [Route("delete-reponse-courrier/{id_cour:int}/{id_cour_rep:int}", Name = "traitement.reponse.delete")]
        public JsonResult deleteReponseCourrier(int id_cour,int id_cour_rep)
        {
            try
            {
                var courrier=db.COURRIER.Find(id_cour);
                var reponse = db.COURRIER.Find(id_cour_rep);
                if (courrier != null && reponse!=null)
                {
                    courrier.Rep_cour = null;
                    reponse.isDelete = true;
                    reponse.dateDelete = DateTime.Now;
                    db.SaveChanges();
                    return Json(new { state = true}, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false, message="Courrier introuvable dans la base!!!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}