﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using GestionCourrierNew.Helper;
using GestionCourrierNew.RoleUtilisateur;

namespace GestionCourrierNew.Controllers.TraitementCourrier
{
    [AuthorizedRoles(Role.Traitement)]

    [RoutePrefix("Traitement/Suivi")]
    public class SuiviCourrierController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();
        // GET: SuiviCourrier
        [Route("", Name = "traitement.suivi")]
        public ActionResult Index()
        {
            //liste des courriers en attente de traitement et traité pas les reponses ni les courriers depart en attente de traitement car ils doivent d'abord etre valider
            var list_id_rep = db.COURRIER.Where(e => e.Rep_cour != null).Select(e => e.Rep_cour).ToList();
			var id_courrier_dep = db.TYPE_COURRIER.FirstOrDefault(e => e.Lib_typ_cour.ToLower().Contains("part")).Id_typ_cour;
			var list_courrier = db.COURRIER.Where(e=>
				e.Stat_cour!=StatutCourrier.Non_traite.Item2 && 
				e.isDelete!=true && 
				list_id_rep.Contains(e.Id_cour)==false &&
				!(e.Stat_cour==StatutCourrier.Attente.Item2 && e.Id_typ_cour==id_courrier_dep)
			).ToList();
            var service = db.SERVICE.Where(e => e.isDelete != true).ToList();
            var contact = db.CORRESPONDANT.Where(e => e.isDelete != true).GroupBy(e => e.TYPE_CORRESPONDANT).ToList();
            ViewBag.courriers = list_courrier;
            ViewBag.services = service;
            ViewBag.contacts = contact;
            return View("TraitementCourrier/SuiviCourrier/Index");
        }

        [HttpGet]
        [Route("edit/{id:int}", Name = "traitement.suivi.edit")]
        public JsonResult edit(int id)
        {
            try
            {
                var courrier_en_traitement = db.COURRIER.Find(id);
                if (courrier_en_traitement != null)
                {
                    var expediteur = db.ECHANGER.Where(e => e.Id_cour == courrier_en_traitement.Id_cour && e.isDest != true).FirstOrDefault();
                    object resultat_exp= new {id="",nom="" };
                    if (expediteur != null)
                    {
                        resultat_exp = expediteur.Id_serv != null ? new { id = expediteur.Id_serv, nom ="Service : "+ expediteur.SERVICE.Lib_serv } : new { id = expediteur.Id_cor, nom = expediteur.CORRESPONDANT.Nom_cor };
                    }
                    return Json(new
                    {
                        state = true,
                        courrier_en_traitement = new
                        {
                            courrier_en_traitement.Id_cour,
                            courrier_en_traitement.Obj_cour,
                            courrier_en_traitement.Ref_cour,
                            courrier_en_traitement.Lien_cour,
                            courrier_en_traitement.Id_nat,
                            expediteur= resultat_exp,
                            categorie =courrier_en_traitement.CATEGORIE_COURRIER?.Lib_cat_cour??"",
                            date = courrier_en_traitement.Date_cour.ToShortDateString(),
                            agent = courrier_en_traitement.UTILISATEUR.Nom_util,
                            type = courrier_en_traitement.TYPE_COURRIER?.Lib_typ_cour,
                            nom_fichier = courrier_en_traitement.nom_fichier_joint ?? "",
                            chemin = courrier_en_traitement.Lien_cour ?? "",
                            statut = courrier_en_traitement.Stat_cour,
                            nature = courrier_en_traitement.NATURE?.Lib_nat,
                            service = db.ECHANGER.Where(e => e.Id_cour == courrier_en_traitement.Id_cour && e.Id_serv != null && e.isDest==true).Select(e => new { e.Id_serv, e.SERVICE.Lib_serv }).ToList(),
                            contact = db.ECHANGER.Where(e => e.Id_cour == courrier_en_traitement.Id_cour && e.Id_cor != null && e.isDest == true).Select(e => new { e.Id_cor, e.CORRESPONDANT.Nom_cor }).ToList(),
                        }
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false, message = "Aucune donnée n'a été trouvée." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [ValidateAntiForgeryToken, HttpPost]
        [Route("action", Name = "traitement.suivi.action")]
        public JsonResult addOrModDestinataires(int id, List<int> services, List<int> contacts)
        {
            try
            {
                if (services==null && contacts == null)
                {
                    throw new Exception("Veuillez indiquer au moins un destinataire svp!!!");
                }

                var courrier = db.COURRIER.Find(id);
                if(courrier==null) throw new Exception("Courrier introuvable!!!");

                var list_destinataire = new List<ECHANGER>();
                var date = DateTime.Now;
                if (services != null)
                {
                    foreach (var item in services)
                    {
                        list_destinataire.Add(new ECHANGER() { Id_serv = item, Id_cour = courrier.Id_cour, Date_envoi = date,isDest=true });
                    }
                }
                if (contacts != null)
                {
                    foreach (var item in contacts)
                    {
                        list_destinataire.Add(new ECHANGER() { Id_cor = item, Id_cour = courrier.Id_cour, Date_envoi = date ,isDest=true});
                    }
                }
                
                var dest = db.ECHANGER.Where(e => e.Id_cour == courrier.Id_cour && e.isDest==true).ToList();
                if (dest.Count()>0)
                {
                    db.ECHANGER.RemoveRange(dest);
                    db.SaveChanges();
                }
                db.ECHANGER.AddRange(list_destinataire);
                db.SaveChanges();

                courrier.Stat_cour = StatutCourrier.Traite.Item2;

                db.SaveChanges();

                return Json(new
                {
                    state = true,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}