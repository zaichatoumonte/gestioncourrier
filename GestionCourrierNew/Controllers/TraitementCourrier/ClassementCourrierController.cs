﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using GestionCourrierNew.Helper;
using GestionCourrierNew.RoleUtilisateur;
//using GestionCourrierNew.Filter;

namespace GestionCourrierNew.Controllers.TraitementCourrier
{
    [AuthorizedRoles(Role.Traitement)]
    [RoutePrefix("traitement/classement")]
    public class ClassementCourrierController : Controller
    {
        GestionCourrierEntities db=new GestionCourrierEntities();

        // GET: ClassementCourrier
        [Route("", Name = "traitement.classement")]
        public ActionResult Index()
        {
            var categorie_courrier = db.CATEGORIE_COURRIER.OrderBy(e => e.Lib_cat_cour).ToList();
            var type_courrier = db.TYPE_COURRIER.OrderBy(e => e.Lib_typ_cour).ToList();
            var nature = db.NATURE.Where(e => e.isDelete != true).OrderBy(e => e.Lib_nat).ToList();
            var statutCourrier = new List<Tuple<int, string>>() { StatutCourrier.Traite, StatutCourrier.Non_traite, StatutCourrier.Attente };
            var agent = db.UTILISATEUR.Where(e => e.isDelete != true).OrderBy(e => e.Nom_util).ToList();

            ViewBag.type_courrier = type_courrier;
            ViewBag.statutCourrier = statutCourrier;
            ViewBag.nature = nature;
            ViewBag.agent = agent;
            ViewBag.categorie_courrier = categorie_courrier;
            return View("TraitementCourrier/ClassementCourrier/Index");
        }


        [HttpPost]
        [Route("", Name = "traitement.classement.search")]
        public JsonResult SearchCourrier(string other="",
            int? id_typ_cour=0 ,
            int? id_stat_cour=0, 
            int? id_nat=0,
            int? id_util=0,
            int? id_cat=0
            )
        {
            try
            {
                var type_depart_id = db.TYPE_COURRIER.FirstOrDefault(e => e.Lib_typ_cour.ToLower().Contains("part")).Id_typ_cour;
                var list = db.V_COURRIER.Where(e => e.isDelete != true).ToList();
                other = other.ToLower();
                var statut = id_stat_cour == StatutCourrier.Attente.Item1 ? StatutCourrier.Attente.Item2 : id_stat_cour == StatutCourrier.Traite.Item1 ? StatutCourrier.Traite.Item2 : StatutCourrier.Non_traite.Item2;
                if ( id_stat_cour != 0 )
                {
                    list = list.Where(e => e.Stat_cour == statut).ToList();
                }
                if ( id_typ_cour != 0 )
                {
                    list = list.Where(e => e.Id_typ_cour == id_typ_cour).ToList();
                    if (id_typ_cour== type_depart_id && id_cat!=0)
                    {
                        list = list.Where(e => e.Id_cat_cour == id_cat).ToList();
                    }
                }
                if ( id_util != 0 )
                {
                    list = list.Where(e => e.Id_util == id_util).ToList();
                }
                if ( id_nat != 0 )
                {
                    list = list.Where(e => e.Id_nat == id_nat).ToList();
                }
                if ( other.Trim() != string.Empty )
                {
                    list = list.Where(e =>
                        e.Obj_cour.ToLower().Contains(other) ||
                        ( e.Id_serv != null && e.Lib_serv.ToLower().Contains(other) ) ||
                        ( e.Id_cor != null && e.Nom_cor.ToLower().Contains(other) )
                   ).ToList();
                }

                var l = list.GroupBy(p => p.Id_cour,
                        (key, e) => new {
                            courrier = new
                            {
                                Id_cour = key,
                                Ref_cour_rep=e.FirstOrDefault().Rep_cour==null ?"": db.COURRIER.Find(e.FirstOrDefault().Rep_cour).Ref_cour,
                                e.FirstOrDefault().Ref_cour,
                                e.FirstOrDefault().Obj_cour,
                                e.FirstOrDefault().Date_cour,
                                e.FirstOrDefault().Lib_nat,
                                e.FirstOrDefault().Nom_util,
                                e.FirstOrDefault().Stat_cour,
                                e.FirstOrDefault().Lib_typ_cour,
                                Lib_cat_cour = e.FirstOrDefault().Lib_cat_cour ?? ""
                            },
                            expediteur = e.Where(x => x.isDest != true).Select(x => new {x.Id_cor,x.Id_serv, x.Lib_serv, x.Resp_serv, x.Civ_serv, x.Nom_cor, x.Civ_cor, x.Lib_typ_cor, Resp_cor = x.Resp_cor }).FirstOrDefault(),

                            destinataire_service = db.COURRIER.Find(key).ECHANGER.Where(dest => dest.isDest == true && dest.Id_serv!=null).Select(dest =>new { dest.Id_cor,dest.Id_serv,dest.SERVICE.Lib_serv,dest.SERVICE.Civ_serv,dest.SERVICE.Resp_serv}),

                            destinataire_contact = db.COURRIER.Find(key).ECHANGER.Where(dest => dest.isDest == true && dest.Id_cor != null).Select(dest => new { dest.Id_cor, dest.Id_serv, dest.CORRESPONDANT.Nom_cor, dest.CORRESPONDANT.Civ_cor, dest.CORRESPONDANT.Ref_cor, dest.CORRESPONDANT.Resp_cor,dest.CORRESPONDANT.TYPE_CORRESPONDANT.Lib_typ_cor }),
                        }).ToList();

                return Json(new
                {
                    state = true,
                    list = l
                });
            }
            catch (Exception e)
            {
                return Json(new { state = false ,message=e.Message});
            }
        }
    }
}