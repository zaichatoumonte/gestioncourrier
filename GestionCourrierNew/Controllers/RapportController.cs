﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using GestionCourrierNew.Filter;
using GestionCourrierNew.Models;
using GestionCourrierNew.RoleUtilisateur;

namespace GestionCourrierNew.Controllers
{
    [AuthorizedRoles(Role.Edition)]
    [RoutePrefix("Rapport")]
    public class RapportController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();

        // GET: Rapport
        [Route("Service",Name ="rapport.service")]
        public ActionResult ListeService()
        {
            var list = db.SERVICE.Where(e => e.isDelete != true).ToList();
            ViewBag.list = list;
            return View();
        }

        [Route("Correspondant", Name = "rapport.contact")]
        public ActionResult ListeContact()
        {
            var list = db.CORRESPONDANT.Where(e => e.isDelete != true).ToList();
            ViewBag.list = list;
            return View();
        }

        [Route("Nature", Name = "rapport.nature")]
        public ActionResult ListeNature()
        {
            var list = db.NATURE.Where(e => e.isDelete != true).ToList();
            ViewBag.list = list;
            return View();
        }

        [Route("Utilisateur", Name = "rapport.utilisateur")]
        public ActionResult ListeUtilisateur()
        {
            var list = db.UTILISATEUR.Where(e => e.isDelete != true).ToList();
            ViewBag.list = list;
            return View();
        }

        [Route("Courriers/Reponse", Name = "rapport.reponse")]
        public ActionResult ListeReponse()
        {
            ViewBag.type = "reponse";
            return View("Courriers");
        }

        
        [Route("Depart/Courriers", Name = "rapport.depart")]
        public ActionResult ListeCourrierDepart()
        {
            ViewBag.type = "depart";
            return View("Courriers");
        }

        [Route("Arrive/Courriers", Name = "rapport.arrive")]
        public ActionResult ListeCourrierArrive()
        {
            ViewBag.type = "arrive";
            return View("Courriers");
        }
    }
}