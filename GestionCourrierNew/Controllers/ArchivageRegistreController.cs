﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using GestionCourrierNew.RoleUtilisateur;
using GestionCourrierNew.Helper;

namespace GestionCourrierNew.Controllers
{
    [RoutePrefix("Archivage")]
    [AuthorizedRoles(Role.Archive)]
    public class ArchivageRegistreController : Controller
    {
        // GET: ArchivageRegistre
        GestionCourrierEntities db = new GestionCourrierEntities();

        [Route("",Name ="archives.index")]
        public ActionResult Index()
        {
            var courrier = db.COURRIER.Where(e => e.isDelete != true).ToList();
            ViewBag.list = courrier;
            return View();
        }

        [Route("Archiver",Name ="archives.archiver")]
        [HttpGet]
        public JsonResult ArchiverCourrier()
        {
            try
            {
                var date = DateTime.Now;
                
                var archives=(
                    from e in db.COURRIER.Where(e => e.isDelete != true).ToList()
                     let expediteur = e.ECHANGER.Where(x => x.isDest != true).FirstOrDefault()
                     let destinataire_serv = e.ECHANGER.Where(x => x.isDest == true && x.SERVICE!=null).Select(x=> x.SERVICE.Ref_serv).ToArray()
                    let destinataire_cont = e.ECHANGER.Where(x => x.isDest == true && x.CORRESPONDANT != null).Select(x => x.CORRESPONDANT.Ref_cor).ToArray()
                    let chaine=string.Join(" ",destinataire_serv)
                    let chaine2= string.Join(" ",destinataire_cont)
                    select new ARCHIVAGE() {
                         Annee_arch = date,
                         Obj_cour_arch = e.Obj_cour,
                         Cont_cour_arch = e.Cont_cour,
                         Date_cour_arch = e.Date_cour,
                         Ref_cour_arch = e.Ref_cour,
                         Ref_rep_cour_arch = db.COURRIER.Find(e.Rep_cour)?.Ref_cour,
                         Stat_cour_arch = e.Stat_cour,
                         Nom_util_arch = e.UTILISATEUR.Nom_util,
                         Lib_cat_cour_arch = e.CATEGORIE_COURRIER?.Lib_cat_cour,
                         Lib_typ_cour_arch = e.TYPE_COURRIER.Lib_typ_cour,
                         Lib_nat_arch = e.NATURE.Lib_nat,
                         Lien_cour_arch = e.Lien_cour,
                         Nom_fichier_joint_arch = e.nom_fichier_joint,
                         Ref_exp_arch=expediteur==null?null:expediteur.Id_cor!=null?expediteur.CORRESPONDANT.Ref_cor:expediteur.SERVICE.Ref_serv,
                         Destinataires_arch=(chaine==""&& chaine2=="")?null:(chaine+" "+chaine2).Trim()
                     }).ToList();
                db.ARCHIVAGE.AddRange(archives);
                db.ECHANGER.RemoveRange(db.ECHANGER.ToList());
                db.COURRIER.RemoveRange(db.COURRIER.ToList());
                db.SaveChanges();
                return Json(new { state = true, archives }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false,message=e.Message }, JsonRequestBehavior.AllowGet);

            }
        }

        [Route("Consulter", Name = "archives.consulter")]
        public ActionResult Consultation()
        {
            var categorie_courrier = db.CATEGORIE_COURRIER.OrderBy(e => e.Lib_cat_cour).ToList();
            var type_courrier = db.TYPE_COURRIER.OrderBy(e => e.Lib_typ_cour).ToList();
            var nature = db.NATURE.Where(e => e.isDelete != true).OrderBy(e => e.Lib_nat).ToList();
            var statutCourrier = new List<Tuple<int, string>>() { StatutCourrier.Traite, StatutCourrier.Non_traite, StatutCourrier.Attente };
            var agent = db.UTILISATEUR.Where(e => e.isActif_util == true).OrderBy(e => e.Nom_util).ToList();

            ViewBag.type_courrier = type_courrier;
            ViewBag.statutCourrier = statutCourrier;
            ViewBag.nature = nature;
            ViewBag.agent = agent;
            ViewBag.categorie_courrier = categorie_courrier;
            return View();
        }

        [Route("Consulter/GetData", Name = "archives.consulter.data")]
        [HttpPost]
        public JsonResult Data(string other,string typ_cour,string stat_cour,string Lib_nat,string Nom_util,string Lib_cat,int? annee)
        {
            try
            {
                var list = db.ARCHIVAGE.ToList();
                other = other.ToLower();
                if (annee != 0)
                {
                    list = list.Where(e => e.Annee_arch.Year == annee).ToList();
                }
                if (stat_cour != "")
                {
                    list = list.Where(e => e.Stat_cour_arch.ToLower() == stat_cour.ToLower()).ToList();
                }
                if (typ_cour != "")
                {
                    list = list.Where(e => e.Lib_typ_cour_arch.ToLower() == typ_cour.ToLower()).ToList();
                }
                if (Nom_util != "")
                {
                    list = list.Where(e => e.Nom_util_arch.ToLower() == Nom_util.ToLower()).ToList();
                }
                if (Lib_nat != "")
                {
                    list = list.Where(e => e.Lib_nat_arch.ToLower() == Lib_nat.ToLower()).ToList();
                }
                if (Lib_cat != "")
                {
                    list = list.Where(e => e.Lib_cat_cour_arch?.ToLower() == Lib_cat.ToLower()).ToList();
                }

                var list_arch = list.Select(e => new {
                    Ref_cour=e.Ref_cour_arch,
                    Obj_cour=e.Obj_cour_arch,
                    Stat_cour=e.Stat_cour_arch,
                    Nom_util=e.Nom_util_arch,
                    Lib_nat=e.Lib_nat_arch,
                    Lib_typ_cour=e.Lib_typ_cour_arch,
                    e.Ref_rep_cour_arch,
                    e.Id_arch,
                    expediteur= GetEchangeIntervenantArchive(e.Ref_exp_arch),
                    destinataire=GetListEchangeIntervenantArchive(e.Destinataires_arch)
                }).ToList();
                if (other != "")
                {
                    list_arch = list_arch.Where(
                         e => e.Obj_cour.ToLower().Contains(other)==true ||
                         (e.expediteur!=null &&  e.expediteur.Nom.ToLower().Contains(other))||
                         (e.destinataire!=null && e.destinataire.Item1!=null && e.destinataire.Item1.Where(x=>x.Nom.ToLower().Contains(other)==true).Count()>0)||
                         (e.destinataire!=null && e.destinataire.Item2!=null && e.destinataire.Item2.Where(x => x.Nom.ToLower().Contains(other) == true).Count() > 0)
                    ).ToList();
                }
                return Json(new { state = true,list_arch});
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message });
            }
        }

        [Route("Consulter/detail-archive", Name = "archives.consulter.detail")]
        [HttpGet]
        public JsonResult Detail_archive(int id_arch)
        {
            try
            {
                var archive=db.ARCHIVAGE.Find(id_arch);
                if (archive != null)
                {
                    var ref_arr = db.ARCHIVAGE.Where(e => e.Ref_rep_cour_arch == archive.Ref_cour_arch).FirstOrDefault();
                    return Json(new { state = true, archive =
                        new {
                            archive.Annee_arch.Year,
                            //archive.Cont_cour_arch,
                            date = archive.Date_cour_arch.ToShortDateString(),
                            Lib_cat_cour = archive.Lib_cat_cour_arch ?? "Non Defini",
                            Lib_nat=archive.Lib_nat_arch,
                            Lib_typ_cour=archive.Lib_typ_cour_arch,
                            Lien_cour=archive.Lien_cour_arch??"#",
                            Nom_fichier_joint=archive.Nom_fichier_joint_arch??"Aucun",
                            Nom_util=archive.Nom_util_arch,
                            Obj_cour=archive.Obj_cour_arch,
                            Ref_cour=archive.Ref_cour_arch,
                            Ref_rep=archive.Ref_rep_cour_arch??"Aucun",
                            Stat_cour=archive.Stat_cour_arch,
                            expediteur=GetEchangeIntervenantArchive(archive.Ref_exp_arch),
                            destinataire= GetListEchangeIntervenantArchive(archive.Destinataires_arch),
                            Ref_arr=ref_arr!=null?ref_arr.Ref_cour_arch:""
                        }
                    },JsonRequestBehavior.AllowGet);
                }
                
                return Json(new { state = false, message="archive introuvable" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public static Contact GetEchangeIntervenantArchive(string info_ref)
        {
            var db = new GestionCourrierEntities();
          
            var destOrserv = new Contact();
            if (string.IsNullOrWhiteSpace(info_ref))
            {
                return null;
            }
            if (info_ref.IndexOf("SERV") != -1)
            {
                destOrserv = db.SERVICE.Where(e => e.Ref_serv == info_ref).Select(e => new Contact {Resp= e.Resp_serv, Civ=e.Civ_serv, Nom=e.Lib_serv }).FirstOrDefault();
            }
            else if (info_ref.IndexOf("COR") != -1)
            {
                destOrserv = db.CORRESPONDANT.Where(e => e.Ref_cor == info_ref).Select(e => new Contact { Resp=e.Resp_cor,Civ= e.Civ_cor, Nom=e.Nom_cor, Type=e.TYPE_CORRESPONDANT.Lib_typ_cor }).FirstOrDefault();
            }
            return destOrserv;
        }
        public static Tuple<List<Contact>,List<Contact>> GetListEchangeIntervenantArchive(string info_ref)
        {
            //var db = new GestionCourrierEntities();
            if (info_ref == null) { return null; };
            var list_dest = info_ref.Split(' ').ToList();
            List<Contact> services = new List<Contact>();
            List<Contact> contacts = new List<Contact>();
            foreach (var item in list_dest)
            {
                if (item.IndexOf("SERV") != -1)
                {
                    services.Add(GetEchangeIntervenantArchive(item));
                }
                else if (item.IndexOf("COR") != -1)
                {
                    contacts.Add(GetEchangeIntervenantArchive(item));
                }
            }
            return new Tuple<List<Contact>, List<Contact>>( services,contacts);
        }

    }
}