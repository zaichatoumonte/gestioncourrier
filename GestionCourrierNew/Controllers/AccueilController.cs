﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using GestionCourrierNew.Helper;
using System.Data.Entity;
using System.Web.Security;

namespace GestionCourrierNew.Controllers
{
    [AllowAnonymous]
    public class AccueilController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();
        // GET: Accueil

        //[Authorize]
        //public ActionResult Index()
        //{
        //    //List<int?> listr = new List<int?>();
        //    //listr = db.COURRIER.Where(e => e.isDelete != true && e.Stat_cour == StatutCourrier.Attente.Item2)
        //    //    .Select(e => DbFunctions.DiffDays(e.Date_cour, DateTime.Now)).ToList();
        //    var list = (from courrier in db.COURRIER
        //                let dif = DbFunctions.DiffDays(courrier.Date_cour,DateTime.Now) 
                        
        //                where dif >= 7 && courrier.isDelete != true && courrier.Stat_cour == StatutCourrier.Attente.Item2
        //                select courrier).ToList();
        //    //var list = db.COURRIER.Where(e => e.isDelete != true && e.Stat_cour == StatutCourrier.Attente.Item2 && ((DateTime.Now.Date - e.Date_cour.Date).TotalDays>=7)).ToList();
        //    ViewBag.list = list;
        //    return View();
        //}

        public ActionResult Accueil()
        {
            try
            {
                if (db.UTILISATEUR.Count() == 0)
                {
                    return View("FirstRegistration");
                }
				ViewData["accueil"] = true;
                return View();
            }
            catch (Exception e)
            {
                return RedirectToActionPermanent("Index","Error",new {message= e.Message});
            }
        }

        [HttpPost,ValidateAntiForgeryToken]
        [Route("Accueil/Initialisation/Creation-Administrateur",Name = "Accueil.InitialisationApplication")]
        public ActionResult CreateFirstAppUserAsAdministrator(UTILISATEUR user, string password_confirm)
        {
            try
            {
                //ce test est fait au cas ou l'utilisateur tente par lui meme de lancer cette page afin de creer un compte damin frauduleusement
                if (db.UTILISATEUR.Count() == 0)
                {
                    string error = "";
                    if (string.IsNullOrWhiteSpace(user.Nom_util))
                    {
                        error = "Veuillez indiquer le nom de l'utilisateur svp!!!";
                    }
                    else if (string.IsNullOrWhiteSpace(user.Login_util))
                    {
                        error = "Veuillez indiquer le login de l'utilisateur svp!!!";
                    }
                    else if (string.IsNullOrWhiteSpace(user.Email_util))
                    {
                        error = "Veuillez indiquer le mail de l'utilisateur svp!!!";
                    }
                    else if (string.IsNullOrWhiteSpace(user.PasswdHash_util))
                    {
                        error = "Veuillez indiquer le mot de passe de l'utilisateur svp!!!";
                    }
                    else if (user.PasswdHash_util != password_confirm)
                    {
                        error = "Les mots de passe doivent être identique!!!";
                    }

                    if (error != "")
                    {
                        ViewData["error"] = error;
                        ViewData["nom"] = user.Nom_util;
                        ViewData["email"] = user.Email_util;
                        ViewData["login"] = user.Login_util;
                        ViewData["mdp"] = user.PasswdHash_util;
                        ViewData["cmdp"] = password_confirm.ToString();
                        return View("FirstRegistration");
                    }
                    
                    //en cas d'erreur au moment de l'ajout de l'administrateur, il ne faudrait pas qu'on recree toutes les valeurs obligatoires
                    if (db.MODULE.Count() == 0)
                    {
                        //Initialisation de la bd (creation des informations primordiales)
                        var moduleList = new List<MODULE>() {
                            new MODULE() {Lib_mod="Reception de courrier",Role_cs=RoleUtilisateur.Role.Receive.ToString() },
                            new MODULE() {Lib_mod="Traitement des courriers",Role_cs=RoleUtilisateur.Role.Traitement.ToString() },
                            new MODULE() {Lib_mod="Archivage",Role_cs=RoleUtilisateur.Role.Archive.ToString() },
                            new MODULE() {Lib_mod="Parametrages",Role_cs=RoleUtilisateur.Role.Configuration.ToString() },
                            new MODULE() {Lib_mod="Edition des rapports",Role_cs=RoleUtilisateur.Role.Edition.ToString()},
						};
                        db.MODULE.AddRange(moduleList);
                        var adminRole = new DROIT_ACCES()
                        {
                            Lib_drt = "Administrateur"
                        };
                        db.DROIT_ACCES.Add(adminRole);
                        db.PARAMETRAGE.Add(new PARAMETRAGE() { Nom_directeur = "Edouard Fonh Gbei", Num_cour_ad = 1, Num_cour_st = 1 });
                        db.CATEGORIE_COURRIER.AddRange(new List<CATEGORIE_COURRIER>() {
                            new CATEGORIE_COURRIER() {Lib_cat_cour="Soit Transmis"},
                            new CATEGORIE_COURRIER() {Lib_cat_cour="Courrier Administratif"},
                        });
                        db.TYPE_COURRIER.AddRange(new List<TYPE_COURRIER>() {
                            new TYPE_COURRIER() {Lib_typ_cour="Courrier Départ"},
                            new TYPE_COURRIER() {Lib_typ_cour="Courrier Arrivé"},
                        });
                        db.SaveChanges();
                        foreach (var item in moduleList)
                        {
                            db.ACCEDER.Add(new ACCEDER() { Id_drt = adminRole.Id_drt, Id_mod = item.Id_mod });
                        }
                        db.SaveChanges();
                    }
                    string user_password = user.PasswdHash_util;
                    user.Id_drt = db.DROIT_ACCES.FirstOrDefault(e=>e.Lib_drt== "Administrateur").Id_drt;
                    user.isDelete = false;
                    user.isActif_util = true;
                    user.IsUpdate = true;
                    user.PasswdHash_util = CodeHelper.GetMd5Hash(user.PasswdHash_util);
                    user.Date_creation_util = DateTime.Now;
                    db.UTILISATEUR.Add(user);
                    db.SaveChanges();
					AuthentificationController.CreateAuthentificationCookie(Response, user.Nom_util, user.Id_util, user.Date_creation_util);
                    //FormsAuthentication.SetAuthCookie(user.Nom_util, false);
                    return RedirectToActionPermanent("Accueil");
                }
                else
                {
                    return RedirectToActionPermanent("Index", "Error", new {code="404"});
                }
            }
            catch (Exception e)
            {
                
                return RedirectToActionPermanent("Index", "Error", new { message = e.Message});
            }
        }
    }
}