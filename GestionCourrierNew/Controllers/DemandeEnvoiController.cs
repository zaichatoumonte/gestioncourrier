﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using GestionCourrierNew.RoleUtilisateur;

namespace GestionCourrierNew.Controllers
{
	[AuthorizedRoles(Role.Demande)]
	[RoutePrefix("Reception/DemandeCourrier")]
	public class DemandeEnvoiController : Controller
    {
		GestionCourrierEntities db = new GestionCourrierEntities();
		int id_courrier_depart = 0;

		// GET: DemandeEnvoi
		[Route("", Name = "demande.index")]
		public ActionResult index()
		{
			try
			{
				//affichage de tous les courriers emis par l'utilisateur authentifier
				ViewBag.Title = "Demande de validation de Courrier Départs";
				var active_user = AuthentificationController.GetUserAuthentificateData(Request).Nom_util;
				id_courrier_depart = db.TYPE_COURRIER.FirstOrDefault(e => e.Lib_typ_cour.ToLower().Contains("part"))?.Id_typ_cour ?? 0;
				var list_id_rep = db.COURRIER.Where(e => e.Rep_cour != null).Select(e => e.Rep_cour).ToList();
				var list = db.COURRIER.Where(e => e.isDelete != true && e.Id_typ_cour == id_courrier_depart && list_id_rep.Contains(e.Id_cour) == false /*&& e.Stat_cour==Helper.StatutCourrier.Attente.Item2*/ && e.UTILISATEUR.Nom_util==active_user).ToList();
				var nature = db.NATURE.Where(e => e.isDelete != true).ToList();
				var service = db.SERVICE.Where(e => e.isDelete != true).ToList();
				var contact = db.CORRESPONDANT.Where(e => e.isDelete != true).GroupBy(e => e.TYPE_CORRESPONDANT).ToList();

				ViewBag.courriers = list;
				ViewBag.nature = nature;
				ViewBag.services = service;
				ViewBag.contacts = contact;

				return View();
			}
			catch (Exception e)
			{
                return RedirectToActionPermanent("Index", "Error", new { message = e.Message});
			}
		}


		[HttpGet]
        [Route("delete/{id:int}", Name = "demande.delete")]
        public JsonResult delete(int id)
        {
            try
            {
                var courrier = db.COURRIER.Find(id);
				//la suppression est possible si l'utilisateur en cours est le createur de ce courrier
				//si celui ci est en attente de traitement
				//et s'il s agit d un ciurrier depart
				id_courrier_depart = db.TYPE_COURRIER.FirstOrDefault(e => e.Lib_typ_cour.ToLower().Contains("part"))?.Id_typ_cour ?? 0;
				if (courrier != null && courrier.Stat_cour==Helper.StatutCourrier.Attente.Item2 && courrier.Id_typ_cour==id_courrier_depart && courrier.UTILISATEUR.Nom_util==AuthentificationController.GetUserAuthentificateData(this.Request).Nom_util)
                {
                    courrier.isDelete = true;
                    courrier.dateDelete = DateTime.Now;
                    db.SaveChanges();
                    return Json(new { state = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { state = false,message="Ce courrier n'existe pas ou vous n'avez pas l'autorisation pour le supprimer!!!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}