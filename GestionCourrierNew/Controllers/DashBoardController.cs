﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using GestionCourrierNew.Helper;
using System.Data.Entity;
//using GestionCourrierNew.RoleUtilisateur;
//using GestionCourrierNew.Filter;

namespace GestionCourrierNew.Controllers
{
    //[_AuthentificationFilter]
    //[AuthorizedRoles(Role.Dashbord)]
    [Authorize]
    [RoutePrefix("TableauDeBord")]
    public class DashBoardController : Controller
    {
        GestionCourrierEntities db = new GestionCourrierEntities();
        // GET: DashBoard
        [Route("Accueil",Name ="dashboad.accueil")]
        public ActionResult Index()
        {
			//decompte des courrier par statut enregistrerau cours de cette annee et aussi du nombre d'utilisateur de la plateforme
            var traite = db.COURRIER.Where(e => e.isDelete!=true && e.Stat_cour == StatutCourrier.Traite.Item2 && e.Date_cour.Year == DateTime.Now.Year).Count();
            var attente = db.COURRIER.Where(e => e.isDelete != true && e.Stat_cour == StatutCourrier.Attente.Item2 && e.Date_cour.Year == DateTime.Now.Year).Count();
            var non_traite = db.COURRIER.Where(e => e.isDelete != true && e.Stat_cour == StatutCourrier.Non_traite.Item2 && e.Date_cour.Year == DateTime.Now.Year).Count();
            var utilisateur = db.UTILISATEUR.Where(e=>e.isDelete!=true).Count();
            ViewBag.nb_traite = traite;
            ViewBag.nb_non_traite = non_traite;
            ViewBag.nb_utilisateur = utilisateur;
            ViewBag.nb_attente = attente;
            return View();
        }

        [HttpGet]
        [Route("Data", Name = "dashboad.data")]
        public JsonResult Statistique()
        {
            try
            {
                /*
                //list de tout les courriers non supprimés de l'annee en cours quelque soit leur statut
                var list = db.COURRIER
                    .Where(e => e.isDelete != true && e.Date_cour.Year == DateTime.Now.Year).ToList()
                    //on regroupe ces courrier par mois de creation
                    .Select(e => new { date = string.Format("{0:MM}", e.Date_cour), e.Stat_cour, e.Id_cour })
                    .GroupBy(e => e.date)
                    .Select(e => new {
                        mois = e.Key,
                        data = e.GroupBy(x => x.Stat_cour).Select(x => new { statut = x.Key, nombre = x.Count() }) })
                    .OrderBy(e => e.mois).ToList();

                //liste des courriers non supprime de l'anne en cours qui ont été traité
                var list_traite = db.COURRIER
                    .Where(e => e.isDelete != true && e.Date_cour.Year == DateTime.Now.Year && e.Stat_cour == StatutCourrier.Traite.Item2).ToList()
                    .Select(e => new { date = string.Format("{0:MM}", e.Date_cour), Lib_typ_cour=e.TYPE_COURRIER.Lib_typ_cour.ToLower(), e.Id_cour,e.Rep_cour }).ToList();

                var list_arrive = list_traite.Where(e => e.Lib_typ_cour.Contains("arr") == true)
                     .GroupBy(e => e.date)
                     .Select(e => new
                     {
                         mois = e.Key,
                         data=e.Count()
                     }).ToList();

                //on recupere la liste des reponses aux courriers
                var list_id_rep = db.COURRIER.Where(e => e.isDelete != true & e.Rep_cour != null).Select(e => e.Rep_cour).ToList();
                
                //on recupere tout les courrier traité dont l'id fait partie de la liste
                var list_rep = list_traite.Where(e=>list_id_rep.Contains(e.Id_cour)).ToList()
                     .GroupBy(e => e.date)
                     .Select(e => new
                     {
                         mois = e.Key,
                         data = e.Count()
                     }).ToList();
                //on recupere tout les courrier depart traité dont l'id  ne fait pas partie de la liste
                var list_dep = list_traite.Where(e => e.Lib_typ_cour.Contains("part")==true && list_id_rep.Contains(e.Id_cour)==false).ToList()
                     .GroupBy(e => e.date)
                     .Select(e => new
                     {
                         mois = e.Key,
                         data = e.Count()
                     }).ToList();

                var attente=db.COURRIER.Where(e=>e.Stat_cour==StatutCourrier.Attente.Item2 && e.isDelete!=true && e.Date_cour.Year == DateTime.Now.Year).ToList()
                        .Select(e => new { date = string.Format("{0:MM}", e.Date_cour), e.TYPE_COURRIER.Lib_typ_cour, e.Id_cour })
                        .GroupBy(e=>e.date)
                        .Select(e => new {
                            mois = e.Key,
                            data = e.GroupBy(x => x.Lib_typ_cour).Select(x => new { statut = x.Key, nombre = x.Count() })
                        })
                        .OrderBy(e => e.mois).ToList();
*/
                /*debut d'un autre essai*/

                var all_month = new string[] {"", "Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre" };

                var initiale_list = db.COURRIER.Where(e => e.isDelete != true && e.Date_cour.Year == DateTime.Now.Year).ToList()
                        .Select(e => new { e.Date_cour.Month, Lib_typ_cour=e.TYPE_COURRIER.Lib_typ_cour.ToLower(), e.Id_cour, e.Stat_cour });

                var list_reponse_id= db.COURRIER.Where(e => e.isDelete != true & e.Rep_cour != null && e.Date_cour.Year == DateTime.Now.Year).Select(e => e.Rep_cour).ToList();
                
                //index du mois limite (dernier mois a representer sur le graphe
                var now_month = DateTime.Now.Month;

                //les axes des differents graphes
                var month_categorie_graph = new List<string>();

                var graph_tout_statut_series = new string[] {
                    StatutCourrier.Non_traite.Item2,
                    StatutCourrier.Attente.Item2,
                    StatutCourrier.Traite.Item2
                };
                var graph_detail_traite_series = new string[] {"Courrier Arrivé", "Courrier Départ", "Réponse" };
                var graph_detail_attente_series = new string[] {"Courrier Arrivé", "Courrier Départ" };

                var data_list_statut = new List<List<int>>() { new List<int>(), new List<int>(), new List<int>() };
                var data_list_traite = new List<List<int>>() { new List<int>(), new List<int>(), new List<int>() };
                var data_list_attente = new List<List<int>>() { new List<int>(), new List<int>() };

                for ( int i = 1; i <= now_month; i++ )
                {
                    month_categorie_graph.Add(all_month[i]);
                    //toute les donne du mois i
                    var list_month = initiale_list.Where(e => e.Month == i).ToList();

                    //decompte des données par statut de ce mois
                    data_list_statut[0].Add(list_month.Where(e => e.Stat_cour == graph_tout_statut_series[0]).Count());
                    data_list_statut[1].Add(list_month.Where(e => e.Stat_cour == graph_tout_statut_series[1]).Count());
                    data_list_statut[2].Add(list_month.Where(e => e.Stat_cour == graph_tout_statut_series[2]).Count());

                    //decompte des données pour les courriers traite de ce mois
                    data_list_traite[0].Add(list_month.Where(e => e.Stat_cour == graph_tout_statut_series[2] && e.Lib_typ_cour.Contains("arr")).Count());
                    data_list_traite[1].Add(list_month.Where(e => e.Stat_cour == graph_tout_statut_series[2] && e.Lib_typ_cour.Contains("part") && !list_reponse_id.Contains(e.Id_cour)).Count());
                    data_list_traite[2].Add(list_month.Where(e => e.Stat_cour == graph_tout_statut_series[2] && e.Lib_typ_cour.Contains("part") && list_reponse_id.Contains(e.Id_cour)).Count());

                    //decompte des données pour les courriers en attente de traitement de ce mois
                    data_list_attente[0].Add(list_month.Where(e => e.Stat_cour == graph_tout_statut_series[1] && e.Lib_typ_cour.Contains("arr")).Count());
                    data_list_attente[1].Add(list_month.Where(e => e.Stat_cour == graph_tout_statut_series[1] && e.Lib_typ_cour.Contains("part")).Count());
                }

                var final_graph_data = new
                {
                    graphe_tout_statut = new
                    {
                        series = graph_tout_statut_series,
                        nontraite = data_list_statut[0].ToArray(),
                        attente = data_list_statut[1].ToArray(),
                        traite = data_list_statut[2].ToArray(),
                    },
                    graphe_statut_traite = new
                    {
                        series = graph_detail_traite_series,
                        arrive_traite = data_list_traite[0].ToArray(),
                        depart_traite = data_list_traite[1].ToArray(),
                        reponse_traite = data_list_traite[2].ToArray(),
                    },
                    graphe_statut_attente = new
                    {
                        series = graph_detail_attente_series,
                        arrive_attente = data_list_attente[0].ToArray(),
                        depart_attente = data_list_attente[1].ToArray(),
                    },
                    categories=month_categorie_graph
                };

                return Json(new { state = true,
                    final_graph_data
                    /*
                    tout_courrier =list,
                    traite_courrier =new{ list_dep, list_rep, list_arrive }
                    */
                },JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false ,e.Message}, JsonRequestBehavior.AllowGet);
            }
        }
    }
}