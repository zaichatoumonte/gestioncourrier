﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using GestionCourrierNew.RoleUtilisateur;

namespace GestionCourrierNew.Controllers
{
    [Authorize]
    [AuthorizedRoles(Role.Configuration,Role.Receive,Role.Traitement,Role.Demande)]
    [RoutePrefix("Parametrage/Correspondant")]
    public class AddContactController : Controller
    {
        // GET: AddContact
        GestionCourrierEntities db = new GestionCourrierEntities();

        [ValidateAntiForgeryToken, HttpPost]
        [Route("action", Name = "parametrage.contact.action")]
        public JsonResult actionCreateAdd(CORRESPONDANT contact)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(contact.Nom_cor)) throw new Exception("Veuillez indiquer le nom du correspondant svp!!!");
                if (string.IsNullOrWhiteSpace(contact.Civ_cor)) throw new Exception("Veuillez indiquer la civilité du correspondant/responsable svp!!!");
                if (string.IsNullOrWhiteSpace(contact.Tel_cor)) throw new Exception("Veuillez indiquer le numero du correspondant svp!!!");
                if (contact.Id_typ_cor == 0) throw new Exception("Veuillez indiquer le type de correspondant svp!!!");
                var exist = db.CORRESPONDANT.Where(e => e.Nom_cor.ToLower() == contact.Nom_cor.ToLower() && e.isDelete != true).FirstOrDefault();
                if (exist != null && contact.Id_cor != exist.Id_cor)
                {
                    throw new Exception("Ce correspondant existe déja");
                }
                if (contact.Id_cor == 0)
                {
                    contact.Ref_cor = "";
                    contact.isDelete = false;
                    db.CORRESPONDANT.Add(contact);
                }
                else
                {
                    var mod = db.CORRESPONDANT.Find(contact.Id_cor);
                    mod.Nom_cor = contact.Nom_cor;
                    mod.Mail_cor = contact.Mail_cor;
                    mod.Ad_cor = contact.Ad_cor;
                    mod.Tel_cor = contact.Tel_cor;
                    mod.Id_typ_cor = contact.Id_typ_cor;
                    mod.Civ_cor = contact.Civ_cor;
                    mod.Resp_cor = contact.Resp_cor;
                }
                db.SaveChanges();
                return Json(new
                {
                    state = true,
                    contact = new
                    {
                        contact.Id_cor,
                        contact.Nom_cor,
                        contact.Ad_cor,
                        contact.Mail_cor,
                        contact.Resp_cor,
                        contact.Tel_cor,
                        db.TYPE_CORRESPONDANT.Find(contact.Id_typ_cor)?.Lib_typ_cor,
                        contact.Civ_cor,
                    }
                }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException e)
            {
                var exception = SharedController.ErrorPrinter(e.EntityValidationErrors);
                var listException = exception.Item1;
                var message = exception.Item2;
                return Json(new { state = false, message, listException = listException }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException)
            {
                return Json(new { state = false, message = "Les informations que vous avez entrées ne sont pas valident." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}