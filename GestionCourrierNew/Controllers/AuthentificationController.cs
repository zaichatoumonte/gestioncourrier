﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Helper;
using GestionCourrierNew.Models;
using System.Web.Security;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;

namespace GestionCourrierNew.Controllers
{
    /// <summary>
    /// ce controleur permet au utilisateur de se connecter pour avoir acces a la plateforme 
    /// L'utilisateur pour pouvoir se connecter doit avoir un compte actif
    /// Quand l'utilisateur se connecte pour la premiere fois apres que l'administrateur ai ajouté son compte, il doit changer son login et son mot de passe
    /// </summary>
    
    [AllowAnonymous]
    [RoutePrefix("Authentification")]
    public class AuthentificationController : Controller
    {
        // GET: Authentification
        GestionCourrierEntities db = new GestionCourrierEntities();

        [Route("Connexion",Name ="authentification.login")]
        public ActionResult Login()
        {
            return View();
        }

        //traitement de la demande d'authentification
        [Route("Verification",Name = "authentification.checkIdentifier")]
        [ValidateAntiForgeryToken,HttpPost]
        public JsonResult VerifyConnexion(string login,string pass)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(login) || string.IsNullOrWhiteSpace(pass)) throw new Exception("Veuillez renseigner tous les champs svp!!!");

                var list = db.UTILISATEUR.ToList();

                bool is_username_already = list.Exists(e => e.Login_util == login.Trim());
                if (is_username_already == false) throw new Exception("Les informations que vous avez entrées ne sont pas valident."); /*return Json(new {state=false, message = "login" });*/

                var passhash = CodeHelper.GetMd5Hash(pass.Trim());
                bool is_pass_already = list.Exists(e => e.Login_util == login.Trim() && e.PasswdHash_util == passhash);
                if (is_pass_already == false) throw new Exception("Verifier vos informations de connexion svp!!!");/* return Json(new { state = false, message = "mot" });*/

                StringComparer comparer = StringComparer.OrdinalIgnoreCase;
                var users=list.Where(e => comparer.Compare(e.Login_util, login) == 0 && comparer.Compare(e.PasswdHash_util, passhash) == 0).ToList();
                UTILISATEUR user=new UTILISATEUR();
                if (users.Count() == 1)
                {
                    user = users[0];
                }
                else
                {
                    foreach (var item in list)
                    {
                        if (users.Exists(e => e.Login_util == login.Trim() && e.PasswdHash_util == passhash))
                        {
                            user= item;
                            break;
                        }
                    }
                }
                if (user.isActif_util == false) return Json(new { state = false, message = "Désolé, votre compte est inatif!!!" });
                if (user.IsUpdate == true)
                {
                    //FormsAuthentication.SetAuthCookie(user.Nom_util, false);
                    var IsCookieCreate = CreateAuthentificationCookie(Response, user.Nom_util, user.Id_util, user.Date_creation_util);
                    if (IsCookieCreate) return Json(new { state = true, url = "/Accueil" });
                    else throw new Exception("Erreur lors de la création de la session!!")/*Json(new { state = false, message = "Erreur lors de la création de la session!!" })*/;
                    
                }
                else
                {
                    return Json(new { state = true, login = user.Login_util, id = user.Id_util });
                }
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message });
            }
        }

        //premiere connexion de l'utilisateur à son compte apres la creation de celui-ci
        //il faut qu'il reinitialise son login et son mot de passe
        [Route("Reinitialisation",Name = "authentification.resetIdentifier")]
        [ValidateAntiForgeryToken, HttpPost]
        public JsonResult ChangeConnexionParameter(string login_new, string pass_new,int id_user)
        {
            try
            {
                /*
                 Les champs login_util et PasswdHash_util sont uniques dans pas la base de donnee
                 la comparaison ne tient pas compte de la casse dans la base de donnee
                 on s'assure que lorsque l'utilisateur reinitialise son compte, qu'il change entierement le login pas la casse
                 */
                if (string.IsNullOrWhiteSpace(login_new) || string.IsNullOrWhiteSpace(pass_new)) return Json(new { state = false, message = "Veuillez renseigner tous les champs svp!!!" });

                var user = db.UTILISATEUR.Find(id_user);
                if (user == null) return Json(new { state = false, message = "Désolé, nous ne retrouvons pas vos données!!!" });
                if (user.IsUpdate == true) return Json(new { state = false, message = "Ce compte est déja réinitialiser." });

                var passhash = CodeHelper.GetMd5Hash(pass_new.Trim());
                var login_exist = db.UTILISATEUR.FirstOrDefault(e => e.Login_util.ToLower() == login_new.ToLower());
                if(user.Login_util.ToLower()==login_new.ToLower() || login_exist!=null) return Json(new { state = false, message = "Changez votre login svp!!!" });
                if (user.PasswdHash_util == passhash) return Json(new { state = false, message = "Changez votre mot de passe svp!!!" });
                user.Login_util =login_new;
                user.PasswdHash_util = passhash;
                user.IsUpdate = true;
                db.SaveChanges();
                return VerifyConnexion(login_new,pass_new);
               // return Json(new { state = true, url = "/Accueil" });
            }
            catch (DbEntityValidationException e)
            {
                var exception = SharedController.ErrorPrinter(e.EntityValidationErrors);
                var listException = exception.Item1;
                var message = exception.Item2;
                return Json(new { state = false, message, listException = listException }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException)
            {
                return Json(new { state = false, message = "Les informations que vous avez entrées ne sont pas valident." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //changement du login et du mot de passe par l'utilisateur
        [Authorize]
        [Route("Modification",Name = "authentification.changeIdentifier")]
        [HttpPost, ValidateAntiForgeryToken]
        public JsonResult UpdateConnexionParameter(string login_new, string pass_new, string pass_last/*, int id_user*/)
        {
            try
            {
                /*
                 Les champs login_util et PasswdHash_util sont uniques dans pas la base de donnee
                 la comparaison ne tient pas compte de la casse dans la base de donnee
                 on s'assure que lorsque l'utilisateur reinitialise son compte, qu'il change entierement le login pas la casse
                 */
                if (string.IsNullOrWhiteSpace(login_new) || string.IsNullOrWhiteSpace(pass_new) || string.IsNullOrWhiteSpace(pass_last)) return Json(new { state = false, message = "Veuillez renseigner tous les champs svp!!!" });
                //var user = db.UTILISATEUR.Find(id_user);
                int? id_util =GetUserAuthentificateData(Request)?.Id_util;
                var user = db.UTILISATEUR.Find(id_util);
                if (user == null) return Json(new { state = false, message = "Désolé, nous ne retrouvons pas vos données!!!" });

				var verify_password = CodeHelper.VerifyMd5Hash(pass_last, user.PasswdHash_util);

				if (!verify_password)
				{
					throw new Exception("L'ancien mot de passe est incorrect!!!");
				}

                var passhash = CodeHelper.GetMd5Hash(pass_new.Trim());
                user.Login_util = login_new;
                user.PasswdHash_util = passhash;
                db.SaveChanges();
                return Json(new { state = true});
            }
            catch (DbEntityValidationException e)
            {
                var exception = SharedController.ErrorPrinter(e.EntityValidationErrors);
                var listException = exception.Item1;
                var message = exception.Item2;
                return Json(new { state = false, message, listException = listException }, JsonRequestBehavior.AllowGet);
            }
            catch (DbUpdateException)
            {
                return Json(new { state = false, message = "Les informations que vous avez entrées ne sont pas valident." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { state = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [Route("Deconnexion",Name = "authentification.deconnexion")]
        public ActionResult Deconnexion()
        {
            try
            {
                //supprimer les info de l'utilisateur du cache
                Session.Abandon();
                //effacer le ticket d'authentification
                FormsAuthentication.SignOut();
                //effacer le cookie d'authentification
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                cookie.Expires = DateTime.Now.AddYears(-1);
                Response.Cookies.Add(cookie);
                return RedirectToActionPermanent("Accueil", "Accueil");
            }
            catch (Exception)
            {
                return HttpNotFound("Une erreur s'est produite lors de la requete de deconnexion.");
            }
        }
        
        public static bool CreateAuthentificationCookie(HttpResponseBase response,string userName,int id_user,DateTime? date_creation)
        {
            try
            {
                if (response != null)
                {
					var string_date = string.Format("{0:dd/MM/yyyy}", date_creation);
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                       1,
                       userName,
                       DateTime.Now,
                       DateTime.Now.AddHours(2),
                       true,
                       id_user.ToString()+"_"+string_date,
                       FormsAuthentication.FormsCookiePath);
                    string encrypt = FormsAuthentication.Encrypt(ticket);
                    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypt);
                    response.Cookies.Add(cookie);
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
            
        }

		[Authorize]
        public static UTILISATEUR GetUserAuthentificateData(HttpRequestBase request)
        {
            try
            {
                var cookie =request.Cookies[FormsAuthentication.FormsCookieName];
                if (cookie != null)
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
					var data = ticket.UserData.Split('_');
					var date = data[1].Split('/');
                    return new UTILISATEUR() {
						Id_util =int.Parse(data[0]),
						Date_creation_util =new DateTime(int.Parse(date[2]), int.Parse(date[1]), int.Parse(date[0])), 
						Nom_util=ticket.Name
					};
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
    }
}