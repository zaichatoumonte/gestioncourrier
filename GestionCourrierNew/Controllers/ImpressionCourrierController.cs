﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GestionCourrierNew.Models;
//using GestionCourrierNew.Filter;

namespace GestionCourrierNew.Controllers
{
    [Authorize]
    [RoutePrefix("Courrier/Impression")]
    public class ImpressionCourrierController : Controller
    {
        // GET: ImpressionCourrier
        [Route("Depart/{id:int}",Name = "CourrierDepart.Imprimer")]
        public ActionResult Index(int id)
        {
            GestionCourrierEntities db = new GestionCourrierEntities();
            var courrier = db.COURRIER.Find(id);
            ViewBag.courrier = courrier;
            ViewBag.url_retour = Url.RouteUrl("reception.courrierDepart");
            return View();
        }

        [Route("Reponse/{id:int}", Name = "CourrierDepart.Imprimer.Reponse")]
        public ActionResult IndexReponse(int id)
        {
            GestionCourrierEntities db = new GestionCourrierEntities();
            var courrier = db.COURRIER.Find(id);
            ViewBag.courrier = courrier;
            ViewBag.url_retour = Url.RouteUrl("traitement.reponse");
            return View("Index");
        }

        [Route("Classement/{id:int}", Name = "CourrierDepart.Imprimer.Classement")]
        public ActionResult IndexClassement(int id)
        {
            GestionCourrierEntities db = new GestionCourrierEntities();
            var courrier = db.COURRIER.Find(id);
            ViewBag.courrier = courrier;
            ViewBag.url_retour = Url.RouteUrl("traitement.classement");
            return View("Index");
        }

        [Route("Archive/{id:int}", Name = "CourrierDepart.Imprimer.archivage")]
        public ActionResult IndexArchivage(int id)
        {
            GestionCourrierEntities db = new GestionCourrierEntities();
            var archive = db.ARCHIVAGE.Find(id);
			var courrier = new COURRIER() {
				Ref_cour = archive.Ref_cour_arch,
				Obj_cour = archive.Obj_cour_arch,
				Cont_cour = archive.Cont_cour_arch,
				CATEGORIE_COURRIER=new CATEGORIE_COURRIER() { Lib_cat_cour = archive.Lib_cat_cour_arch }
            };
            ViewBag.courrier = courrier;
            ViewBag.archive = archive.Destinataires_arch;
            ViewBag.url_retour = Url.RouteUrl("archives.consulter");
            return View("Index");
        }

        [HttpGet]
        [Route("Detail/{dest_archive}", Name = "CourrierDepart.Imprimer.getDestArchive")]
        public JsonResult DetailCourrier(string dest_archive)
        {
            try
            {
                var List = ArchivageRegistreController.GetListEchangeIntervenantArchive(dest_archive);
                List<SERVICE> service = new List<SERVICE>();
                List<CORRESPONDANT> contact = new List<CORRESPONDANT>();
                if (List != null)
                {
                    foreach (var item in List.Item1)
                    {
                        service.Add(new SERVICE()
                        {
                            Lib_serv = item.Nom,
                            Civ_serv = item.Civ,
                            Resp_serv = item.Resp,
                        });
                    }
                    foreach (var item in List.Item2)
                    {
                        contact.Add(new CORRESPONDANT()
                        {
                            Nom_cor = item.Nom,
                            Civ_cor = item.Civ,
                            Resp_cor = item.Resp,
                        });
                    }
                }
                return Json(new { state = true, data = new { destCont = contact, destServ = service } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { state = false }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}